## Descrição do serviço
este projeto é um fork do repositório privado [VisualSciELO](https://gomachine@bitbucket.org/net-works/visualscielo.git). Projeto este em desenvolvimento pelo Juan Pablo.

### Motivação para criar este Fork
Como o projeto estava deficiente para ser portado para uma Imagem Docker. A equipe DevOps SciELO fez ajustes para que a imagem fosse criada.

### O que esperar da Imagem Docker
O projeto VisualSciELO foi desenvolvido em Python 2.7 e tem como Servidor HTTP WSGI o gunicorn.

#### Como iniciar o conteiner

```
docker run \
--name visualscielo -p 8000:8000 \
-d scieloorg/visualscielo
```
Variáveis aceitas:
```
- HOSTNAME=<endereço web do servidor iniciado>
- SERVER=<endereço servidor articlemeta api. Endereço padrão: fractalnet-works.ddns.net> 
- ACCESSSTATS=<porta articlemeta. Porta padrão: 11660> 
- PUBLICATIONSTATS=<porta publicationstats. Porta padrão: 11620> 
- ARTICLEMETA=<porta articlemeta rpc. Porta padrão: 11720> 
- CITEDBY=<porta citedby. Porta padrão: 11>
```

#### Como acessar o site:
```
http://<endereço ip/dns>:<porta>/v3
```

#### IMPORTANTE:
Quando inicia o conteiner já é esperado o erro 500 no browser. Para corrigir o problema:
```
docker exec -ti <ID docker> bash
/app $ mkdir  BibliometricsApp/app/cache/v3/
```
