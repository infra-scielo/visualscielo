#!/usr/bin/env python
# -*- coding: utf-8 -*-
ISO_3166_ALPHA2 = {
	"WF": {
		"alpha3": "WLF",
		"fullname": "Wallis and Futuna Islands",
		"code": "ocwlf",
		"demonym": "Wallisian",
		"continent": "oc"
	},
	"JP": {
		"alpha3": "JPN",
		"fullname": "Japan",
		"code": "asjpn",
		"demonym": "Japanese",
		"continent": "as"
	},
	"JM": {
		"alpha3": "JAM",
		"fullname": "Jamaica",
		"code": "najam",
		"demonym": "Jamaican",
		"continent": "na"
	},
	"JO": {
		"alpha3": "JOR",
		"fullname": "Jordan",
		"code": "asjor",
		"demonym": "Jordanian",
		"continent": "as"
	},
	"WS": {
		"alpha3": "WSM",
		"fullname": "Samoa",
		"code": "ocwsm",
		"demonym": "Samoan",
		"continent": "oc"
	},
	"GW": {
		"alpha3": "GNB",
		"fullname": "Guinea-Bissau",
		"code": "afgnb",
		"demonym": "Guinean",
		"continent": "af"
	},
	"GU": {
		"alpha3": "GUM",
		"fullname": "Guam",
		"code": "ocgum",
		"demonym": "Guamanian",
		"continent": "oc"
	},
	"GT": {
		"alpha3": "GTM",
		"fullname": "Guatemala",
		"code": "nagtm",
		"demonym": "Guatemalan",
		"continent": "na"
	},
	"GR": {
		"alpha3": "GRC",
		"fullname": "Greece",
		"code": "eugrc",
		"demonym": "Greek",
		"continent": "eu"
	},
	"GQ": {
		"alpha3": "GNQ",
		"fullname": "Equatorial Guinea",
		"code": "afgnq",
		"demonym": "Equatorial Guinean",
		"continent": "af"
	},
	"GP": {
		"alpha3": "GLP",
		"fullname": "Guadeloupe",
		"code": "naglp",
		"demonym": "Guadeloupean",
		"continent": "na"
	},
	"GY": {
		"alpha3": "GUY",
		"fullname": "Guyana",
		"code": "saguy",
		"demonym": "Guyanese",
		"continent": "sa"
	},
	"GF": {
		"alpha3": "GUF",
		"fullname": "French Guiana",
		"code": "saguf",
		"demonym": "French Guianese",
		"continent": "sa"
	},
	"GE": {
		"alpha3": "GEO",
		"fullname": "Georgia",
		"code": "asgeo",
		"demonym": "Georgian",
		"continent": "as"
	},
	"GD": {
		"alpha3": "GRD",
		"fullname": "Grenada and Carriacuou",
		"code": "nagrd",
		"demonym": "Grenadian",
		"continent": "na"
	},
	"GB": {
		"alpha3": "GBR",
		"fullname": "United Kingdom",
		"code": "eugbr",
		"demonym": "British",
		"continent": "eu"
	},
	"GA": {
		"alpha3": "GAB",
		"fullname": "Gabon Republic",
		"code": "afgab",
		"demonym": "Gabonese",
		"continent": "af"
	},
	"GN": {
		"alpha3": "GIN",
		"fullname": "Guinea",
		"code": "afgin",
		"demonym": "Guinean",
		"continent": "af"
	},
	"GM": {
		"alpha3": "GMB",
		"fullname": "Gambia",
		"code": "afgmb",
		"demonym": "Gambian",
		"continent": "af"
	},
	"GL": {
		"alpha3": "GRL",
		"fullname": "Greenland",
		"code": "nagrl",
		"demonym": "Greenlander",
		"continent": "na"
	},
	"GI": {
		"alpha3": "GIB",
		"fullname": "Gibraltar",
		"code": "eugib",
		"demonym": "Gibralterian",
		"continent": "eu"
	},
	"GH": {
		"alpha3": "GHA",
		"fullname": "Ghana",
		"code": "afgha",
		"demonym": "Ghanaian",
		"continent": "af"
	},
	"PR": {
		"alpha3": "PRI",
		"fullname": "Puerto Rico",
		"code": "napri",
		"demonym": "Puerto Rican",
		"continent": "na"
	},
	"PS": {
		"alpha3": "PSE",
		"fullname": "Palestine",
		"code": "aspse",
		"demonym": "Palestinian",
		"continent": "as"
	},
	"PW": {
		"alpha3": "PLW",
		"fullname": "Palau",
		"code": "ocplw",
		"demonym": "Palauan",
		"continent": "oc"
	},
	"PT": {
		"alpha3": "PRT",
		"fullname": "Portugal",
		"code": "euprt",
		"demonym": "Portuguese",
		"continent": "eu"
	},
	"PY": {
		"alpha3": "PRY",
		"fullname": "Paraguay",
		"code": "sapry",
		"demonym": "Paraguayan",
		"continent": "sa"
	},
	"PA": {
		"alpha3": "PAN",
		"fullname": "Panama",
		"code": "napan",
		"demonym": "Panamanian",
		"continent": "na"
	},
	"PF": {
		"alpha3": "PYF",
		"fullname": "French Polynesia",
		"code": "ocpyf",
		"demonym": "French Polynesian",
		"continent": "oc"
	},
	"PG": {
		"alpha3": "PNG",
		"fullname": "Papua New Guinea",
		"code": "ocpng",
		"demonym": "Papua New Guinean",
		"continent": "oc"
	},
	"PE": {
		"alpha3": "PER",
		"fullname": "Peru",
		"code": "saper",
		"demonym": "Peruvian",
		"continent": "sa"
	},
	"PK": {
		"alpha3": "PAK",
		"fullname": "Pakistan",
		"code": "aspak",
		"demonym": "Pakistani",
		"continent": "as"
	},
	"PH": {
		"alpha3": "PHL",
		"fullname": "Philippines",
		"code": "asphl",
		"demonym": "Filipino",
		"continent": "as"
	},
	"PL": {
		"alpha3": "POL",
		"fullname": "Poland",
		"code": "eupol",
		"demonym": "Polish",
		"continent": "eu"
	},
	"PM": {
		"alpha3": "SPM",
		"fullname": "St. Pierre and Miquelon",
		"code": "naspm",
		"demonym": "Saint-Pierrais",
		"continent": "na"
	},
	"ZM": {
		"alpha3": "ZMB",
		"fullname": "Zambia",
		"code": "afzmb",
		"demonym": "Zambian",
		"continent": "af"
	},
	"ZA": {
		"alpha3": "ZAF",
		"fullname": "South Africa",
		"code": "afzaf",
		"demonym": "South African",
		"continent": "af"
	},
	"ZW": {
		"alpha3": "ZWE",
		"fullname": "Zimbabwe",
		"code": "afzwe",
		"demonym": "Zimbabwean",
		"continent": "af"
	},
	"ME": {
		"alpha3": "MNE",
		"fullname": "Montenegro",
		"code": "eumne",
		"demonym": "Montenegrin",
		"continent": "eu"
	},
	"MD": {
		"alpha3": "MDA",
		"fullname": "Moldova",
		"code": "eumda",
		"demonym": "Moldovan",
		"continent": "eu"
	},
	"MG": {
		"alpha3": "MDG",
		"fullname": "Madagascar",
		"code": "afmdg",
		"demonym": "Malagasy",
		"continent": "af"
	},
	"MA": {
		"alpha3": "MAR",
		"fullname": "Morocco",
		"code": "afmar",
		"demonym": "Moroccan",
		"continent": "af"
	},
	"MC": {
		"alpha3": "MCO",
		"fullname": "Monaco",
		"code": "eumco",
		"demonym": "Monacan",
		"continent": "eu"
	},
	"MM": {
		"alpha3": "MMR",
		"fullname": "Myanmar",
		"code": "asmmr",
		"demonym": "Myanmarese",
		"continent": "as"
	},
	"ML": {
		"alpha3": "MLI",
		"fullname": "Mali Republic",
		"code": "afmli",
		"demonym": "Malian",
		"continent": "af"
	},
	"MO": {
		"alpha3": "MAC",
		"fullname": "Macau",
		"code": "asmac",
		"demonym": "Macanese",
		"continent": "as"
	},
	"MN": {
		"alpha3": "MNG",
		"fullname": "Mongolia",
		"code": "asmng",
		"demonym": "Mongolian",
		"continent": "as"
	},
	"MH": {
		"alpha3": "MHL",
		"fullname": "Marshall Islands",
		"code": "ocmhl",
		"demonym": "Marshallese",
		"continent": "oc"
	},
	"MK": {
		"alpha3": "MKD",
		"fullname": "Macedonia",
		"code": "eumkd",
		"demonym": "Macedonian",
		"continent": "eu"
	},
	"MU": {
		"alpha3": "MUS",
		"fullname": "Mauritius",
		"code": "afmus",
		"demonym": "Mauritian",
		"continent": "af"
	},
	"MT": {
		"alpha3": "MLT",
		"fullname": "Malta",
		"code": "eumlt",
		"demonym": "Maltese",
		"continent": "eu"
	},
	"MW": {
		"alpha3": "MWI",
		"fullname": "Malawi",
		"code": "afmwi",
		"demonym": "Malawian",
		"continent": "af"
	},
	"MV": {
		"alpha3": "MDV",
		"fullname": "Maldives",
		"code": "asmdv",
		"demonym": "Maldivan",
		"continent": "as"
	},
	"MQ": {
		"alpha3": "MTQ",
		"fullname": "Martinique",
		"code": "namtq",
		"demonym": "Martinican",
		"continent": "na"
	},
	"MP": {
		"alpha3": "MNP",
		"fullname": "Saipan",
		"code": "ocmnp",
		"demonym": "Northern Mariana Islander",
		"continent": "oc"
	},
	"MS": {
		"alpha3": "MSR",
		"fullname": "Montserrat",
		"code": "namsr",
		"demonym": "Montserratian",
		"continent": "na"
	},
	"MR": {
		"alpha3": "MRT",
		"fullname": "Mauritania",
		"code": "afmrt",
		"demonym": "Mauritanian",
		"continent": "af"
	},
	"MY": {
		"alpha3": "MYS",
		"fullname": "Malaysia",
		"code": "asmys",
		"demonym": "Malaysian",
		"continent": "as"
	},
	"MX": {
		"alpha3": "MEX",
		"fullname": "Mexico",
		"code": "namex",
		"demonym": "Mexican",
		"continent": "na"
	},
	"MZ": {
		"alpha3": "MOZ",
		"fullname": "Mozambique",
		"code": "afmoz",
		"demonym": "Mozambican",
		"continent": "af"
	},
	"FR": {
		"alpha3": "FRA",
		"fullname": "France",
		"code": "eufra",
		"demonym": "French",
		"continent": "eu"
	},
	"FI": {
		"alpha3": "FIN",
		"fullname": "Finland",
		"code": "eufin",
		"demonym": "Finnish",
		"continent": "eu"
	},
	"FJ": {
		"alpha3": "FJI",
		"fullname": "Fiji Islands",
		"code": "ocfji",
		"demonym": "Fijian",
		"continent": "oc"
	},
	"FK": {
		"alpha3": "FLK",
		"fullname": "Falkland Islands",
		"code": "saflk",
		"demonym": "Falkland Islander",
		"continent": "sa"
	},
	"FM": {
		"alpha3": "FSM",
		"fullname": "Federated States of Micronesia",
		"code": "ocfsm",
		"demonym": "Micronesian",
		"continent": "oc"
	},
	"FO": {
		"alpha3": "FRO",
		"fullname": "Faroe Islands",
		"code": "eufro",
		"demonym": "Faroese",
		"continent": "eu"
	},
	"CK": {
		"alpha3": "COK",
		"fullname": "Cook Islands",
		"code": "occok",
		"demonym": "Cook Islander",
		"continent": "oc"
	},
	"CI": {
		"alpha3": "CIV",
		"fullname": "Ivory Coast",
		"code": "afciv",
		"demonym": "Ivorian",
		"continent": "af"
	},
	"CH": {
		"alpha3": "CHE",
		"fullname": "Switzerland",
		"code": "euche",
		"demonym": "Swiss",
		"continent": "eu"
	},
	"CO": {
		"alpha3": "COL",
		"fullname": "Colombia",
		"code": "sacol",
		"demonym": "Colombian",
		"continent": "sa"
	},
	"CN": {
		"alpha3": "CHN",
		"fullname": "China",
		"code": "aschn",
		"demonym": "Chinese",
		"continent": "as"
	},
	"CM": {
		"alpha3": "CMR",
		"fullname": "Cameroon",
		"code": "afcmr",
		"demonym": "Cameroonian",
		"continent": "af"
	},
	"CL": {
		"alpha3": "CHL",
		"fullname": "Chile",
		"code": "sachl",
		"demonym": "Chilean",
		"continent": "sa"
	},
	"CC": {
		"alpha3": "CCK",
		"fullname": "Cocos Islands",
		"code": "ascck",
		"demonym": "Cocossian",
		"continent": "as"
	},
	"CA": {
		"alpha3": "CAN",
		"fullname": "Canada",
		"code": "nacan",
		"demonym": "Canadian",
		"continent": "na"
	},
	"CG": {
		"alpha3": "COG",
		"fullname": "Congo",
		"code": "afcog",
		"demonym": "Congolese",
		"continent": "af"
	},
	"CF": {
		"alpha3": "CAF",
		"fullname": "Central African Republic",
		"code": "afcaf",
		"demonym": "Central African",
		"continent": "af"
	},
	"CD": {
		"alpha3": "COD",
		"fullname": "Dem. Republic of the Congo",
		"code": "afcod",
		"demonym": "Congolese",
		"continent": "af"
	},
	"CZ": {
		"alpha3": "CZE",
		"fullname": "Czech Republic",
		"code": "eucze",
		"demonym": "Czech",
		"continent": "eu"
	},
	"CY": {
		"alpha3": "CYP",
		"fullname": "Cyprus",
		"code": "eucyp",
		"demonym": "Cypriot",
		"continent": "eu"
	},
	"CX": {
		"alpha3": "CXR",
		"fullname": "Christmas Island",
		"code": "ascxr",
		"demonym": "Christmas Islander",
		"continent": "as"
	},
	"CR": {
		"alpha3": "CRI",
		"fullname": "Costa Rica",
		"code": "nacri",
		"demonym": "Costa Rican",
		"continent": "na"
	},
	"CW": {
		"alpha3": "ANT",
		"fullname": "Netherlands Antilles",
		"code": "naant",
		"demonym": "Cura\u00e7aoan",
		"continent": "na"
	},
	"CV": {
		"alpha3": "CPV",
		"fullname": "Cape Verde Islands",
		"code": "afcpv",
		"demonym": "Cape Verdean",
		"continent": "af"
	},
	"CU": {
		"alpha3": "CUB",
		"fullname": "Cuba",
		"code": "nacub",
		"demonym": "Cuban",
		"continent": "na"
	},
	"SZ": {
		"alpha3": "SWZ",
		"fullname": "Swaziland",
		"code": "afswz",
		"demonym": "Swazi",
		"continent": "af"
	},
	"SY": {
		"alpha3": "SYR",
		"fullname": "Syria",
		"code": "assyr",
		"demonym": "Syrian",
		"continent": "as"
	},
	"SX": {
		"alpha3": "SXM",
		"fullname": "Sint Maarten",
		"code": "nasxm",
		"demonym": "Saint Martin (Netherlands)",
		"continent": "na"
	},
	"SS": {
		"alpha3": "SSD",
		"fullname": "South Sudan",
		"code": "afssd",
		"demonym": "Sudanese",
		"continent": "af"
	},
	"SR": {
		"alpha3": "SUR",
		"fullname": "Suriname",
		"code": "sasur",
		"demonym": "Surinamer",
		"continent": "sa"
	},
	"SV": {
		"alpha3": "SLV",
		"fullname": "El Salvador",
		"code": "naslv",
		"demonym": "Salvadorean",
		"continent": "na"
	},
	"ST": {
		"alpha3": "STP",
		"fullname": "Sao Tome",
		"code": "afstp",
		"demonym": "S\u00e3o Tomean",
		"continent": "af"
	},
	"SK": {
		"alpha3": "SVK",
		"fullname": "Slovakia",
		"code": "eusvk",
		"demonym": "Slovakian",
		"continent": "eu"
	},
	"SI": {
		"alpha3": "SVN",
		"fullname": "Slovenia",
		"code": "eusvn",
		"demonym": "Slovenian",
		"continent": "eu"
	},
	"SH": {
		"alpha3": "SHN",
		"fullname": "St. Helena",
		"code": "afshn",
		"demonym": "Saint Helenian",
		"continent": "af"
	},
	"SO": {
		"alpha3": "SOM",
		"fullname": "Somalia Republic",
		"code": "afsom",
		"demonym": "Somali",
		"continent": "af"
	},
	"SN": {
		"alpha3": "SEN",
		"fullname": "Senegal Republic",
		"code": "afsen",
		"demonym": "Senegalese",
		"continent": "af"
	},
	"SM": {
		"alpha3": "SMR",
		"fullname": "San Marino",
		"code": "eusmr",
		"demonym": "Sanmarinese",
		"continent": "eu"
	},
	"SL": {
		"alpha3": "SLE",
		"fullname": "Sierra Leone",
		"code": "afsle",
		"demonym": "Sierra Leonean",
		"continent": "af"
	},
	"SC": {
		"alpha3": "SYC",
		"fullname": "Seychelles",
		"code": "afsyc",
		"demonym": "Seychellois",
		"continent": "af"
	},
	"SB": {
		"alpha3": "SLB",
		"fullname": "Solomon Islands",
		"code": "ocslb",
		"demonym": "Solomon Islander",
		"continent": "oc"
	},
	"SA": {
		"alpha3": "SAU",
		"fullname": "Saudi Arabia",
		"code": "assau",
		"demonym": "Saudi Arabian",
		"continent": "as"
	},
	"SG": {
		"alpha3": "SGP",
		"fullname": "Singapore",
		"code": "assgp",
		"demonym": "Singaporean",
		"continent": "as"
	},
	"SE": {
		"alpha3": "BES",
		"fullname": "Sint Eustatius",
		"code": "nabes",
		"demonym": "Swedish",
		"continent": "na"
	},
	"SD": {
		"alpha3": "SDN",
		"fullname": "Sudan",
		"code": "afsdn",
		"demonym": "Sudanese",
		"continent": "af"
	},
	"YE": {
		"alpha3": "YEM",
		"fullname": "Yemen",
		"code": "asyem",
		"demonym": "Yemeni",
		"continent": "as"
	},
	"YT": {
		"alpha3": "MYT",
		"fullname": "Mayotte Island",
		"code": "afmyt",
		"demonym": "Mahoran",
		"continent": "af"
	},
	"LB": {
		"alpha3": "LBN",
		"fullname": "Lebanon",
		"code": "aslbn",
		"demonym": "Lebanese",
		"continent": "as"
	},
	"LC": {
		"alpha3": "LCA",
		"fullname": "St. Lucia",
		"code": "nalca",
		"demonym": "Saint Lucian",
		"continent": "na"
	},
	"LA": {
		"alpha3": "LAO",
		"fullname": "Laos",
		"code": "aslao",
		"demonym": "Laotian",
		"continent": "as"
	},
	"LK": {
		"alpha3": "LKA",
		"fullname": "Sri Lanka",
		"code": "aslka",
		"demonym": "Sri Lankan",
		"continent": "as"
	},
	"LI": {
		"alpha3": "LIE",
		"fullname": "Liechtenstein",
		"code": "eulie",
		"demonym": "Liechtensteiner",
		"continent": "eu"
	},
	"LV": {
		"alpha3": "LVA",
		"fullname": "Latvia",
		"code": "eulva",
		"demonym": "Latvian",
		"continent": "eu"
	},
	"LT": {
		"alpha3": "LTU",
		"fullname": "Lithuania",
		"code": "eultu",
		"demonym": "Lithunian",
		"continent": "eu"
	},
	"LU": {
		"alpha3": "LUX",
		"fullname": "Luxembourg",
		"code": "eulux",
		"demonym": "Luxembourger",
		"continent": "eu"
	},
	"LR": {
		"alpha3": "LBR",
		"fullname": "Liberia",
		"code": "aflbr",
		"demonym": "Liberian",
		"continent": "af"
	},
	"LS": {
		"alpha3": "LSO",
		"fullname": "Lesotho",
		"code": "aflso",
		"demonym": "Mosotho",
		"continent": "af"
	},
	"LY": {
		"alpha3": "LBY",
		"fullname": "Libya",
		"code": "aflby",
		"demonym": "Libyan",
		"continent": "af"
	},
	"VA": {
		"alpha3": "VAT",
		"fullname": "Vatican city",
		"code": "euvat",
		"demonym": "Vatican",
		"continent": "eu"
	},
	"VC": {
		"alpha3": "VCT",
		"fullname": "St. Vincent",
		"code": "navct",
		"demonym": "Saint Vincentian",
		"continent": "na"
	},
	"VE": {
		"alpha3": "VEN",
		"fullname": "Venezuela",
		"code": "saven",
		"demonym": "Venezuelan",
		"continent": "sa"
	},
	"VG": {
		"alpha3": "VGB",
		"fullname": "British Virgin Islands",
		"code": "navgb",
		"demonym": "Virgin Islander",
		"continent": "na"
	},
	"IQ": {
		"alpha3": "IRQ",
		"fullname": "Iraq",
		"code": "asirq",
		"demonym": "Iraqi",
		"continent": "as"
	},
	"VI": {
		"alpha3": "VIR",
		"fullname": "US Virgin Islands",
		"code": "navir",
		"demonym": "Virgin Islander",
		"continent": "na"
	},
	"IS": {
		"alpha3": "ISL",
		"fullname": "Iceland",
		"code": "euisl",
		"demonym": "Iceland",
		"continent": "eu"
	},
	"IR": {
		"alpha3": "IRN",
		"fullname": "Iran",
		"code": "asirn",
		"demonym": "Iranian",
		"continent": "as"
	},
	"IT": {
		"alpha3": "ITA",
		"fullname": "Italy",
		"code": "euita",
		"demonym": "Italian",
		"continent": "eu"
	},
	"VN": {
		"alpha3": "VNM",
		"fullname": "Vietnam",
		"code": "asvnm",
		"demonym": "Vietnamese",
		"continent": "as"
	},
	"IM": {
		"alpha3": "IMN",
		"fullname": "Isle of Man",
		"code": "euimn",
		"demonym": "Manx",
		"continent": "eu"
	},
	"IL": {
		"alpha3": "ISR",
		"fullname": "Israel",
		"code": "asisr",
		"demonym": "Israeli",
		"continent": "as"
	},
	"IO": {
		"alpha3": "IOT",
		"fullname": "Diego Garcia",
		"code": "asiot",
		"demonym": "British Indian Ocean Territory",
		"continent": "as"
	},
	"IN": {
		"alpha3": "IND",
		"fullname": "India",
		"code": "asind",
		"demonym": "Indian",
		"continent": "as"
	},
	"IE": {
		"alpha3": "IRL",
		"fullname": "Ireland",
		"code": "euirl",
		"demonym": "Irish",
		"continent": "eu"
	},
	"ID": {
		"alpha3": "IDN",
		"fullname": "Indonesia",
		"code": "asidn",
		"demonym": "Indonesian",
		"continent": "as"
	},
	"BD": {
		"alpha3": "BGD",
		"fullname": "Bangladesh",
		"code": "asbgd",
		"demonym": "Bangladeshi",
		"continent": "as"
	},
	"BE": {
		"alpha3": "BEL",
		"fullname": "Belgium",
		"code": "eubel",
		"demonym": "Belgian",
		"continent": "eu"
	},
	"BF": {
		"alpha3": "BFA",
		"fullname": "Burkina Faso",
		"code": "afbfa",
		"demonym": "Burkinabe",
		"continent": "af"
	},
	"BG": {
		"alpha3": "BGR",
		"fullname": "Bulgaria",
		"code": "eubgr",
		"demonym": "Bulgarian",
		"continent": "eu"
	},
	"BA": {
		"alpha3": "BIH",
		"fullname": "Bosnia and Herzegovina",
		"code": "eubih",
		"demonym": "Bosnian",
		"continent": "eu"
	},
	"BB": {
		"alpha3": "BRB",
		"fullname": "Barbados",
		"code": "nabrb",
		"demonym": "Barbadian",
		"continent": "na"
	},
	"BM": {
		"alpha3": "BMU",
		"fullname": "Bermuda",
		"code": "nabmu",
		"demonym": "Bermudan",
		"continent": "na"
	},
	"BN": {
		"alpha3": "BRN",
		"fullname": "Brunei",
		"code": "asbrn",
		"demonym": "Bruneian",
		"continent": "as"
	},
	"BO": {
		"alpha3": "BOL",
		"fullname": "Bolivia",
		"code": "sabol",
		"demonym": "Bolivian",
		"continent": "sa"
	},
	"BH": {
		"alpha3": "BHR",
		"fullname": "Bahrain",
		"code": "asbhr",
		"demonym": "Bahrainian",
		"continent": "as"
	},
	"BI": {
		"alpha3": "BDI",
		"fullname": "Burundi",
		"code": "afbdi",
		"demonym": "Burundian",
		"continent": "af"
	},
	"BJ": {
		"alpha3": "BEN",
		"fullname": "Benin",
		"code": "afben",
		"demonym": "Beninese",
		"continent": "af"
	},
	"BT": {
		"alpha3": "BTN",
		"fullname": "Bhutan",
		"code": "asbtn",
		"demonym": "Bhutanese",
		"continent": "as"
	},
	"BW": {
		"alpha3": "BWA",
		"fullname": "Botswana",
		"code": "afbwa",
		"demonym": "Motswana",
		"continent": "af"
	},
	"BQ": {
		"alpha3": "BES",
		"fullname": "Bonaire",
		"code": "nabes",
		"demonym": "",
		"continent": "na"
	},
	"BR": {
		"alpha3": "BRA",
		"fullname": "Brazil",
		"code": "sabra",
		"demonym": "Brazilian",
		"continent": "sa"
	},
	"BS": {
		"alpha3": "BHS",
		"fullname": "Bahamas",
		"code": "nabhs",
		"demonym": "Bahameese",
		"continent": "na"
	},
	"BY": {
		"alpha3": "BLR",
		"fullname": "Belarus",
		"code": "eublr",
		"demonym": "Belarusian",
		"continent": "eu"
	},
	"BZ": {
		"alpha3": "BLZ",
		"fullname": "Belize",
		"code": "nablz",
		"demonym": "Belizean",
		"continent": "na"
	},
	"RU": {
		"alpha3": "RUS",
		"fullname": "Russia",
		"code": "eurus",
		"demonym": "Russian",
		"continent": "eu"
	},
	"RW": {
		"alpha3": "RWA",
		"fullname": "Rwanda",
		"code": "afrwa",
		"demonym": "Rwandan",
		"continent": "af"
	},
	"RS": {
		"alpha3": "RSB",
		"fullname": "Yugoslavia",
		"code": "eursb",
		"demonym": "Serbian",
		"continent": "eu"
	},
	"RE": {
		"alpha3": "REU",
		"fullname": "Reunion Island",
		"code": "afreu",
		"demonym": "",
		"continent": "af"
	},
	"RO": {
		"alpha3": "ROU",
		"fullname": "Romania",
		"code": "eurou",
		"demonym": "Romanian",
		"continent": "eu"
	},
	"OM": {
		"alpha3": "OMN",
		"fullname": "Oman",
		"code": "asomn",
		"demonym": "Omani",
		"continent": "as"
	},
	"HR": {
		"alpha3": "HRV",
		"fullname": "Croatia",
		"code": "euhrv",
		"demonym": "Croatian",
		"continent": "eu"
	},
	"HT": {
		"alpha3": "HTI",
		"fullname": "Haiti",
		"code": "nahti",
		"demonym": "Haitian",
		"continent": "na"
	},
	"HU": {
		"alpha3": "HUN",
		"fullname": "Hungary",
		"code": "euhun",
		"demonym": "Hungarian",
		"continent": "eu"
	},
	"HK": {
		"alpha3": "HKG",
		"fullname": "Hong Kong",
		"code": "ashkg",
		"demonym": "Hong Konger",
		"continent": "as"
	},
	"HN": {
		"alpha3": "HND",
		"fullname": "Honduras",
		"code": "nahnd",
		"demonym": "Honduran",
		"continent": "na"
	},
	"EE": {
		"alpha3": "EST",
		"fullname": "Estonia",
		"code": "euest",
		"demonym": "Estonian",
		"continent": "eu"
	},
	"EG": {
		"alpha3": "EGY",
		"fullname": "Egypt",
		"code": "afegy",
		"demonym": "Egyptian",
		"continent": "af"
	},
	"EC": {
		"alpha3": "ECU",
		"fullname": "Ecuador",
		"code": "saecu",
		"demonym": "Ecuadorean",
		"continent": "sa"
	},
	"ET": {
		"alpha3": "ETH",
		"fullname": "Ethiopia",
		"code": "afeth",
		"demonym": "Ethiopian",
		"continent": "af"
	},
	"ES": {
		"alpha3": "ESP",
		"fullname": "Spain",
		"code": "euesp",
		"demonym": "Spanish",
		"continent": "eu"
	},
	"ER": {
		"alpha3": "ERI",
		"fullname": "Eritrea",
		"code": "aferi",
		"demonym": "Eritrean",
		"continent": "af"
	},
	"UY": {
		"alpha3": "URY",
		"fullname": "Uruguay",
		"code": "saury",
		"demonym": "Uruguayan",
		"continent": "sa"
	},
	"UZ": {
		"alpha3": "UZB",
		"fullname": "Uzbekistan",
		"code": "asuzb",
		"demonym": "Uzbekistani",
		"continent": "as"
	},
	"US": {
		"alpha3": "USA",
		"fullname": "United States",
		"code": "nausa",
		"demonym": "American",
		"continent": "na"
	},
	"UM": {
		"alpha3": "UMI",
		"fullname": "Wake Island",
		"code": "ocumi",
		"demonym": "United States Minor Outlying Islands",
		"continent": "oc"
	},
	"UG": {
		"alpha3": "UGA",
		"fullname": "Uganda",
		"code": "afuga",
		"demonym": "Ugandan",
		"continent": "af"
	},
	"UA": {
		"alpha3": "UKR",
		"fullname": "Ukraine",
		"code": "euukr",
		"demonym": "Ukrainian",
		"continent": "eu"
	},
	"VU": {
		"alpha3": "VUT",
		"fullname": "Vanuatu",
		"code": "ocvut",
		"demonym": "Ni-Vanuatu",
		"continent": "oc"
	},
	"NI": {
		"alpha3": "NIC",
		"fullname": "Nicaragua",
		"code": "nanic",
		"demonym": "Nicaraguan",
		"continent": "na"
	},
	"NL": {
		"alpha3": "NLD",
		"fullname": "Netherlands",
		"code": "eunld",
		"demonym": "Dutch",
		"continent": "eu"
	},
	"NO": {
		"alpha3": "NOR",
		"fullname": "Norway",
		"code": "eunor",
		"demonym": "Norwegian",
		"continent": "eu"
	},
	"NA": {
		"alpha3": "NAM",
		"fullname": "Namibia",
		"code": "afnam",
		"demonym": "Namibian",
		"continent": "af"
	},
	"NC": {
		"alpha3": "NCL",
		"fullname": "New Caledonia",
		"code": "ocncl",
		"demonym": "New Caledonian",
		"continent": "oc"
	},
	"NE": {
		"alpha3": "NER",
		"fullname": "Niger Republic",
		"code": "afner",
		"demonym": "Nigerien",
		"continent": "af"
	},
	"NF": {
		"alpha3": "NFK",
		"fullname": "Norfolk Island",
		"code": "ocnfk",
		"demonym": "Norfolk Islander",
		"continent": "oc"
	},
	"NG": {
		"alpha3": "NGA",
		"fullname": "Nigeria",
		"code": "afnga",
		"demonym": "Nigerian",
		"continent": "af"
	},
	"NZ": {
		"alpha3": "NZL",
		"fullname": "New Zealand",
		"code": "ocnzl",
		"demonym": "New Zealander",
		"continent": "oc"
	},
	"NP": {
		"alpha3": "NPL",
		"fullname": "Nepal",
		"code": "asnpl",
		"demonym": "Nepalese",
		"continent": "as"
	},
	"NR": {
		"alpha3": "NRU",
		"fullname": "Nauru",
		"code": "ocnru",
		"demonym": "Nauruan",
		"continent": "oc"
	},
	"NU": {
		"alpha3": "NIU",
		"fullname": "Niue",
		"code": "ocniu",
		"demonym": "Niuean",
		"continent": "oc"
	},
	"KG": {
		"alpha3": "KGZ",
		"fullname": "Kyrgyzstan",
		"code": "askgz",
		"demonym": "Kyrgyzstani",
		"continent": "as"
	},
	"KE": {
		"alpha3": "KEN",
		"fullname": "Kenya",
		"code": "afken",
		"demonym": "Kenyan",
		"continent": "af"
	},
	"KI": {
		"alpha3": "KIR",
		"fullname": "Kiribati",
		"code": "ockir",
		"demonym": "I-Kiribati",
		"continent": "oc"
	},
	"KH": {
		"alpha3": "KHM",
		"fullname": "Cambodia",
		"code": "askhm",
		"demonym": "Cambodian",
		"continent": "as"
	},
	"KN": {
		"alpha3": "KNA",
		"fullname": "St. Kitts",
		"code": "nakna",
		"demonym": "Kittian",
		"continent": "na"
	},
	"KM": {
		"alpha3": "COM",
		"fullname": "Comoros",
		"code": "afcom",
		"demonym": "Comoran",
		"continent": "af"
	},
	"KR": {
		"alpha3": "KOR",
		"fullname": "South Korea",
		"code": "askor",
		"demonym": "South Korean",
		"continent": "as"
	},
	"KP": {
		"alpha3": "PRK",
		"fullname": "North Korea",
		"code": "asprk",
		"demonym": "North Korean",
		"continent": "as"
	},
	"KW": {
		"alpha3": "KWT",
		"fullname": "Kuwait",
		"code": "askwt",
		"demonym": "Kuwaiti",
		"continent": "as"
	},
	"KZ": {
		"alpha3": "KAZ",
		"fullname": "Kazakhstan",
		"code": "askaz",
		"demonym": "Kazakhstani",
		"continent": "as"
	},
	"KY": {
		"alpha3": "CYM",
		"fullname": "Cayman Islands",
		"code": "nacym",
		"demonym": "Caymanian",
		"continent": "na"
	},
	"DO": {
		"alpha3": "DOM",
		"fullname": "Dominican Republic",
		"code": "nadom",
		"demonym": "Dominican",
		"continent": "na"
	},
	"DM": {
		"alpha3": "DMA",
		"fullname": "Dominica",
		"code": "nadma",
		"demonym": "Dominican",
		"continent": "na"
	},
	"DJ": {
		"alpha3": "DJI",
		"fullname": "Djibouti",
		"code": "afdji",
		"demonym": "Djiboutian",
		"continent": "af"
	},
	"DK": {
		"alpha3": "DNK",
		"fullname": "Denmark",
		"code": "eudnk",
		"demonym": "Danish",
		"continent": "eu"
	},
	"DE": {
		"alpha3": "DEU",
		"fullname": "Germany",
		"code": "eudeu",
		"demonym": "German",
		"continent": "eu"
	},
	"DZ": {
		"alpha3": "DZA",
		"fullname": "Algeria",
		"code": "afdza",
		"demonym": "Algerian",
		"continent": "af"
	},
	"TZ": {
		"alpha3": "TZA",
		"fullname": "Zanzibar",
		"code": "aftza",
		"demonym": "Tanzanian",
		"continent": "af"
	},
	"TV": {
		"alpha3": "TUV",
		"fullname": "Tuvalu",
		"code": "octuv",
		"demonym": "Tuvaluan",
		"continent": "oc"
	},
	"TW": {
		"alpha3": "TWN",
		"fullname": "Taiwan",
		"code": "astwn",
		"demonym": "Taiwanese",
		"continent": "as"
	},
	"TT": {
		"alpha3": "TTO",
		"fullname": "Trinidad and Tobago",
		"code": "natto",
		"demonym": "Trinidadian",
		"continent": "na"
	},
	"TR": {
		"alpha3": "TUR",
		"fullname": "Turkey",
		"code": "astur",
		"demonym": "Turkish",
		"continent": "as"
	},
	"TN": {
		"alpha3": "TUN",
		"fullname": "Tunisia",
		"code": "aftun",
		"demonym": "Tunisian",
		"continent": "af"
	},
	"TO": {
		"alpha3": "TON",
		"fullname": "Tonga",
		"code": "octon",
		"demonym": "Tongan",
		"continent": "oc"
	},
	"TL": {
		"alpha3": "TLS",
		"fullname": "East Timor",
		"code": "octls",
		"demonym": "Timorese",
		"continent": "oc"
	},
	"TM": {
		"alpha3": "TKM",
		"fullname": "Turkmenistan",
		"code": "astkm",
		"demonym": "Turkmen",
		"continent": "as"
	},
	"TJ": {
		"alpha3": "TJK",
		"fullname": "Tajikistan",
		"code": "astjk",
		"demonym": "Tajikistani",
		"continent": "as"
	},
	"TK": {
		"alpha3": "TKL",
		"fullname": "Tokelau",
		"code": "octkl",
		"demonym": "Tokelauan",
		"continent": "oc"
	},
	"TH": {
		"alpha3": "THA",
		"fullname": "Thailand",
		"code": "astha",
		"demonym": "Thai",
		"continent": "as"
	},
	"TG": {
		"alpha3": "TGO",
		"fullname": "Togo",
		"code": "aftgo",
		"demonym": "Togolese",
		"continent": "af"
	},
	"TD": {
		"alpha3": "TCD",
		"fullname": "Chad Republic",
		"code": "aftcd",
		"demonym": "Chadian",
		"continent": "af"
	},
	"TC": {
		"alpha3": "TCA",
		"fullname": "Turks and Caicos Islands",
		"code": "natca",
		"demonym": "Turks and Caicos Islander",
		"continent": "na"
	},
	"AE": {
		"alpha3": "ARE",
		"fullname": "United Arab Emirates",
		"code": "asare",
		"demonym": "Emirian",
		"continent": "as"
	},
	"AD": {
		"alpha3": "AND",
		"fullname": "Andorra",
		"code": "euand",
		"demonym": "Andorran",
		"continent": "eu"
	},
	"AG": {
		"alpha3": "ATG",
		"fullname": "Antigua and Barbuda",
		"code": "naatg",
		"demonym": "Antiguan",
		"continent": "na"
	},
	"AF": {
		"alpha3": "AFG",
		"fullname": "Afghanistan",
		"code": "asafg",
		"demonym": "Afghani",
		"continent": "as"
	},
	"AI": {
		"alpha3": "AIA",
		"fullname": "Anguilla",
		"code": "naaia",
		"demonym": "Anguillan",
		"continent": "na"
	},
	"AM": {
		"alpha3": "ARM",
		"fullname": "Armenia",
		"code": "asarm",
		"demonym": "Armenian",
		"continent": "as"
	},
	"AL": {
		"alpha3": "ALB",
		"fullname": "Albania",
		"code": "eualb",
		"demonym": "Albanian",
		"continent": "eu"
	},
	"AO": {
		"alpha3": "AGO",
		"fullname": "Angola",
		"code": "afago",
		"demonym": "Angolan",
		"continent": "af"
	},
	"AQ": {
		"alpha3": "ATA",
		"fullname": "Antarctica",
		"code": "anata",
		"demonym": "Antarctic",
		"continent": "an"
	},
	"AS": {
		"alpha3": "ASM",
		"fullname": "American Samoa",
		"code": "ocasm",
		"demonym": "Samoan",
		"continent": "oc"
	},
	"AR": {
		"alpha3": "ARG",
		"fullname": "Argentina",
		"code": "saarg",
		"demonym": "Argentine",
		"continent": "sa"
	},
	"AU": {
		"alpha3": "AUS",
		"fullname": "Australia",
		"code": "ocaus",
		"demonym": "Australian",
		"continent": "oc"
	},
	"AT": {
		"alpha3": "AUT",
		"fullname": "Austria",
		"code": "euaut",
		"demonym": "Austrian",
		"continent": "eu"
	},
	"AW": {
		"alpha3": "ABW",
		"fullname": "Aruba",
		"code": "naabw",
		"demonym": "Arubian",
		"continent": "na"
	},
	"AZ": {
		"alpha3": "AZE",
		"fullname": "Azerbaijan",
		"code": "asaze",
		"demonym": "Azerbaijani",
		"continent": "as"
	},
	"QA": {
		"alpha3": "QAT",
		"fullname": "Qatar",
		"code": "asqat",
		"demonym": "Qatari",
		"continent": "as"
	}
}