elasticQueries = {
    "1": {
        "text": "Documents",
        "tabulations": {
            "t1": {
                "text": "by country of publication",
                "query": {
                    "aggs": {
                        "by_all": {
                            "aggs":{ 
                                "by_collection": {
                                    "terms": {
                                        "field": "collection",
                                        "size": 0
                                    },
                                    "aggs": {
                                        "by_area": {
                                            "terms": {
                                                "field": "subject_areas",
                                                "size": 0
                                            },
                                            "aggs":{
                                                "by_year":{
                                                    "terms":{
                                                        "field":"publication_year",
                                                        "size":0
                                                    }
                                                }
                                            }
                                        },
                                        "by_language":{
                                            "terms":{
                                                "field":"languages","size":0
                                            },
                                            "aggs":{
                                                "by_year":{
                                                    "terms":{
                                                        "field":"publication_year",
                                                        "size":0
                                                    }
                                                }
                                            }
                                        },
                                        "by_affiliation":{
                                            "terms":{
                                                "field":"aff_countries",
                                                "size":0
                                            },
                                            "aggs":{
                                                "by_year":{
                                                    "terms":{
                                                        "field":"publication_year",
                                                        "size":0
                                                    }
                                                }
                                            }
                                        },
                                        "by_journal":{
                                            "terms":{
                                                "field":"journal_title",
                                                "size":0
                                            },
                                            "aggs":{
                                                "by_year":{
                                                    "terms":{
                                                        "field":"publication_year",
                                                        "size":0
                                                    }
                                                }
                                            }
                                        },
                                        "by_year":{
                                            "terms":{
                                                "field":"publication_year",
                                                "size":0
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            },
            "t2": {
                "text": "by thematic area",
                "query": {
                    "aggs": {
                        "by_all": {
                            "aggs":{ 
                                "by_area": {
                                    "terms": {
                                        "field": "subject_areas",
                                        "size": 0
                                    },
                                    "aggs": {
                                        "by_collection": {
                                            "terms": {
                                                "field": "collection",
                                                "size": 0
                                            },
                                            "aggs":{
                                                "by_year":{
                                                    "terms":{
                                                        "field":"publication_year",
                                                        "size":0
                                                    }
                                                }
                                            }
                                        },
                                        "by_language":{
                                            "terms":{
                                                "field":"languages","size":0
                                            },
                                            "aggs":{
                                                "by_year":{
                                                    "terms":{
                                                        "field":"publication_year",
                                                        "size":0
                                                    }
                                                }
                                            }
                                        },
                                        "by_affiliation":{
                                            "terms":{
                                                "field":"aff_countries",
                                                "size":0
                                            },
                                            "aggs":{
                                                "by_year":{
                                                    "terms":{
                                                        "field":"publication_year",
                                                        "size":0
                                                    }
                                                }
                                            }
                                        },
                                        "by_journal":{
                                            "terms":{
                                                "field":"journal_title",
                                                "size":0
                                            },
                                            "aggs":{
                                                "by_year":{
                                                    "terms":{
                                                        "field":"publication_year",
                                                        "size":0
                                                    }
                                                }
                                            }
                                        },
                                        "by_year":{
                                            "terms":{
                                                "field":"publication_year",
                                                "size":0
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            },
            "t4": {
                "text": "by language",
                "query": {
                    "aggs": {
                        "by_all": {
                            "aggs":{ 
                                "by_language": {
                                    "terms": {
                                        "field": "languages",
                                        "size": 0
                                    },
                                    "aggs": {
                                        "by_collection": {
                                            "terms": {
                                                "field": "collection",
                                                "size": 0
                                            },
                                            "aggs":{
                                                "by_year":{
                                                    "terms":{
                                                        "field":"publication_year",
                                                        "size":0
                                                    }
                                                }
                                            }
                                        },
                                        "by_area":{
                                            "terms":{
                                                "field":"subject_areas","size":0
                                            },
                                            "aggs":{
                                                "by_year":{
                                                    "terms":{
                                                        "field":"publication_year",
                                                        "size":0
                                                    }
                                                }
                                            }
                                        },
                                        "by_affiliation":{
                                            "terms":{
                                                "field":"aff_countries",
                                                "size":0
                                            },
                                            "aggs":{
                                                "by_year":{
                                                    "terms":{
                                                        "field":"publication_year",
                                                        "size":0
                                                    }
                                                }
                                            }
                                        },
                                        "by_journal":{
                                            "terms":{
                                                "field":"journal_title",
                                                "size":0
                                            },
                                            "aggs":{
                                                "by_year":{
                                                    "terms":{
                                                        "field":"publication_year",
                                                        "size":0
                                                    }
                                                }
                                            }
                                        },
                                        "by_year":{
                                            "terms":{
                                                "field":"publication_year",
                                                "size":0
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            },
            "t5": {
                "text": "by country of author's affiliation",
                "query": {
                    "aggs": {
                        "by_all": {
                            "aggs":{ 
                                "by_affiliation": {
                                    "terms": {
                                        "field": "aff_countries",
                                        "size": 0
                                    },
                                    "aggs": {
                                        "by_collection": {
                                            "terms": {
                                                "field": "collection",
                                                "size": 0
                                            },
                                            "aggs":{
                                                "by_year":{
                                                    "terms":{
                                                        "field":"publication_year",
                                                        "size":0
                                                    }
                                                }
                                            }
                                        },
                                        "by_language":{
                                            "terms":{
                                                "field":"languages","size":0
                                            },
                                            "aggs":{
                                                "by_year":{
                                                    "terms":{
                                                        "field":"publication_year",
                                                        "size":0
                                                    }
                                                }
                                            }
                                        },
                                        "by_area":{
                                            "terms":{
                                                "field":"subject_areas",
                                                "size":0
                                            },
                                            "aggs":{
                                                "by_year":{
                                                    "terms":{
                                                        "field":"publication_year",
                                                        "size":0
                                                    }
                                                }
                                            }
                                        },
                                        "by_journal":{
                                            "terms":{
                                                "field":"journal_title",
                                                "size":0
                                            },
                                            "aggs":{
                                                "by_year":{
                                                    "terms":{
                                                        "field":"publication_year",
                                                        "size":0
                                                    }
                                                }
                                            }
                                        },
                                        "by_year":{
                                            "terms":{
                                                "field":"publication_year",
                                                "size":0
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            },
            "t7": {
                "text": "by journals",
                "query": {
                    "aggs": {
                        "by_all": {
                            "aggs":{ 
                                "by_journal": {
                                    "terms": {
                                        "field": "journal_title",
                                        "size": 0
                                    },
                                    "aggs": {
                                        "by_collection": {
                                            "terms": {
                                                "field": "collection",
                                                "size": 0
                                            },
                                            "aggs":{
                                                "by_year":{
                                                    "terms":{
                                                        "field":"publication_year",
                                                        "size":0
                                                    }
                                                }
                                            }
                                        },
                                        "by_language":{
                                            "terms":{
                                                "field":"languages","size":0
                                            },
                                            "aggs":{
                                                "by_year":{
                                                    "terms":{
                                                        "field":"publication_year",
                                                        "size":0
                                                    }
                                                }
                                            }
                                        },
                                        "by_area":{
                                            "terms":{
                                                "field":"subject_areas",
                                                "size":0
                                            },
                                            "aggs":{
                                                "by_year":{
                                                    "terms":{
                                                        "field":"publication_year",
                                                        "size":0
                                                    }
                                                }
                                            }
                                        },
                                        "by_affiliation":{
                                            "terms":{
                                                "field":"aff_countries",
                                                "size":0
                                            },
                                            "aggs":{
                                                "by_year":{
                                                    "terms":{
                                                        "field":"publication_year",
                                                        "size":0
                                                    }
                                                }
                                            }
                                        },
                                        "by_year":{
                                            "terms":{
                                                "field":"publication_year",
                                                "size":0
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    },
    "2": {
        "text": "Relationships",
        "tabulations": {
            "r1": {
                "text": "Network graph",
                "query": {
                    "fields":[
                        "aff_countries",
                        "publication_year"
                    ]
                }
            },
            "r2": {
                "text": "Network collection",
                "query": {
                    "fields":[
                        "aff_countries",
                        "subject_areas"
                    ]
                }
            },
            "r3": {
                "text": "Network graph rings",
                "query": {
                    "fields":[
                        "aff_countries",
                        "collection"
                    ]
                }
            },
        }
    }
}