#!/usr/bin/env python
# -*- coding: utf-8 -*-
from app.v1.classes.filters import Filters
from datetime import datetime

def process_filters_form(form):
  filters = Filters()
  #Scope
  filters.scope = form.get('scope', '1')
  #Tabulation
  filters.tabulation = form.get('tabulation', '1')
  ##Visualization
  filters.visualization = form.get('visualization', 'treemap')
  #Years
  filters.years, filters.from_year, filters.to_year = 'ALL', 1900, datetime.now().year
  filters.years = form.get('years')
  if filters.years == 'ALL':
    filters.to_year = datetime.now().year
    filters.from_year = 1900
  elif filters.years == 'RANGE':
    filters.to_year = form.get('years[to]', datetime.now().year)
    filters.from_year = form.get('years[from]', min(filters.to_year, datetime.now().year))
  #Study Unit
  filters.study_unit = int(form.get('study_unit', 1))
  filters.collections = form.getlist('study_unit[collections][]')
  if 'ALL' in filters.collections:
    filters.collections = []
  filters.journals = form.getlist('study_unit[journals][]')
  if 'ALL' in filters.journals:
    filters.journals = []
  filters.document_type = form.get('study_unit[document-type]', None)
  filters.document_type_details = form.getlist('study_unit[document_type_details][]')
  if 'ALL' in filters.document_type_details:
    filters.document_type_details = []  
  #Thematic Areas
  filters.thematic_areas = form.getlist('thematic_areas[]')
  if 'ALL' in filters.thematic_areas:
    filters.thematic_areas = []
  filters.wos_thematic_areas = form.getlist('wos_thematic_areas[]')
  if 'ALL' in filters.wos_thematic_areas:
    filters.wos_thematic_areas = []
  #Languages
  filters.languages = form.getlist('languages[]')
  if u'ALL' in filters.languages:
    filters.languages = []
    
  return filters