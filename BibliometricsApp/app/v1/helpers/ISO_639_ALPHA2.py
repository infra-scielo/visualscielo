#!/usr/bin/env python
# -*- coding: utf-8 -*-
ISO_639_ALPHA2 = {
	"aa": {
		"alpha3-b": "aar",
		"english": "Afar"
	},
	"ab": {
		"alpha3-b": "abk",
		"english": "Abkhazian"
	},
	"af": {
		"alpha3-b": "afr",
		"english": "Afrikaans"
	},
	"ak": {
		"alpha3-b": "aka",
		"english": "Akan"
	},
	"sq": {
		"alpha3-b": "alb",
		"english": "Albanian"
	},
	"am": {
		"alpha3-b": "amh",
		"english": "Amharic"
	},
	"ar": {
		"alpha3-b": "ara",
		"english": "Arabic"
	},
	"an": {
		"alpha3-b": "arg",
		"english": "Aragonese"
	},
	"hy": {
		"alpha3-b": "arm",
		"english": "Armenian"
	},
	"as": {
		"alpha3-b": "asm",
		"english": "Assamese"
	},
	"av": {
		"alpha3-b": "ava",
		"english": "Avaric"
	},
	"ae": {
		"alpha3-b": "ave",
		"english": "Avestan"
	},
	"ay": {
		"alpha3-b": "aym",
		"english": "Aymara"
	},
	"az": {
		"alpha3-b": "aze",
		"english": "Azerbaijani"
	},
	"ba": {
		"alpha3-b": "bak",
		"english": "Bashkir"
	},
	"bm": {
		"alpha3-b": "bam",
		"english": "Bambara"
	},
	"eu": {
		"alpha3-b": "baq",
		"english": "Basque"
	},
	"be": {
		"alpha3-b": "bel",
		"english": "Belarusian"
	},
	"bn": {
		"alpha3-b": "ben",
		"english": "Bengali"
	},
	"bh": {
		"alpha3-b": "bih",
		"english": "Bihari languages"
	},
	"bi": {
		"alpha3-b": "bis",
		"english": "Bislama"
	},
	"bs": {
		"alpha3-b": "bos",
		"english": "Bosnian"
	},
	"br": {
		"alpha3-b": "bre",
		"english": "Breton"
	},
	"bg": {
		"alpha3-b": "bul",
		"english": "Bulgarian"
	},
	"my": {
		"alpha3-b": "bur",
		"english": "Burmese"
	},
	"ca": {
		"alpha3-b": "cat",
		"english": "Catalan - Valencian"
	},
	"ch": {
		"alpha3-b": "cha",
		"english": "Chamorro"
	},
	"ce": {
		"alpha3-b": "che",
		"english": "Chechen"
	},
	"zh": {
		"alpha3-b": "chi",
		"english": "Chinese"
	},
	"cu": {
		"alpha3-b": "chu",
		"english": "Church Slavic - Old Slavonic - Church Slavonic - Old Bulgarian - Old Church Slavonic"
	},
	"cv": {
		"alpha3-b": "chv",
		"english": "Chuvash"
	},
	"kw": {
		"alpha3-b": "cor",
		"english": "Cornish"
	},
	"co": {
		"alpha3-b": "cos",
		"english": "Corsican"
	},
	"cr": {
		"alpha3-b": "cre",
		"english": "Cree"
	},
	"cs": {
		"alpha3-b": "cze",
		"english": "Czech"
	},
	"da": {
		"alpha3-b": "dan",
		"english": "Danish"
	},
	"dv": {
		"alpha3-b": "div",
		"english": "Divehi - Dhivehi - Maldivian"
	},
	"nl": {
		"alpha3-b": "dut",
		"english": "Dutch - Flemish"
	},
	"dz": {
		"alpha3-b": "dzo",
		"english": "Dzongkha"
	},
	"en": {
		"alpha3-b": "eng",
		"english": "English"
	},
	"eo": {
		"alpha3-b": "epo",
		"english": "Esperanto"
	},
	"et": {
		"alpha3-b": "est",
		"english": "Estonian"
	},
	"ee": {
		"alpha3-b": "ewe",
		"english": "Ewe"
	},
	"fo": {
		"alpha3-b": "fao",
		"english": "Faroese"
	},
	"fj": {
		"alpha3-b": "fij",
		"english": "Fijian"
	},
	"fi": {
		"alpha3-b": "fin",
		"english": "Finnish"
	},
	"fr": {
		"alpha3-b": "fre",
		"english": "French"
	},
	"fy": {
		"alpha3-b": "fry",
		"english": "Western Frisian"
	},
	"ff": {
		"alpha3-b": "ful",
		"english": "Fulah"
	},
	"ka": {
		"alpha3-b": "geo",
		"english": "Georgian"
	},
	"de": {
		"alpha3-b": "ger",
		"english": "German"
	},
	"gd": {
		"alpha3-b": "gla",
		"english": "Gaelic - Scottish Gaelic"
	},
	"ga": {
		"alpha3-b": "gle",
		"english": "Irish"
	},
	"gl": {
		"alpha3-b": "glg",
		"english": "Galician"
	},
	"gv": {
		"alpha3-b": "glv",
		"english": "Manx"
	},
	"el": {
		"alpha3-b": "gre",
		"english": "Greek, Modern (1453-)"
	},
	"gn": {
		"alpha3-b": "grn",
		"english": "Guarani"
	},
	"gu": {
		"alpha3-b": "guj",
		"english": "Gujarati"
	},
	"ht": {
		"alpha3-b": "hat",
		"english": "Haitian - Haitian Creole"
	},
	"ha": {
		"alpha3-b": "hau",
		"english": "Hausa"
	},
	"he": {
		"alpha3-b": "heb",
		"english": "Hebrew"
	},
	"hz": {
		"alpha3-b": "her",
		"english": "Herero"
	},
	"hi": {
		"alpha3-b": "hin",
		"english": "Hindi"
	},
	"ho": {
		"alpha3-b": "hmo",
		"english": "Hiri Motu"
	},
	"hr": {
		"alpha3-b": "hrv",
		"english": "Croatian"
	},
	"hu": {
		"alpha3-b": "hun",
		"english": "Hungarian"
	},
	"ig": {
		"alpha3-b": "ibo",
		"english": "Igbo"
	},
	"is": {
		"alpha3-b": "ice",
		"english": "Icelandic"
	},
	"io": {
		"alpha3-b": "ido",
		"english": "Ido"
	},
	"ii": {
		"alpha3-b": "iii",
		"english": "Sichuan Yi - Nuosu"
	},
	"iu": {
		"alpha3-b": "iku",
		"english": "Inuktitut"
	},
	"ie": {
		"alpha3-b": "ile",
		"english": "Interlingue - Occidental"
	},
	"ia": {
		"alpha3-b": "ina",
		"english": "Interlingua (International Auxiliary Language Association)"
	},
	"id": {
		"alpha3-b": "ind",
		"english": "Indonesian"
	},
	"ik": {
		"alpha3-b": "ipk",
		"english": "Inupiaq"
	},
	"it": {
		"alpha3-b": "ita",
		"english": "Italian"
	},
	"jv": {
		"alpha3-b": "jav",
		"english": "Javanese"
	},
	"ja": {
		"alpha3-b": "jpn",
		"english": "Japanese"
	},
	"kl": {
		"alpha3-b": "kal",
		"english": "Kalaallisut - Greenlandic"
	},
	"kn": {
		"alpha3-b": "kan",
		"english": "Kannada"
	},
	"ks": {
		"alpha3-b": "kas",
		"english": "Kashmiri"
	},
	"kr": {
		"alpha3-b": "kau",
		"english": "Kanuri"
	},
	"kk": {
		"alpha3-b": "kaz",
		"english": "Kazakh"
	},
	"km": {
		"alpha3-b": "khm",
		"english": "Central Khmer"
	},
	"ki": {
		"alpha3-b": "kik",
		"english": "Kikuyu - Gikuyu"
	},
	"rw": {
		"alpha3-b": "kin",
		"english": "Kinyarwanda"
	},
	"ky": {
		"alpha3-b": "kir",
		"english": "Kirghiz - Kyrgyz"
	},
	"kv": {
		"alpha3-b": "kom",
		"english": "Komi"
	},
	"kg": {
		"alpha3-b": "kon",
		"english": "Kongo"
	},
	"ko": {
		"alpha3-b": "kor",
		"english": "Korean"
	},
	"kj": {
		"alpha3-b": "kua",
		"english": "Kuanyama - Kwanyama"
	},
	"ku": {
		"alpha3-b": "kur",
		"english": "Kurdish"
	},
	"lo": {
		"alpha3-b": "lao",
		"english": "Lao"
	},
	"la": {
		"alpha3-b": "lat",
		"english": "Latin"
	},
	"lv": {
		"alpha3-b": "lav",
		"english": "Latvian"
	},
	"li": {
		"alpha3-b": "lim",
		"english": "Limburgan - Limburger - Limburgish"
	},
	"ln": {
		"alpha3-b": "lin",
		"english": "Lingala"
	},
	"lt": {
		"alpha3-b": "lit",
		"english": "Lithuanian"
	},
	"lb": {
		"alpha3-b": "ltz",
		"english": "Luxembourgish - Letzeburgesch"
	},
	"lu": {
		"alpha3-b": "lub",
		"english": "Luba-Katanga"
	},
	"lg": {
		"alpha3-b": "lug",
		"english": "Ganda"
	},
	"mk": {
		"alpha3-b": "mac",
		"english": "Macedonian"
	},
	"mh": {
		"alpha3-b": "mah",
		"english": "Marshallese"
	},
	"ml": {
		"alpha3-b": "mal",
		"english": "Malayalam"
	},
	"mi": {
		"alpha3-b": "mao",
		"english": "Maori"
	},
	"mr": {
		"alpha3-b": "mar",
		"english": "Marathi"
	},
	"ms": {
		"alpha3-b": "may",
		"english": "Malay"
	},
	"mg": {
		"alpha3-b": "mlg",
		"english": "Malagasy"
	},
	"mt": {
		"alpha3-b": "mlt",
		"english": "Maltese"
	},
	"mn": {
		"alpha3-b": "mon",
		"english": "Mongolian"
	},
	"na": {
		"alpha3-b": "nau",
		"english": "Nauru"
	},
	"nv": {
		"alpha3-b": "nav",
		"english": "Navajo - Navaho"
	},
	"nr": {
		"alpha3-b": "nbl",
		"english": "Ndebele, South - South Ndebele"
	},
	"nd": {
		"alpha3-b": "nde",
		"english": "North Ndebele"
	},
	"ng": {
		"alpha3-b": "ndo",
		"english": "Ndonga"
	},
	"ne": {
		"alpha3-b": "nep",
		"english": "Nepali"
	},
	"nn": {
		"alpha3-b": "nno",
		"english": "Norwegian Nynorsk - Nynorsk, Norwegian"
	},
	"nb": {
		"alpha3-b": "nob",
		"english": "Bokmål, Norwegian - Norwegian Bokmål"
	},
	"no": {
		"alpha3-b": "nor",
		"english": "Norwegian"
	},
	"ny": {
		"alpha3-b": "nya",
		"english": "Chichewa - Chewa - Nyanja"
	},
	"oc": {
		"alpha3-b": "oci",
		"english": "Occitan (post 1500) - Provençal"
	},
	"oj": {
		"alpha3-b": "oji",
		"english": "Ojibwa"
	},
	"or": {
		"alpha3-b": "ori",
		"english": "Oriya"
	},
	"om": {
		"alpha3-b": "orm",
		"english": "Oromo"
	},
	"os": {
		"alpha3-b": "oss",
		"english": "Ossetian - Ossetic"
	},
	"pa": {
		"alpha3-b": "pan",
		"english": "Panjabi - Punjabi"
	},
	"fa": {
		"alpha3-b": "per",
		"english": "Persian"
	},
	"pi": {
		"alpha3-b": "pli",
		"english": "Pali"
	},
	"pl": {
		"alpha3-b": "pol",
		"english": "Polish"
	},
	"pt": {
		"alpha3-b": "por",
		"english": "Portuguese"
	},
	"ps": {
		"alpha3-b": "pus",
		"english": "Pushto - Pashto"
	},
	"qu": {
		"alpha3-b": "que",
		"english": "Quechua"
	},
	"rm": {
		"alpha3-b": "roh",
		"english": "Romansh"
	},
	"ro": {
		"alpha3-b": "rum",
		"english": "Romanian - Moldavian - Moldovan"
	},
	"rn": {
		"alpha3-b": "run",
		"english": "Rundi"
	},
	"ru": {
		"alpha3-b": "rus",
		"english": "Russian"
	},
	"sg": {
		"alpha3-b": "sag",
		"english": "Sango"
	},
	"sa": {
		"alpha3-b": "san",
		"english": "Sanskrit"
	},
	"si": {
		"alpha3-b": "sin",
		"english": "Sinhala - Sinhalese"
	},
	"sk": {
		"alpha3-b": "slo",
		"english": "Slovak"
	},
	"sl": {
		"alpha3-b": "slv",
		"english": "Slovenian"
	},
	"se": {
		"alpha3-b": "sme",
		"english": "Northern Sami"
	},
	"sm": {
		"alpha3-b": "smo",
		"english": "Samoan"
	},
	"sn": {
		"alpha3-b": "sna",
		"english": "Shona"
	},
	"sd": {
		"alpha3-b": "snd",
		"english": "Sindhi"
	},
	"so": {
		"alpha3-b": "som",
		"english": "Somali"
	},
	"st": {
		"alpha3-b": "sot",
		"english": "Sotho, Southern"
	},
	"es": {
		"alpha3-b": "spa",
		"english": "Spanish - Castilian"
	},
	"sc": {
		"alpha3-b": "srd",
		"english": "Sardinian"
	},
	"sr": {
		"alpha3-b": "srp",
		"english": "Serbian"
	},
	"ss": {
		"alpha3-b": "ssw",
		"english": "Swati"
	},
	"su": {
		"alpha3-b": "sun",
		"english": "Sundanese"
	},
	"sw": {
		"alpha3-b": "swa",
		"english": "Swahili"
	},
	"sv": {
		"alpha3-b": "swe",
		"english": "Swedish"
	},
	"ty": {
		"alpha3-b": "tah",
		"english": "Tahitian"
	},
	"ta": {
		"alpha3-b": "tam",
		"english": "Tamil"
	},
	"tt": {
		"alpha3-b": "tat",
		"english": "Tatar"
	},
	"te": {
		"alpha3-b": "tel",
		"english": "Telugu"
	},
	"tg": {
		"alpha3-b": "tgk",
		"english": "Tajik"
	},
	"tl": {
		"alpha3-b": "tgl",
		"english": "Tagalog"
	},
	"th": {
		"alpha3-b": "tha",
		"english": "Thai"
	},
	"bo": {
		"alpha3-b": "tib",
		"english": "Tibetan"
	},
	"ti": {
		"alpha3-b": "tir",
		"english": "Tigrinya"
	},
	"to": {
		"alpha3-b": "ton",
		"english": "Tonga (Tonga Islands)"
	},
	"tn": {
		"alpha3-b": "tsn",
		"english": "Tswana"
	},
	"ts": {
		"alpha3-b": "tso",
		"english": "Tsonga"
	},
	"tk": {
		"alpha3-b": "tuk",
		"english": "Turkmen"
	},
	"tr": {
		"alpha3-b": "tur",
		"english": "Turkish"
	},
	"tw": {
		"alpha3-b": "twi",
		"english": "Twi"
	},
	"ug": {
		"alpha3-b": "uig",
		"english": "Uighur - Uyghur"
	},
	"uk": {
		"alpha3-b": "ukr",
		"english": "Ukrainian"
	},
	"ur": {
		"alpha3-b": "urd",
		"english": "Urdu"
	},
	"uz": {
		"alpha3-b": "uzb",
		"english": "Uzbek"
	},
	"ve": {
		"alpha3-b": "ven",
		"english": "Venda"
	},
	"vi": {
		"alpha3-b": "vie",
		"english": "Vietnamese"
	},
	"vo": {
		"alpha3-b": "vol",
		"english": "Volapük"
	},
	"cy": {
		"alpha3-b": "wel",
		"english": "Welsh"
	},
	"wa": {
		"alpha3-b": "wln",
		"english": "Walloon"
	},
	"wo": {
		"alpha3-b": "wol",
		"english": "Wolof"
	},
	"xh": {
		"alpha3-b": "xho",
		"english": "Xhosa"
	},
	"yi": {
		"alpha3-b": "yid",
		"english": "Yiddish"
	},
	"yo": {
		"alpha3-b": "yor",
		"english": "Yoruba"
	},
	"za": {
		"alpha3-b": "zha",
		"english": "Zhuang - Chuang"
	},
	"zu": {
		"alpha3-b": "zul",
		"english": "Zulu"
	}
}