#!/usr/bin/env python
# -*- coding: utf-8 -*-
ISO_3166_ALPHA3 = {
	"ALB": {
		"code": "eualb",
		"alpha2": "AL",
		"continent": "eu",
		"fullname": "Albania"
	},
	"AND": {
		"code": "euand",
		"alpha2": "AD",
		"continent": "eu",
		"fullname": "Andorra"
	},
	"AUT": {
		"code": "euaut",
		"alpha2": "AT",
		"continent": "eu",
		"fullname": "Austria"
	},
	"BLR": {
		"code": "eublr",
		"alpha2": "BY",
		"continent": "eu",
		"fullname": "Belarus"
	},
	"BEL": {
		"code": "eubel",
		"alpha2": "BE",
		"continent": "eu",
		"fullname": "Belgium"
	},
	"BIH": {
		"code": "eubih",
		"alpha2": "BA",
		"continent": "eu",
		"fullname": "Bosnia and Herzegovina"
	},
	"BGR": {
		"code": "eubgr",
		"alpha2": "BG",
		"continent": "eu",
		"fullname": "Bulgaria"
	},
	"HRV": {
		"code": "euhrv",
		"alpha2": "HR",
		"continent": "eu",
		"fullname": "Croatia"
	},
	"CYP": {
		"code": "eucyp",
		"alpha2": "CY",
		"continent": "eu",
		"fullname": "Cyprus"
	},
	"CZE": {
		"code": "eucze",
		"alpha2": "CZ",
		"continent": "eu",
		"fullname": "Czech Republic"
	},
	"DNK": {
		"code": "eudnk",
		"alpha2": "DK",
		"continent": "eu",
		"fullname": "Denmark"
	},
	"EST": {
		"code": "euest",
		"alpha2": "EE",
		"continent": "eu",
		"fullname": "Estonia"
	},
	"FRO": {
		"code": "eufro",
		"alpha2": "FO",
		"continent": "eu",
		"fullname": "Faroe Islands"
	},
	"FIN": {
		"code": "eufin",
		"alpha2": "FI",
		"continent": "eu",
		"fullname": "Finland"
	},
	"FRA": {
		"code": "eufra",
		"alpha2": "FR",
		"continent": "eu",
		"fullname": "France"
	},
	"DEU": {
		"code": "eudeu",
		"alpha2": "DE",
		"continent": "eu",
		"fullname": "Germany"
	},
	"GIB": {
		"code": "eugib",
		"alpha2": "GI",
		"continent": "eu",
		"fullname": "Gibraltar"
	},
	"GRC": {
		"code": "eugrc",
		"alpha2": "GR",
		"continent": "eu",
		"fullname": "Greece"
	},
	"HUN": {
		"code": "euhun",
		"alpha2": "HU",
		"continent": "eu",
		"fullname": "Hungary"
	},
	"ISL": {
		"code": "euisl",
		"alpha2": "IS",
		"continent": "eu",
		"fullname": "Iceland"
	},
	"IRL": {
		"code": "euirl",
		"alpha2": "IE",
		"continent": "eu",
		"fullname": "Ireland"
	},
	"ITA": {
		"code": "euita",
		"alpha2": "IT",
		"continent": "eu",
		"fullname": "Italy"
	},
	"LVA": {
		"code": "eulva",
		"alpha2": "LV",
		"continent": "eu",
		"fullname": "Latvia"
	},
	"LIE": {
		"code": "eulie",
		"alpha2": "LI",
		"continent": "eu",
		"fullname": "Liechtenstein"
	},
	"LTU": {
		"code": "eultu",
		"alpha2": "LT",
		"continent": "eu",
		"fullname": "Lithuania"
	},
	"LUX": {
		"code": "eulux",
		"alpha2": "LU",
		"continent": "eu",
		"fullname": "Luxembourg"
	},
	"MKD": {
		"code": "eumkd",
		"alpha2": "MK",
		"continent": "eu",
		"fullname": "Macedonia"
	},
	"MLT": {
		"code": "eumlt",
		"alpha2": "MT",
		"continent": "eu",
		"fullname": "Malta"
	},
	"MDA": {
		"code": "eumda",
		"alpha2": "MD",
		"continent": "eu",
		"fullname": "Moldova"
	},
	"MCO": {
		"code": "eumco",
		"alpha2": "MC",
		"continent": "eu",
		"fullname": "Monaco"
	},
	"NLD": {
		"code": "eunld",
		"alpha2": "NL",
		"continent": "eu",
		"fullname": "Netherlands"
	},
	"NOR": {
		"code": "eunor",
		"alpha2": "NO",
		"continent": "eu",
		"fullname": "Norway"
	},
	"POL": {
		"code": "eupol",
		"alpha2": "PL",
		"continent": "eu",
		"fullname": "Poland"
	},
	"PRT": {
		"code": "euprt",
		"alpha2": "PT",
		"continent": "eu",
		"fullname": "Portugal"
	},
	"ROU": {
		"code": "eurou",
		"alpha2": "RO",
		"continent": "eu",
		"fullname": "Romania"
	},
	"RUS": {
		"code": "eurus",
		"alpha2": "RU",
		"continent": "eu",
		"fullname": "Russia"
	},
	"SMR": {
		"code": "eusmr",
		"alpha2": "SM",
		"continent": "eu",
		"fullname": "San Marino"
	},
	"SRB": {
		"code": "eusrb",
		"alpha2": "RS",
		"continent": "eu",
		"fullname": "Serbia"
	},
	"SVK": {
		"code": "eusvk",
		"alpha2": "SK",
		"continent": "eu",
		"fullname": "Slovakia"
	},
	"SVN": {
		"code": "eusvn",
		"alpha2": "SI",
		"continent": "eu",
		"fullname": "Slovenia"
	},
	"ESP": {
		"code": "euesp",
		"alpha2": "ES",
		"continent": "eu",
		"fullname": "Spain"
	},
	"SWE": {
		"code": "euswe",
		"alpha2": "SE",
		"continent": "eu",
		"fullname": "Sweden"
	},
	"CHE": {
		"code": "euche",
		"alpha2": "CH",
		"continent": "eu",
		"fullname": "Switzerland"
	},
	"UKR": {
		"code": "euukr",
		"alpha2": "UA",
		"continent": "eu",
		"fullname": "Ukraine"
	},
	"GBR": {
		"code": "eugbr",
		"alpha2": "GB",
		"continent": "eu",
		"fullname": "United Kingdom"
	},
	"VAT": {
		"code": "euvat",
		"alpha2": "VA",
		"continent": "eu",
		"fullname": "Vatican city"
	},
	"RSB": {
		"code": "eursb",
		"alpha2": "RS",
		"continent": "eu",
		"fullname": "Yugoslavia"
	},
	"IMN": {
		"code": "euimn",
		"alpha2": "IM",
		"continent": "eu",
		"fullname": "Isle of Man"
	},
	"MNE": {
		"code": "eumne",
		"alpha2": "ME",
		"continent": "eu",
		"fullname": "Montenegro"
	},
	"DZA": {
		"code": "afdza",
		"alpha2": "DZ",
		"continent": "af",
		"fullname": "Algeria"
	},
	"AGO": {
		"code": "afago",
		"alpha2": "AO",
		"continent": "af",
		"fullname": "Angola"
	},
	"SHN": {
		"code": "afshn",
		"alpha2": "SH",
		"continent": "af",
		"fullname": "Ascension"
	},
	"BEN": {
		"code": "afben",
		"alpha2": "BJ",
		"continent": "af",
		"fullname": "Benin"
	},
	"BWA": {
		"code": "afbwa",
		"alpha2": "BW",
		"continent": "af",
		"fullname": "Botswana"
	},
	"BFA": {
		"code": "afbfa",
		"alpha2": "BF",
		"continent": "af",
		"fullname": "Burkina Faso"
	},
	"BDI": {
		"code": "afbdi",
		"alpha2": "BI",
		"continent": "af",
		"fullname": "Burundi"
	},
	"CMR": {
		"code": "afcmr",
		"alpha2": "CM",
		"continent": "af",
		"fullname": "Cameroon"
	},
	"CPV": {
		"code": "afcpv",
		"alpha2": "CV",
		"continent": "af",
		"fullname": "Cape Verde Islands"
	},
	"CAF": {
		"code": "afcaf",
		"alpha2": "CF",
		"continent": "af",
		"fullname": "Central African Republic"
	},
	"TCD": {
		"code": "aftcd",
		"alpha2": "TD",
		"continent": "af",
		"fullname": "Chad Republic"
	},
	"COM": {
		"code": "afcom",
		"alpha2": "KM",
		"continent": "af",
		"fullname": "Comoros"
	},
	"COG": {
		"code": "afcog",
		"alpha2": "CG",
		"continent": "af",
		"fullname": "Congo"
	},
	"DJI": {
		"code": "afdji",
		"alpha2": "DJ",
		"continent": "af",
		"fullname": "Djibouti"
	},
	"EGY": {
		"code": "afegy",
		"alpha2": "EG",
		"continent": "af",
		"fullname": "Egypt"
	},
	"GNQ": {
		"code": "afgnq",
		"alpha2": "GQ",
		"continent": "af",
		"fullname": "Equatorial Guinea"
	},
	"ERI": {
		"code": "aferi",
		"alpha2": "ER",
		"continent": "af",
		"fullname": "Eritrea"
	},
	"ETH": {
		"code": "afeth",
		"alpha2": "ET",
		"continent": "af",
		"fullname": "Ethiopia"
	},
	"GAB": {
		"code": "afgab",
		"alpha2": "GA",
		"continent": "af",
		"fullname": "Gabon Republic"
	},
	"GMB": {
		"code": "afgmb",
		"alpha2": "GM",
		"continent": "af",
		"fullname": "Gambia"
	},
	"GHA": {
		"code": "afgha",
		"alpha2": "GH",
		"continent": "af",
		"fullname": "Ghana"
	},
	"GNB": {
		"code": "afgnb",
		"alpha2": "GW",
		"continent": "af",
		"fullname": "Guinea-Bissau"
	},
	"GIN": {
		"code": "afgin",
		"alpha2": "GN",
		"continent": "af",
		"fullname": "Guinea"
	},
	"CIV": {
		"code": "afciv",
		"alpha2": "CI",
		"continent": "af",
		"fullname": "Ivory Coast"
	},
	"KEN": {
		"code": "afken",
		"alpha2": "KE",
		"continent": "af",
		"fullname": "Kenya"
	},
	"LSO": {
		"code": "aflso",
		"alpha2": "LS",
		"continent": "af",
		"fullname": "Lesotho"
	},
	"LBR": {
		"code": "aflbr",
		"alpha2": "LR",
		"continent": "af",
		"fullname": "Liberia"
	},
	"LBY": {
		"code": "aflby",
		"alpha2": "LY",
		"continent": "af",
		"fullname": "Libya"
	},
	"MDG": {
		"code": "afmdg",
		"alpha2": "MG",
		"continent": "af",
		"fullname": "Madagascar"
	},
	"MWI": {
		"code": "afmwi",
		"alpha2": "MW",
		"continent": "af",
		"fullname": "Malawi"
	},
	"MLI": {
		"code": "afmli",
		"alpha2": "ML",
		"continent": "af",
		"fullname": "Mali Republic"
	},
	"MRT": {
		"code": "afmrt",
		"alpha2": "MR",
		"continent": "af",
		"fullname": "Mauritania"
	},
	"MUS": {
		"code": "afmus",
		"alpha2": "MU",
		"continent": "af",
		"fullname": "Mauritius"
	},
	"MYT": {
		"code": "afmyt",
		"alpha2": "YT",
		"continent": "af",
		"fullname": "Mayotte Island"
	},
	"MAR": {
		"code": "afmar",
		"alpha2": "MA",
		"continent": "af",
		"fullname": "Morocco"
	},
	"MOZ": {
		"code": "afmoz",
		"alpha2": "MZ",
		"continent": "af",
		"fullname": "Mozambique"
	},
	"NAM": {
		"code": "afnam",
		"alpha2": "NA",
		"continent": "af",
		"fullname": "Namibia"
	},
	"NER": {
		"code": "afner",
		"alpha2": "NE",
		"continent": "af",
		"fullname": "Niger Republic"
	},
	"NGA": {
		"code": "afnga",
		"alpha2": "NG",
		"continent": "af",
		"fullname": "Nigeria"
	},
	"STP": {
		"code": "afstp",
		"alpha2": "ST",
		"continent": "af",
		"fullname": "Principe"
	},
	"REU": {
		"code": "afreu",
		"alpha2": "RE",
		"continent": "af",
		"fullname": "Reunion Island"
	},
	"RWA": {
		"code": "afrwa",
		"alpha2": "RW",
		"continent": "af",
		"fullname": "Rwanda"
	},
	"STP": {
		"code": "afstp",
		"alpha2": "ST",
		"continent": "af",
		"fullname": "Sao Tome"
	},
	"SEN": {
		"code": "afsen",
		"alpha2": "SN",
		"continent": "af",
		"fullname": "Senegal Republic"
	},
	"SYC": {
		"code": "afsyc",
		"alpha2": "SC",
		"continent": "af",
		"fullname": "Seychelles"
	},
	"SLE": {
		"code": "afsle",
		"alpha2": "SL",
		"continent": "af",
		"fullname": "Sierra Leone"
	},
	"SOM": {
		"code": "afsom",
		"alpha2": "SO",
		"continent": "af",
		"fullname": "Somalia Republic"
	},
	"ZAF": {
		"code": "afzaf",
		"alpha2": "ZA",
		"continent": "af",
		"fullname": "South Africa"
	},
	"SHN": {
		"code": "afshn",
		"alpha2": "SH",
		"continent": "af",
		"fullname": "St. Helena"
	},
	"SDN": {
		"code": "afsdn",
		"alpha2": "SD",
		"continent": "af",
		"fullname": "Sudan"
	},
	"SWZ": {
		"code": "afswz",
		"alpha2": "SZ",
		"continent": "af",
		"fullname": "Swaziland"
	},
	"TZA": {
		"code": "aftza",
		"alpha2": "TZ",
		"continent": "af",
		"fullname": "Tanzania"
	},
	"TGO": {
		"code": "aftgo",
		"alpha2": "TG",
		"continent": "af",
		"fullname": "Togo"
	},
	"TUN": {
		"code": "aftun",
		"alpha2": "TN",
		"continent": "af",
		"fullname": "Tunisia"
	},
	"UGA": {
		"code": "afuga",
		"alpha2": "UG",
		"continent": "af",
		"fullname": "Uganda"
	},
	"COD": {
		"code": "afcod",
		"alpha2": "CD",
		"continent": "af",
		"fullname": "Zaire"
	},
	"ZMB": {
		"code": "afzmb",
		"alpha2": "ZM",
		"continent": "af",
		"fullname": "Zambia"
	},
	"TZA": {
		"code": "aftza",
		"alpha2": "TZ",
		"continent": "af",
		"fullname": "Zanzibar"
	},
	"ZWE": {
		"code": "afzwe",
		"alpha2": "ZW",
		"continent": "af",
		"fullname": "Zimbabwe"
	},
	"SSD": {
		"code": "afssd",
		"alpha2": "SS",
		"continent": "af",
		"fullname": "South Sudan"
	},
	"COD": {
		"code": "afcod",
		"alpha2": "CD",
		"continent": "af",
		"fullname": "Dem. Republic of the Congo"
	},
	"ATA": {
		"code": "anata",
		"alpha2": "AQ",
		"continent": "an",
		"fullname": "Antarctica"
	},
	"AIA": {
		"code": "naaia",
		"alpha2": "AI",
		"continent": "na",
		"fullname": "Anguilla"
	},
	"ATG": {
		"code": "naatg",
		"alpha2": "AG",
		"continent": "na",
		"fullname": "Antigua and Barbuda"
	},
	"ABW": {
		"code": "naabw",
		"alpha2": "AW",
		"continent": "na",
		"fullname": "Aruba"
	},
	"BHS": {
		"code": "nabhs",
		"alpha2": "BS",
		"continent": "na",
		"fullname": "Bahamas"
	},
	"BRB": {
		"code": "nabrb",
		"alpha2": "BB",
		"continent": "na",
		"fullname": "Barbados"
	},
	"BLZ": {
		"code": "nablz",
		"alpha2": "BZ",
		"continent": "na",
		"fullname": "Belize"
	},
	"BMU": {
		"code": "nabmu",
		"alpha2": "BM",
		"continent": "na",
		"fullname": "Bermuda"
	},
	"VGB": {
		"code": "navgb",
		"alpha2": "VG",
		"continent": "na",
		"fullname": "British Virgin Islands"
	},
	"CAN": {
		"code": "nacan",
		"alpha2": "CA",
		"continent": "na",
		"fullname": "Canada"
	},
	"CYM": {
		"code": "nacym",
		"alpha2": "KY",
		"continent": "na",
		"fullname": "Cayman Islands"
	},
	"CRI": {
		"code": "nacri",
		"alpha2": "CR",
		"continent": "na",
		"fullname": "Costa Rica"
	},
	"CUB": {
		"code": "nacub",
		"alpha2": "CU",
		"continent": "na",
		"fullname": "Cuba"
	},
	"CUW": {
		"code": "nacuw",
		"alpha2": "CW",
		"continent": "na",
		"fullname": "Curacao"
	},
	"DMA": {
		"code": "nadma",
		"alpha2": "DM",
		"continent": "na",
		"fullname": "Dominica"
	},
	"DOM": {
		"code": "nadom",
		"alpha2": "DO",
		"continent": "na",
		"fullname": "Dominican Republic"
	},
	"SLV": {
		"code": "naslv",
		"alpha2": "SV",
		"continent": "na",
		"fullname": "El Salvador"
	},
	"GRL": {
		"code": "nagrl",
		"alpha2": "GL",
		"continent": "na",
		"fullname": "Greenland"
	},
	"GRD": {
		"code": "nagrd",
		"alpha2": "GD",
		"continent": "na",
		"fullname": "Grenada and Carriacuou"
	},
	"GLP": {
		"code": "naglp",
		"alpha2": "GP",
		"continent": "na",
		"fullname": "Guadeloupe"
	},
	"GTM": {
		"code": "nagtm",
		"alpha2": "GT",
		"continent": "na",
		"fullname": "Guatemala"
	},
	"HTI": {
		"code": "nahti",
		"alpha2": "HT",
		"continent": "na",
		"fullname": "Haiti"
	},
	"HND": {
		"code": "nahnd",
		"alpha2": "HN",
		"continent": "na",
		"fullname": "Honduras"
	},
	"JAM": {
		"code": "najam",
		"alpha2": "JM",
		"continent": "na",
		"fullname": "Jamaica"
	},
	"MTQ": {
		"code": "namtq",
		"alpha2": "MQ",
		"continent": "na",
		"fullname": "Martinique"
	},
	"MEX": {
		"code": "namex",
		"alpha2": "MX",
		"continent": "na",
		"fullname": "Mexico"
	},
	"SPM": {
		"code": "naspm",
		"alpha2": "PM",
		"continent": "na",
		"fullname": "Miquelon"
	},
	"MSR": {
		"code": "namsr",
		"alpha2": "MS",
		"continent": "na",
		"fullname": "Montserrat"
	},
	"ANT": {
		"code": "naant",
		"alpha2": "CW",
		"continent": "na",
		"fullname": "Netherlands Antilles"
	},
	"KNA": {
		"code": "nakna",
		"alpha2": "KN",
		"continent": "na",
		"fullname": "Nevis"
	},
	"NIC": {
		"code": "nanic",
		"alpha2": "NI",
		"continent": "na",
		"fullname": "Nicaragua"
	},
	"PAN": {
		"code": "napan",
		"alpha2": "PA",
		"continent": "na",
		"fullname": "Panama"
	},
	"PRI": {
		"code": "napri",
		"alpha2": "PR",
		"continent": "na",
		"fullname": "Puerto Rico"
	},
	"KNA": {
		"code": "nakna",
		"alpha2": "KN",
		"continent": "na",
		"fullname": "St. Kitts"
	},
	"LCA": {
		"code": "nalca",
		"alpha2": "LC",
		"continent": "na",
		"fullname": "St. Lucia"
	},
	"SPM": {
		"code": "naspm",
		"alpha2": "PM",
		"continent": "na",
		"fullname": "St. Pierre and Miquelon"
	},
	"VCT": {
		"code": "navct",
		"alpha2": "VC",
		"continent": "na",
		"fullname": "St. Vincent"
	},
	"TTO": {
		"code": "natto",
		"alpha2": "TT",
		"continent": "na",
		"fullname": "Trinidad and Tobago"
	},
	"TCA": {
		"code": "natca",
		"alpha2": "TC",
		"continent": "na",
		"fullname": "Turks and Caicos Islands"
	},
	"VIR": {
		"code": "navir",
		"alpha2": "VI",
		"continent": "na",
		"fullname": "US Virgin Islands"
	},
	"USA": {
		"code": "nausa",
		"alpha2": "US",
		"continent": "na",
		"fullname": "United States"
	},
	"SXM": {
		"code": "nasxm",
		"alpha2": "SX",
		"continent": "na",
		"fullname": "Sint Maarten"
	},
	"BES": {
		"code": "nabes",
		"alpha2": "BQ",
		"continent": "na",
		"fullname": "Bonaire"
	},
	"BES": {
		"code": "nabes",
		"alpha2": "SA",
		"continent": "na",
		"fullname": "Saba"
	},
	"BES": {
		"code": "nabes",
		"alpha2": "SE",
		"continent": "na",
		"fullname": "Sint Eustatius"
	},
	"AFG": {
		"code": "asafg",
		"alpha2": "AF",
		"continent": "as",
		"fullname": "Afghanistan"
	},
	"ARM": {
		"code": "asarm",
		"alpha2": "AM",
		"continent": "as",
		"fullname": "Armenia"
	},
	"AZE": {
		"code": "asaze",
		"alpha2": "AZ",
		"continent": "as",
		"fullname": "Azerbaijan"
	},
	"BHR": {
		"code": "asbhr",
		"alpha2": "BH",
		"continent": "as",
		"fullname": "Bahrain"
	},
	"BGD": {
		"code": "asbgd",
		"alpha2": "BD",
		"continent": "as",
		"fullname": "Bangladesh"
	},
	"BTN": {
		"code": "asbtn",
		"alpha2": "BT",
		"continent": "as",
		"fullname": "Bhutan"
	},
	"BRN": {
		"code": "asbrn",
		"alpha2": "BN",
		"continent": "as",
		"fullname": "Brunei"
	},
	"KHM": {
		"code": "askhm",
		"alpha2": "KH",
		"continent": "as",
		"fullname": "Cambodia"
	},
	"CHN": {
		"code": "aschn",
		"alpha2": "CN",
		"continent": "as",
		"fullname": "China"
	},
	"CXR": {
		"code": "ascxr",
		"alpha2": "CX",
		"continent": "as",
		"fullname": "Christmas Island"
	},
	"CCK": {
		"code": "ascck",
		"alpha2": "CC",
		"continent": "as",
		"fullname": "Cocos Islands"
	},
	"IOT": {
		"code": "asiot",
		"alpha2": "IO",
		"continent": "as",
		"fullname": "Diego Garcia"
	},
	"GEO": {
		"code": "asgeo",
		"alpha2": "GE",
		"continent": "as",
		"fullname": "Georgia"
	},
	"HKG": {
		"code": "ashkg",
		"alpha2": "HK",
		"continent": "as",
		"fullname": "Hong Kong"
	},
	"IND": {
		"code": "asind",
		"alpha2": "IN",
		"continent": "as",
		"fullname": "India"
	},
	"IDN": {
		"code": "asidn",
		"alpha2": "ID",
		"continent": "as",
		"fullname": "Indonesia"
	},
	"IRN": {
		"code": "asirn",
		"alpha2": "IR",
		"continent": "as",
		"fullname": "Iran"
	},
	"IRQ": {
		"code": "asirq",
		"alpha2": "IQ",
		"continent": "as",
		"fullname": "Iraq"
	},
	"ISR": {
		"code": "asisr",
		"alpha2": "IL",
		"continent": "as",
		"fullname": "Israel"
	},
	"JPN": {
		"code": "asjpn",
		"alpha2": "JP",
		"continent": "as",
		"fullname": "Japan"
	},
	"JOR": {
		"code": "asjor",
		"alpha2": "JO",
		"continent": "as",
		"fullname": "Jordan"
	},
	"KAZ": {
		"code": "askaz",
		"alpha2": "KZ",
		"continent": "as",
		"fullname": "Kazakhstan"
	},
	"PRK": {
		"code": "asprk",
		"alpha2": "KP",
		"continent": "as",
		"fullname": "North Korea"
	},
	"KOR": {
		"code": "askor",
		"alpha2": "KR",
		"continent": "as",
		"fullname": "South Korea"
	},
	"KWT": {
		"code": "askwt",
		"alpha2": "KW",
		"continent": "as",
		"fullname": "Kuwait"
	},
	"KGZ": {
		"code": "askgz",
		"alpha2": "KG",
		"continent": "as",
		"fullname": "Kyrgyzstan"
	},
	"LAO": {
		"code": "aslao",
		"alpha2": "LA",
		"continent": "as",
		"fullname": "Laos"
	},
	"LBN": {
		"code": "aslbn",
		"alpha2": "LB",
		"continent": "as",
		"fullname": "Lebanon"
	},
	"MAC": {
		"code": "asmac",
		"alpha2": "MO",
		"continent": "as",
		"fullname": "Macau"
	},
	"MYS": {
		"code": "asmys",
		"alpha2": "MY",
		"continent": "as",
		"fullname": "Malaysia"
	},
	"MDV": {
		"code": "asmdv",
		"alpha2": "MV",
		"continent": "as",
		"fullname": "Maldives"
	},
	"MNG": {
		"code": "asmng",
		"alpha2": "MN",
		"continent": "as",
		"fullname": "Mongolia"
	},
	"MMR": {
		"code": "asmmr",
		"alpha2": "MM",
		"continent": "as",
		"fullname": "Myanmar"
	},
	"NPL": {
		"code": "asnpl",
		"alpha2": "NP",
		"continent": "as",
		"fullname": "Nepal"
	},
	"OMN": {
		"code": "asomn",
		"alpha2": "OM",
		"continent": "as",
		"fullname": "Oman"
	},
	"PAK": {
		"code": "aspak",
		"alpha2": "PK",
		"continent": "as",
		"fullname": "Pakistan"
	},
	"PHL": {
		"code": "asphl",
		"alpha2": "PH",
		"continent": "as",
		"fullname": "Philippines"
	},
	"QAT": {
		"code": "asqat",
		"alpha2": "QA",
		"continent": "as",
		"fullname": "Qatar"
	},
	"SAU": {
		"code": "assau",
		"alpha2": "SA",
		"continent": "as",
		"fullname": "Saudi Arabia"
	},
	"SGP": {
		"code": "assgp",
		"alpha2": "SG",
		"continent": "as",
		"fullname": "Singapore"
	},
	"LKA": {
		"code": "aslka",
		"alpha2": "LK",
		"continent": "as",
		"fullname": "Sri Lanka"
	},
	"SYR": {
		"code": "assyr",
		"alpha2": "SY",
		"continent": "as",
		"fullname": "Syria"
	},
	"TWN": {
		"code": "astwn",
		"alpha2": "TW",
		"continent": "as",
		"fullname": "Taiwan"
	},
	"TJK": {
		"code": "astjk",
		"alpha2": "TJ",
		"continent": "as",
		"fullname": "Tajikistan"
	},
	"THA": {
		"code": "astha",
		"alpha2": "TH",
		"continent": "as",
		"fullname": "Thailand"
	},
	"TUR": {
		"code": "astur",
		"alpha2": "TR",
		"continent": "as",
		"fullname": "Turkey"
	},
	"TKM": {
		"code": "astkm",
		"alpha2": "TM",
		"continent": "as",
		"fullname": "Turkmenistan"
	},
	"ARE": {
		"code": "asare",
		"alpha2": "AE",
		"continent": "as",
		"fullname": "United Arab Emirates"
	},
	"UZB": {
		"code": "asuzb",
		"alpha2": "UZ",
		"continent": "as",
		"fullname": "Uzbekistan"
	},
	"VNM": {
		"code": "asvnm",
		"alpha2": "VN",
		"continent": "as",
		"fullname": "Vietnam"
	},
	"YEM": {
		"code": "asyem",
		"alpha2": "YE",
		"continent": "as",
		"fullname": "Yemen"
	},
	"PSE": {
		"code": "aspse",
		"alpha2": "PS",
		"continent": "as",
		"fullname": "Palestine"
	},
	"ARG": {
		"code": "saarg",
		"alpha2": "AR",
		"continent": "sa",
		"fullname": "Argentina"
	},
	"BOL": {
		"code": "sabol",
		"alpha2": "BO",
		"continent": "sa",
		"fullname": "Bolivia"
	},
	"BRA": {
		"code": "sabra",
		"alpha2": "BR",
		"continent": "sa",
		"fullname": "Brazil"
	},
	"CHL": {
		"code": "sachl",
		"alpha2": "CL",
		"continent": "sa",
		"fullname": "Chile"
	},
	"COL": {
		"code": "sacol",
		"alpha2": "CO",
		"continent": "sa",
		"fullname": "Colombia"
	},
	"ECU": {
		"code": "saecu",
		"alpha2": "EC",
		"continent": "sa",
		"fullname": "Ecuador"
	},
	"FLK": {
		"code": "saflk",
		"alpha2": "FK",
		"continent": "sa",
		"fullname": "Falkland Islands"
	},
	"GUF": {
		"code": "saguf",
		"alpha2": "GF",
		"continent": "sa",
		"fullname": "French Guiana"
	},
	"GUF": {
		"code": "saguf",
		"alpha2": "GY",
		"continent": "sa",
		"fullname": "Guiana"
	},
	"GUY": {
		"code": "saguy",
		"alpha2": "GY",
		"continent": "sa",
		"fullname": "Guyana"
	},
	"PRY": {
		"code": "sapry",
		"alpha2": "PY",
		"continent": "sa",
		"fullname": "Paraguay"
	},
	"PER": {
		"code": "saper",
		"alpha2": "PE",
		"continent": "sa",
		"fullname": "Peru"
	},
	"SUR": {
		"code": "sasur",
		"alpha2": "SR",
		"continent": "sa",
		"fullname": "Suriname"
	},
	"URY": {
		"code": "saury",
		"alpha2": "UY",
		"continent": "sa",
		"fullname": "Uruguay"
	},
	"VEN": {
		"code": "saven",
		"alpha2": "VE",
		"continent": "sa",
		"fullname": "Venezuela"
	},
	"ASM": {
		"code": "ocasm",
		"alpha2": "AS",
		"continent": "oc",
		"fullname": "American Samoa"
	},
	"AUS": {
		"code": "ocaus",
		"alpha2": "AU",
		"continent": "oc",
		"fullname": "Australia"
	},
	"NZL": {
		"code": "ocnzl",
		"alpha2": "NZ",
		"continent": "oc",
		"fullname": "Chatham Island - NZ"
	},
	"COK": {
		"code": "occok",
		"alpha2": "CK",
		"continent": "oc",
		"fullname": "Cook Islands"
	},
	"FJI": {
		"code": "ocfji",
		"alpha2": "FJ",
		"continent": "oc",
		"fullname": "Fiji Islands"
	},
	"PYF": {
		"code": "ocpyf",
		"alpha2": "PF",
		"continent": "oc",
		"fullname": "French Polynesia"
	},
	"GUM": {
		"code": "ocgum",
		"alpha2": "GU",
		"continent": "oc",
		"fullname": "Guam"
	},
	"KIR": {
		"code": "ockir",
		"alpha2": "KI",
		"continent": "oc",
		"fullname": "Kiribati"
	},
	"MNP": {
		"code": "ocmnp",
		"alpha2": "MP",
		"continent": "oc",
		"fullname": "Mariana Islands"
	},
	"MHL": {
		"code": "ocmhl",
		"alpha2": "MH",
		"continent": "oc",
		"fullname": "Marshall Islands"
	},
	"FSM": {
		"code": "ocfsm",
		"alpha2": "FM",
		"continent": "oc",
		"fullname": "Federated States of Micronesia"
	},
	"UMI": {
		"code": "ocumi",
		"alpha2": "UM",
		"continent": "oc",
		"fullname": "Midway Islands"
	},
	"NRU": {
		"code": "ocnru",
		"alpha2": "NR",
		"continent": "oc",
		"fullname": "Nauru"
	},
	"NCL": {
		"code": "ocncl",
		"alpha2": "NC",
		"continent": "oc",
		"fullname": "New Caledonia"
	},
	"NZL": {
		"code": "ocnzl",
		"alpha2": "NZ",
		"continent": "oc",
		"fullname": "New Zealand"
	},
	"NIU": {
		"code": "ocniu",
		"alpha2": "NU",
		"continent": "oc",
		"fullname": "Niue"
	},
	"NFK": {
		"code": "ocnfk",
		"alpha2": "NF",
		"continent": "oc",
		"fullname": "Norfolk Island"
	},
	"PLW": {
		"code": "ocplw",
		"alpha2": "PW",
		"continent": "oc",
		"fullname": "Palau"
	},
	"PNG": {
		"code": "ocpng",
		"alpha2": "PG",
		"continent": "oc",
		"fullname": "Papua New Guinea"
	},
	"MNP": {
		"code": "ocmnp",
		"alpha2": "MP",
		"continent": "oc",
		"fullname": "Saipan"
	},
	"SLB": {
		"code": "ocslb",
		"alpha2": "SB",
		"continent": "oc",
		"fullname": "Solomon Islands"
	},
	"TKL": {
		"code": "octkl",
		"alpha2": "TK",
		"continent": "oc",
		"fullname": "Tokelau"
	},
	"TON": {
		"code": "octon",
		"alpha2": "TO",
		"continent": "oc",
		"fullname": "Tonga"
	},
	"TUV": {
		"code": "octuv",
		"alpha2": "TV",
		"continent": "oc",
		"fullname": "Tuvalu"
	},
	"VUT": {
		"code": "ocvut",
		"alpha2": "VU",
		"continent": "oc",
		"fullname": "Vanuatu"
	},
	"UMI": {
		"code": "ocumi",
		"alpha2": "UM",
		"continent": "oc",
		"fullname": "Wake Island"
	},
	"WLF": {
		"code": "ocwlf",
		"alpha2": "WF",
		"continent": "oc",
		"fullname": "Wallis and Futuna Islands"
	},
	"WSM": {
		"code": "ocwsm",
		"alpha2": "WS",
		"continent": "oc",
		"fullname": "Samoa"
	},
	"TLS": {
		"code": "octls",
		"alpha2": "TL",
		"continent": "oc",
		"fullname": "East Timor"
	}
}