#!/usr/bin/env python
# -*- coding: utf-8 -*-
from datetime import datetime

import hashlib
import json

class Filters():
	def __init__(self, scope = 1, tabulation = "t1",
				 visualization = 'treemap', years = 'ALL',
				 from_year = 1900, to_year = datetime.now().year,
				 thematic_areas = ['ALL'], wos_thematic_areas = ['ALL'],
				 study_unit = 1, collections = ['ALL'],
				 journals = ['ALL'], languages = ['ALL'],
				 document_type = 'ALL', document_type_details = ['ALL']):
		self.scope = scope
		self.tabulation = tabulation
		self.visualization = visualization
		self.years = years
		self.from_year = from_year
		self.to_year = to_year
		self.thematic_areas = thematic_areas
		self.wos_thematic_areas = wos_thematic_areas
		self.collections = collections
		self.journals = journals
		self.languages = languages
		self.document_type = document_type
		self.document_type_details = document_type_details
	
	@property
	def serialize(self):
		return {
			'scope': self.scope,
			'tabulation': self.tabulation,
			'visualization': self.visualization,
			'years': self.years, 
			'from_year': self.from_year, 
			'to_year': self.to_year, 
			'thematic_areas': self.thematic_areas, 
			'wos_thematic_areas': self.wos_thematic_areas, 
			'study_unit': self.study_unit, 
			'collections': self.collections, 
			'journals': self.journals, 
			'languages': self.languages, 
			'document_type': self.document_type, 
			'document_type_details': self.document_type_details, 
		}

	@property
	def md5(self):
		fstr = json.dumps(self.serialize, indent=4).encode('utf8')
		return hashlib.md5(fstr).hexdigest()