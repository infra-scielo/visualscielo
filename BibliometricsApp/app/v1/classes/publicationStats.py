#!/usr/bin/env python
# -*- coding: utf-8 -*-
import json
from datetime import datetime, timedelta
from app.thrift_clients import clients
from app.v1.classes.filters import Filters


class ServerError(Exception):
  def __init__(self, value):
    self.message = 'Server Error: %s' % str(value)
  
  def __str__(self):
    return repr(self.message)


def publicationstats(host):
  address, port = host.split(':')
  return PublicationStats(address, port)


class PublicationStats(clients.PublicationStats):  
  def generate_filters(self, filters = Filters()):
    #Generamos Filtros
    dict_filters = {"and": []}
    dict_filters["and"].append({"range": {"publication_year": { "gte": filters.from_year, "lte": filters.to_year }}}) #Years Range
    if len(filters.thematic_areas) > 0:
      dict_filters["and"].append({"terms": {"subject_areas": filters.thematic_areas}})
    if len(filters.wos_thematic_areas) > 0:
      dict_filters["and"].append({"terms": {"wos_subject_areas": filters.wos_thematic_areas}})
    if len(filters.collections) > 0:
      dict_filters["and"].append({"terms": {"collection": filters.collections}})
    if len(filters.languages) > 0:
      dict_filters["and"].append({"terms":{"languages": filters.languages}})
    if filters.document_type == 'Citable':
      dict_filters["and"].append({"terms": {"document_type": ["article-commentary", "brief-report", "case-report", "rapid-communication", "research-article", "review-article"]}})
    elif filters.document_type == 'non-Citable':
      dict_filters["and"].append({"terms": {"document_type": ["editorial", "book-review", "abstract", "letter", "addendum", "news", "correction", "press-release"]}})
    if len(filters.document_type_details) > 0:
      dict_filters["and"].append({"terms": {"document_type": filters.document_type_details}})
    if len(filters.journals) > 0:
      dict_filters["and"].append({"terms":{"journal_title": filters.journals}})
    return dict_filters


  def query_citations(self, current_filters = Filters()):
    filters = self.generate_filters(current_filters)
    #Armamos Query
    body =  {"aggs" : {"group_by_collection" : {"filter" : filters, "aggs" : {"by_collection" : { "terms" : { "field" : "collection" },"aggs":{"by_journal":{"terms":{"field":"journal_title","size":0},"aggs":{"citation_sum":{"terms":{"field":"citations"}}}}}},"by_year":{"terms":{"field":"collection"},"aggs":{"by_year_documents":{"terms":{"field":"publication_year","order":{"_term":"asc"},"size":0},"aggs":{"by_year_citation":{"terms":{"field":"citations","size":0}}}}}} }}}}    
    query_parameters = [
      clients.accessstats_thrift.kwargs('size', '0'),
    ]
    query_results = json.loads(self.client.search('article', json.dumps(body), query_parameters))  
    return query_results


  #redes coatoria
  def query_coauthors(self, current_filters = Filters()):
    #Filtros
    filters = self.generate_filters(current_filters)
    #Query
    body = {"from": 0,"size": 1000000000, "filter" : filters, "fields": ["aff_countries","publication_year"]}
    query_parameters = [
      clients.accessstats_thrift.kwargs('size', '1000000000'),
    ]
    query_result = json.loads(self.client.search('article', json.dumps(body), query_parameters))   
    return query_result

  #redes collection
  def query_collection(self, current_filters = Filters()):
    #Filtros
    filters = self.generate_filters(current_filters)
    #Query
    body = {"from": 0, "size": 1000000000, "filter" : filters, "fields": ["aff_countries","collection","publication_year"]}
    query_parameters = [
      clients.accessstats_thrift.kwargs('size', '1000000000'),
    ]
    query_result = json.loads(self.client.search('article', json.dumps(body), query_parameters))   
    return query_result

  #Obtener Areas Temáticas
  def query_3(self):
    body = {  "aggs" : {"by_area" : { "terms" : { "field" : "subject_areas" }}}}
    query_parameters = [
      clients.accessstats_thrift.kwargs('size', '0'),
    ]
    query_result = json.loads(self.client.search('article', json.dumps(body), query_parameters))  
    return query_result    

  #query que hace gastón en la funcion complete filter
  def query_4(self,value,size): 
    body = { "aggs" : {"by_value" : { "terms" : { "field" : value,"size":size }}}}
    query_parameters = [
      clients.accessstats_thrift.kwargs('size', '0'),
    ]
    query_result = json.loads(self.client.search('article', json.dumps(body), query_parameters))  

  #Article - Obtener diferentes valores de un campo en especifico.
  def query_article_distinct_field(self, field):
    body = { "aggs" : {"by_value" : { "terms" : { "field" : field }}}}
    query_parameters = [
      clients.accessstats_thrift.kwargs('size', '0'),
    ]
    query_result = json.loads(self.client.search('article', json.dumps(body), query_parameters))  
    return query_result

  #Journal Obtener diferentes valores de un campo en especifico
  def query_journal_distinct_field(self, field):
    body = { "aggs" : {"by_value" : { "terms" : { "field" : field }}}}
    query_parameters = [
      clients.accessstats_thrift.kwargs('size', '0'),
    ]
    query_result = json.loads(self.client.search('journal', json.dumps(body), query_parameters))  
    return query_result

  #Obtener data para llenar el gráfico inicial
  def data_index(self):
    body = {"aggs" : {"by_collection" : { "terms" : { "field" : "collection", "size":1000000000 },"aggs":{"by_journal":{"terms":{"field":"journal_title","size":1000000000}}}}}}
    query_parameters = [
      clients.accessstats_thrift.kwargs('size', '0'),
    ]
    query_result = json.loads(self.client.search('article', json.dumps(body), query_parameters))  
    return query_result    
  
#NUEVO
#Number of documents by country of publication
  def query1(self, current_filters = Filters()):
    filters = self.generate_filters(current_filters)
    #Armamos Query
    body =  {"aggs":{"data":{"filter":filters, "aggs" : {"query" : { "terms" : { "field" : "collection","size":0 }}}}}}    
    query_parameters = [
      clients.accessstats_thrift.kwargs('size', '0'),
    ]
    query_result = json.loads(self.client.search('article', json.dumps(body), query_parameters))  
    return query_result

#Number of documents by country of publication and thematic area
  def query2(self, from_year = 1900, to_year = datetime.now().year,
                      thematic_areas = [], wos_thematic_areas = [], collections = [], languages = [],
                      document_type = 'ALL', document_type_details = [], journals = []):
    filters = self.generate_filters(from_year, to_year, thematic_areas, wos_thematic_areas, collections, languages, document_type, document_type_details, journals)
    #Armamos Query
    body =  {"aggs":{"data":{"filter":filters,"aggs" : {"query" : { "terms" : { "field" : "collection","size":0 },"aggs":{"by_option":{"terms":{"field":"subject_areas","size":0}}}}}}}}
    query_parameters = [
      clients.accessstats_thrift.kwargs('size', '0'),
    ]
    query_results = json.loads(self.client.search('article', json.dumps(body), query_parameters))  
    return query_results

#Number of documents by country of publication and year of publication
  def query3(self, from_year = 1900, to_year = datetime.now().year,
                      thematic_areas = [], wos_thematic_areas = [], collections = [], languages = [],
                      document_type = 'ALL', document_type_details = [], journals = []):
    filters = self.generate_filters(from_year, to_year, thematic_areas, wos_thematic_areas, collections, languages, document_type, document_type_details, journals)
    #Armamos Query
    body =  {"aggs":{"data":{"filter":filters,"aggs" : {"query" : { "terms" : { "field" : "collection","size":0 },"aggs":{"by_option":{"terms":{"field":"publication_year","size":0}}}}}}}}
    query_parameters = [
      clients.accessstats_thrift.kwargs('size', '0'),
    ]
    query_results = json.loads(self.client.search('article', json.dumps(body), query_parameters))  
    return query_results
#Number of documents by country of publication and language
  def query4(self, from_year = 1900, to_year = datetime.now().year,
                      thematic_areas = [], wos_thematic_areas = [], collections = [], languages = [],
                      document_type = 'ALL', document_type_details = [], journals = []):
    filters = self.generate_filters(from_year, to_year, thematic_areas, wos_thematic_areas, collections, languages, document_type, document_type_details, journals)
    #Armamos Query
    body =  {"aggs":{"data":{"filter":filters,"aggs" : {"query" : { "terms" : { "field" : "collection","size":0 },"aggs":{"by_option":{"terms":{"field":"languages","size":0}}}}}}}}
    query_parameters = [
      clients.accessstats_thrift.kwargs('size', '0'),
    ]
    query_results = json.loads(self.client.search('article', json.dumps(body), query_parameters))  
    return query_results
#Number of documents by country of authors affiliation
  def query5(self, from_year = 1900, to_year = datetime.now().year,
                      thematic_areas = [], wos_thematic_areas = [], collections = [], languages = [],
                      document_type = 'ALL', document_type_details = [], journals = []):
    filters = self.generate_filters(from_year, to_year, thematic_areas, wos_thematic_areas, collections, languages, document_type, document_type_details, journals)
    #Armamos Query
    body =  {"aggs":{"data":{"filter":filters,"aggs" : {"query" : { "terms" : { "field" : "aff_countries","size":0 }}}}}}
    query_parameters = [
      clients.accessstats_thrift.kwargs('size', '0'),
    ]
    query_results = json.loads(self.client.search('article', json.dumps(body), query_parameters))  
    return query_results
#Number of documents by country of authors affiliation and thematic area
  def query6(self, from_year = 1900, to_year = datetime.now().year,
                      thematic_areas = [], wos_thematic_areas = [], collections = [], languages = [],
                      document_type = 'ALL', document_type_details = [], journals = []):
    filters = self.generate_filters(from_year, to_year, thematic_areas, wos_thematic_areas, collections, languages, document_type, document_type_details, journals)
    #Armamos Query
    body =  {"aggs":{"data":{"filter":filters,"aggs" : {"query" : { "terms" : { "field" : "aff_countries","size":0 },"aggs":{"by_option":{"terms":{"field":"subject_areas","size":0}}}}}}}}
    query_parameters = [
      clients.accessstats_thrift.kwargs('size', '0'),
    ]
    query_results = json.loads(self.client.search('article', json.dumps(body), query_parameters))  
    return query_results
#Number of documents by country of authors affiliation and year of publication
  def query7(self, from_year = 1900, to_year = datetime.now().year,
                      thematic_areas = [], wos_thematic_areas = [], collections = [], languages = [],
                      document_type = 'ALL', document_type_details = [], journals = []):
    filters = self.generate_filters(from_year, to_year, thematic_areas, wos_thematic_areas, collections, languages, document_type, document_type_details, journals)
    #Armamos Query
    body =  {"aggs":{"data":{"filter":filters,"aggs" : {"query" : { "terms" : { "field" : "aff_countries","size":0 },"aggs":{"by_option":{"terms":{"field":"publication_year","size":0}}}}}}}}
    query_parameters = [
      clients.accessstats_thrift.kwargs('size', '0'),
    ]
    query_results = json.loads(self.client.search('article', json.dumps(body), query_parameters))  
    return query_results
#Number of documents by country of authors affiliation and language
  def query8(self, from_year = 1900, to_year = datetime.now().year,
                      thematic_areas = [], wos_thematic_areas = [], collections = [], languages = [],
                      document_type = 'ALL', document_type_details = [], journals = []):
    filters = self.generate_filters(from_year, to_year, thematic_areas, wos_thematic_areas, collections, languages, document_type, document_type_details, journals)
    #Armamos Query
    body =  {"aggs":{"data":{"filter":filters,"aggs" : {"query" : { "terms" : { "field" : "aff_countries","size":0 },"aggs":{"by_option":{"terms":{"field":"languages","size":0}}}}}}}}
    query_parameters = [
      clients.accessstats_thrift.kwargs('size', '0'),
    ]
    query_results = json.loads(self.client.search('article', json.dumps(body), query_parameters))  
    return query_results
#Number of documents by journal title
  def query9(self, from_year = 1900, to_year = datetime.now().year,
                      thematic_areas = [], wos_thematic_areas = [], collections = [], languages = [],
                      document_type = 'ALL', document_type_details = [], journals = []):
    filters = self.generate_filters(from_year, to_year, thematic_areas, wos_thematic_areas, collections, languages, document_type, document_type_details, journals)
    #Armamos Query
    body =  {"aggs":{"data":{"filter":filters,"aggs" : {"query":{ "terms" : { "field" : "journal_title","size":0 }}}}}}
    query_parameters = [
      clients.accessstats_thrift.kwargs('size', '0'),
    ]
    query_results = json.loads(self.client.search('article', json.dumps(body), query_parameters))  
    return query_results
#Number of documents by journal title and thematic area
  def query10(self, from_year = 1900, to_year = datetime.now().year,
                      thematic_areas = [], wos_thematic_areas = [], collections = [], languages = [],
                      document_type = 'ALL', document_type_details = [], journals = []):
    filters = self.generate_filters(from_year, to_year, thematic_areas, wos_thematic_areas, collections, languages, document_type, document_type_details, journals)
    #Armamos Query
    body =  {"aggs":{"data":{"filter":filters,"aggs" : {"query":{ "terms" : { "field" : "journal_title","size":0 },"aggs":{"by_option":{"terms":{"field":"subject_areas"}}}}}}}}
    query_parameters = [
      clients.accessstats_thrift.kwargs('size', '0'),
    ]
    query_results = json.loads(self.client.search('article', json.dumps(body), query_parameters))  
    return query_results
#Number of documents by journal title and year of publication
  def query11(self, from_year = 1900, to_year = datetime.now().year,
                      thematic_areas = [], wos_thematic_areas = [], collections = [], languages = [],
                      document_type = 'ALL', document_type_details = [], journals = []):
    filters = self.generate_filters(from_year, to_year, thematic_areas, wos_thematic_areas, collections, languages, document_type, document_type_details, journals)
    #Armamos Query
    body =  {"aggs":{"data":{"filter":filters,"aggs" : {"query":{ "terms" : { "field" : "journal_title","size":0 },"aggs":{"by_option":{"terms":{"field":"publication_year"}}}}}}}}
    query_parameters = [
      clients.accessstats_thrift.kwargs('size', '0'),
    ]
    query_results = json.loads(self.client.search('article', json.dumps(body), query_parameters))  
    return query_results
#Number of documents by journal title and language
  def query12(self, from_year = 1900, to_year = datetime.now().year,
                      thematic_areas = [], wos_thematic_areas = [], collections = [], languages = [],
                      document_type = 'ALL', document_type_details = [], journals = []):
    filters = self.generate_filters(from_year, to_year, thematic_areas, wos_thematic_areas, collections, languages, document_type, document_type_details, journals)
    #Armamos Query
    body =  {"aggs":{"data":{"filter":filters,"aggs" : {"query":{ "terms" : { "field" : "journal_title","size":0 },"aggs":{"by_option":{"terms":{"field":"languages"}}}}}}}}
    query_parameters = [
      clients.accessstats_thrift.kwargs('size', '0'),
    ]
    query_results = json.loads(self.client.search('article', json.dumps(body), query_parameters))  
    return query_results
#Number of documents
  def query13(self, from_year = 1900, to_year = datetime.now().year,
                      thematic_areas = [], wos_thematic_areas = [], collections = [], languages = [],
                      document_type = 'ALL', document_type_details = [], journals = []):
    filters = self.generate_filters(from_year, to_year, thematic_areas, wos_thematic_areas, collections, languages, document_type, document_type_details, journals)
    #Armamos Query
    body =  {"aggs":{"data":{"filter":filters,"aggs" : {"query":{ "terms" : { "field" : "_index","size":0 }}}}}}
    query_parameters = [
      clients.accessstats_thrift.kwargs('size', '0'),
    ]
    query_results = json.loads(self.client.search('article', json.dumps(body), query_parameters))  
    return query_results
#Number of documents by thematic area
  def query14(self, from_year = 1900, to_year = datetime.now().year,
                      thematic_areas = [], wos_thematic_areas = [], collections = [], languages = [],
                      document_type = 'ALL', document_type_details = [], journals = []):
    filters = self.generate_filters(from_year, to_year, thematic_areas, wos_thematic_areas, collections, languages, document_type, document_type_details, journals)
    #Armamos Query
    body =  {"aggs":{"data":{"filter":filters,"aggs" : {"query" : { "terms" : { "field" : "subject_areas","size":0 }}}}}}
    query_parameters = [
      clients.accessstats_thrift.kwargs('size', '0'),
    ]
    query_results = json.loads(self.client.search('article', json.dumps(body), query_parameters))  
    return query_results
#Number of documents by year of publication
  def query15(self, from_year = 1900, to_year = datetime.now().year,
                      thematic_areas = [], wos_thematic_areas = [], collections = [], languages = [],
                      document_type = 'ALL', document_type_details = [], journals = []):
    #Armamos Query
    body =  {"aggs":{"data":{"filter":filters,"aggs" : {"query" : { "terms" : { "field" : "publication_year","size":0 }}}}}}
    query_parameters = [
      clients.accessstats_thrift.kwargs('size', '0'),
    ]
    query_results = json.loads(self.client.search('article', json.dumps(body), query_parameters))  
    return query_results
#Number of documents by language
  def query16(self, from_year = 1900, to_year = datetime.now().year,
                      thematic_areas = [], wos_thematic_areas = [], collections = [], languages = [],
                      document_type = 'ALL', document_type_details = [], journals = []):
    filters = self.generate_filters(from_year, to_year, thematic_areas, wos_thematic_areas, collections, languages, document_type, document_type_details, journals)
    #Armamos Query
    body =  {"aggs":{"data":{"filter":filters,"aggs" : {"query" : { "terms" : { "field" : "languages","size":0 }}}}}}
    query_parameters = [
      clients.accessstats_thrift.kwargs('size', '0'),
    ]
    query_results = json.loads(self.client.search('article', json.dumps(body), query_parameters))  
    return query_results
#Number of documents by thematic area and language
  def query17(self, from_year = 1900, to_year = datetime.now().year,
                      thematic_areas = [], wos_thematic_areas = [], collections = [], languages = [],
                      document_type = 'ALL', document_type_details = [], journals = []):
    filters = self.generate_filters(from_year, to_year, thematic_areas, wos_thematic_areas, collections, languages, document_type, document_type_details, journals)
    #Armamos Query
    body =  {"aggs":{"data":{"filter":filters,"aggs" : {"query" : { "terms" : { "field" : "subject_areas","size":0 },"aggs":{"by_option":{"terms":{"field":"languages","size":0}}}}}}}}
    query_parameters = [
      clients.accessstats_thrift.kwargs('size', '0'),
    ]
    query_results = json.loads(self.client.search('article', json.dumps(body), query_parameters))  
    return query_results

#Number of documents by type of authors affiliation collaboration
  def query18(self, from_year = 1900, to_year = datetime.now().year,
                      thematic_areas = [], wos_thematic_areas = [], collections = [], languages = [],
                      document_type = 'ALL', document_type_details = [], journals = []):
    filters = self.generate_filters(from_year, to_year, thematic_areas, wos_thematic_areas, collections, languages, document_type, document_type_details, journals)
    #Armamos Query
    body =  {"aggs":{"data":{"filter":filters,"aggs" : {"query" : { "terms" : { "field" : "collection","size":0 },"aggs":{"by_option":{"terms":{"field":"aff_countries","size":0}}}}}}}}

    query_parameters = [
      clients.accessstats_thrift.kwargs('size', '0'),
    ]
    query_results = json.loads(self.client.search('article', json.dumps(body), query_parameters))  
    return query_results

#Number of documents by type of authors affiliation collaboration and thematic area
  def query19(self, from_year = 1900, to_year = datetime.now().year,
                      thematic_areas = [], wos_thematic_areas = [], collections = [], languages = [],
                      document_type = 'ALL', document_type_details = [], journals = []):
    filters = self.generate_filters(from_year, to_year, thematic_areas, wos_thematic_areas, collections, languages, document_type, document_type_details, journals)
    #Armamos Query
    body =  {"aggs":{"data":{"filter":filters,"aggs" : {"query" : { "terms" : { "field" : "collection","size":0 },"aggs":{"by_option":{"terms":{"field":"subject_areas","size":0},"aggs":{"last_option":{"terms":{"field":"aff_countries","size":0}}}}}}}}}}

    query_parameters = [
      clients.accessstats_thrift.kwargs('size', '0'),
    ]
    query_results = json.loads(self.client.search('article', json.dumps(body), query_parameters))  
    return query_results

#Number of documents by type of authors affiliation collaboration and year of publication
  def query20(self, from_year = 1900, to_year = datetime.now().year,
                      thematic_areas = [], wos_thematic_areas = [], collections = [], languages = [],
                      document_type = 'ALL', document_type_details = [], journals = []):
    filters = self.generate_filters(from_year, to_year, thematic_areas, wos_thematic_areas, collections, languages, document_type, document_type_details, journals)
    #Armamos Query
    body =  {"aggs":{"data":{"filter":filters,"aggs" : {"query" : { "terms" : { "field" : "collection","size":0 },"aggs":{"by_option":{"terms":{"field":"publication_year","size":0},"aggs":{"last_option":{"terms":{"field":"aff_countries","size":0}}}}}}}}}}

    query_parameters = [
      clients.accessstats_thrift.kwargs('size', '0'),
    ]
    query_results = json.loads(self.client.search('article', json.dumps(body), query_parameters))  
    return query_results

#Number of documents by type of authors affiliation collaboration and language
  def query21(self, from_year = 1900, to_year = datetime.now().year,
                      thematic_areas = [], wos_thematic_areas = [], collections = [], languages = [],
                      document_type = 'ALL', document_type_details = [], journals = []):
    filters = self.generate_filters(from_year, to_year, thematic_areas, wos_thematic_areas, collections, languages, document_type, document_type_details, journals)
    #Armamos Query
    body =  {"aggs":{"data":{"filter":filters,"aggs" : {"query" : { "terms" : { "field" : "collection","size":0 },"aggs":{"by_option":{"terms":{"field":"languages","size":0},"aggs":{"last_option":{"terms":{"field":"aff_countries","size":0}}}}}}}}}}

    query_parameters = [
      clients.accessstats_thrift.kwargs('size', '0'),
    ]
    query_results = json.loads(self.client.search('article', json.dumps(body), query_parameters))  
    return query_results

  #Ejecutar query a elastic
  def run_query(self, body, filters = Filters()):
    body["aggs"]["by_all"]["filter"] = self.generate_filters(filters)
    query_parameters = [clients.accessstats_thrift.kwargs('size', '100')]    
    query_result = json.loads(self.client.search('article', json.dumps(body), query_parameters))  
    return query_result
  
  #Ejecutar query a elastic
  def run_query2(self, body, filters = Filters()):
    body["filter"] = self.generate_filters(filters)
    query_parameters = [clients.accessstats_thrift.kwargs('size', '2000')]    
    query_result = json.loads(self.client.search('article', json.dumps(body), query_parameters))  
    return query_result
