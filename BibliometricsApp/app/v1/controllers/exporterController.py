#!/usr/bin/env python
# -*- coding: utf-8 -*-
from flask import Blueprint, render_template,\
                  session, redirect, url_for,\
                  request, jsonify, make_response,\
                  current_app
from app.v1.helpers.dicts import collections as collections_dict, country_collections, thematic_areas as areas, country_code_inv, country_name,country_code_rev,not_collections

import pyexcel as pe
import StringIO
import ast

VERSION = "v1"
mod_export = Blueprint('exporterController', __name__, url_prefix='/'+VERSION)


def gfile_download(filename = 'export', extension = 'xlsx', book = {}):
    #generamos el excel
    book = pe.Book(book)
    io = StringIO.StringIO()
    book.save_to_memory(extension, io)
    output = make_response(io.getvalue())
    output.headers["Content-Disposition"] = "attachment; filename=%s.%s" %(filename, extension)
    output.headers["Content-type"] = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
    return output


@mod_export.route('/export', methods = ['POST'])
def export():
    #Leemos Formulario y obtenemos filtros
    form =  request.form
    
    data = ast.literal_eval(form.get('data', '{}').encode('utf-8'))

    return simple_dict_export(data = data)
    #return gfile_download("[SciELO]Empty", "xlsx", book)


def simple_dict_export(data = {}):
    book = {}
    sheet_name = "Sheet1"
    book[sheet_name] = []
    book[sheet_name].append(["Header1", "Header2", "# Documents"])
    for key, values in data.items():
        for k, v in values.items():
            book[sheet_name].append([key, k, v])
    return gfile_download("[SciELO]Documents", "xlsx", book)



def documents_export(data = {}, journals = {}):
    book = {}
    for country, cant in data.items():
        sheet_name = "%s(%s)" %(country, cant)
        book[sheet_name] = []
        book[sheet_name].append([country, "%s Publications" %(cant)])
        book[sheet_name].append([])
        book[sheet_name].append(["Journal", "#Publications"])
        for j in  journals[country]:
            book[sheet_name].append([j, journals[country][j]["citation"]])
    return gfile_download("[SciELO]Documents", "xlsx", book)

def citations_export(data = {}, journals = {}):
    book = {}
    for country, cit in data.items():
        sheet_name = "%s(%s)" %(country, cit)
        book[sheet_name] = []
        book[sheet_name].append([country, "%s Citations" %(cit)])
        book[sheet_name].append([])
        book[sheet_name].append(["Journal", "#Citations"])
        for j in  journals[country]:
            book[sheet_name].append([j, journals[country][j]["citation"]])
    return gfile_download("[SciELO]Citations", "xlsx", book)


def coauthors_export(data = [{}, {}, {}]):
    book = {}
    sheet_name = 'Co-Authors'
    book[sheet_name] = []
    book[sheet_name].append(["Country", "Provided number of Co-Author relationships", "Co-Author country diversity", "Co-Authors from"])
    for country, rel in data[1].items():
        line = ""
        for k, v in data[0][country].items():
            line += "%s(%s); " %(k, v)
        book[sheet_name].append([country, rel, round(100.0*(float(len(data[0][country]))/240.0), 1), line])
    return gfile_download("[SciELO]Co-Authors", "xlsx", book)


def collections_export(data = [{}, {}, {}, {}]):
    book = {}
    sheet_name = 'Collections'
    book[sheet_name] = []
    book[sheet_name].append(["Collection", "Times Chosen (Documents)", "Chosen by authors from", "Countries"])
    for country, choices in data[1].items():
        line = ""
        for c in data[3][country]:
            line += "%s; " %(c)
        book[sheet_name].append([country, choices, "%s Countries" %(len(data[3][country])), line])
    return gfile_download("[SciELO]Collections", "xlsx", book)