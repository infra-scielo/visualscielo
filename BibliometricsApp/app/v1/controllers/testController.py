#!/usr/bin/env python
# -*- coding: utf-8 -*-
from flask import Blueprint, render_template,\
                  session, redirect, url_for,\
                  request, jsonify, make_response,\
                  flash, current_app
from datetime import datetime
from math import log
from app.v1.helpers.dicts import collections as collections_dict, country_collections, thematic_areas as areas, country_code_inv, country_name,country_code_rev,not_collections, collections, ISO_3166,worldmap_code
from app.v1.helpers.elasticQueries import elasticQueries
from app.v1.helpers.formHelper import *
from app.v1.classes.filters import Filters
from app.v1.classes.publicationStats import PublicationStats

import pyexcel as pe
import StringIO
import requests
import os.path
import random
import json

VERSION = "v1"
mod_test = Blueprint('testController', __name__, url_prefix='/'+VERSION+'/test')

@mod_test.route('/', methods = ['POST'])
def t_index():
  #Leemos Formulario y obtenemos filtros
  form =  request.form
  filters = process_filters_form(form)  

  if(filters.tabulation == "1"):
    return get_query1(filters)
  elif(filters.tabulation == "2"):
    return get_query2(filters)
  elif(filters.tabulation == "3"):
    return get_query3(filters)
  elif(filters.tabulation == "4"):
    return get_query4(filters)
  elif(filters.tabulation == "5"):
    return get_query5(filters)
  elif(filters.tabulation == "6"):
    return get_query6(filters)
  elif(filters.tabulation == "7"):
    return get_query7(filters)
  elif(filters.tabulation == "8"):
    return get_query8(filters)
  elif(filters.tabulation == "9"):
    return get_query5b(filters)
  elif(filters.tabulation == "10"):
    return get_query6b(filters)
  elif(filters.tabulation == "11"):
    return get_query7b(filters)
  elif(filters.tabulation == "12"):
    return get_query8b(filters)
  elif(filters.tabulation == "13"):
    return get_query18(filters)
  elif(filters.tabulation == "14"):
    return get_query19(filters)
  elif(filters.tabulation == "15"):
    return get_query20(filters)
  elif(filters.tabulation == "16"):
    return get_query4(filters)
  elif(filters.tabulation == "17"):
    return get_query18b(filters)
  elif(filters.tabulation == "18"):
    return get_query19b(filters)
  elif(filters.tabulation == "19"):
    return get_query20b(filters)
  elif(filters.tabulation == "20"):
    return get_query21(filters)
  elif(filters.tabulation == "28"):
    return get_query9(filters)
  elif(filters.tabulation == "29"):
    return get_query10(filters)
  elif(filters.tabulation == "30"):
    return get_query5(filters)
  elif(filters.tabulation == "31"):
    return get_query12(filters)
  elif(filters.tabulation == "32"):
    return get_query13(filters)
  elif(filters.tabulation == "33"):
    return get_query14(filters)
  elif(filters.tabulation == "34"):
    return get_query15(filters)
  elif(filters.tabulation == "35"):
    return get_query16(filters)
  elif(filters.tabulation == "36"):
    return get_query14b(filters)
  elif(filters.tabulation == "37"):
    return get_query15b(filters)
  elif(filters.tabulation == "38"):
    return get_query16b(filters)
  elif(filters.tabulation == "39"):
    return get_query17(filters)
  else:
    flash('Impossible to answer the request at this time', 'error')
    return redirect(url_for("indexController.index"))
#-- ----------------------------------------------------------------
#NUEVO

def data_process(RESPONSE,option,flag):
    TREEMAP = []
    MAP = {}
    MAP2 = []
    for data in RESPONSE:
        response = data[option]["buckets"]
        
        data["key2"] = data["key"]
        data["key3"] = data["key"]
        
        
        
        if data["key"] in country_collections:
            data["key"] = country_collections[data["key"]]
        if data["key"] in country_name:
            data["key"] = country_name[data["key"]]
        if data["key2"] in collections:
            data["key2"] = collections[data["key2"]][0]
        else:
            data["key2"] = data["key"]
        if data["key3"] in worldmap_code:
            data["key3"] = worldmap_code[data["key3"]]
        
        
        MAP[data["key"]] = {}
        MAP[data["key"]]["documents"] = data["doc_count"]
        for data2 in response:
            data2["key2"] = data2["key"]
            data2["key3"] = data2["key"]
            if data2["key"].encode("utf8") in country_collections:
                data2["key"] = country_collections[data2["key"]]
            if data2["key"] in country_name:
                data2["key"] = country_name[data2["key"]]
            if data2["key2"] in collections:
                data2["key2"] = collections[data2["key2"]][0]
            else:
                data2["key2"] = data2["key"]
            if data2["key3"] in worldmap_code:
                data2["key3"] = worldmap_code[data2["key3"]]
            MAP[data["key"]][data2["key"]] = data2["doc_count"]
            if flag == 0:
                response2 = data["by_year"]["buckets"]
                for data3 in response2:
                    TREEMAP.append({"documents":data2["doc_count"],"name":data3["key"],"group":data["key2"],"group2":data2["key2"]})
                    

            else:
                TREEMAP.append({"documents":data2["doc_count"],"name":data2["key2"],"group":data["key2"]})
        MAP2.append({"value":data["doc_count"],"country":data["key3"],"name":data["key2"]})
        
    return MAP,TREEMAP
#=======
#  TREEMAP = []
#  MAP = {}
#  for data in RESPONSE:
#    response = data[option]["buckets"]
#    
#    data["key2"] = data["key"]
#    
#    if data["key"] in country_collections:
#      data["key"] = country_collections[data["key"]]
#    if data["key"] in country_name:
#      data["key"] = country_name[data["key"]]
#    if data["key2"] in collections:
#      data["key2"] = collections[data["key2"]][0]
#    else:
#      data["key2"] = data["key"]
#    
#    MAP[data["key"]] = {}
#    MAP[data["key"]]["documents"] = data["doc_count"]
#    for data2 in response:
#      data2["key2"] = data2["key"]
#      if data2["key"].encode("utf8") in country_collections:
#        data2["key"] = country_collections[data2["key"]]
#      if data2["key"] in country_name:
#        data2["key"] = country_name[data2["key"]]
#      if data2["key2"] in collections:
#        data2["key2"] = collections[data2["key2"]][0]
#      else:
#        data2["key2"] = data2["key"]
#      
#      MAP[data["key"]][data2["key"]] = data2["doc_count"]
#      if flag == 0:
#        response2 = data["by_year"]["buckets"]
#        for data3 in response2:
#          TREEMAP.append({"documents":data2["doc_count"],"name":data3["key"],"group":data["key2"],"group2":data2["key2"]})
#      else:
#        TREEMAP.append({"documents":data2["doc_count"],"name":data2["key2"],"group":data["key2"]})
#  return MAP,TREEMAP
#>>>>>>> 76edd5b1eff62072e16ea7a2da32fa879c8dcc34


#COLLECTION COMO PIVOTE
def get_query1(filters = Filters()):
    publication = PublicationStats(current_app.config.get("SERVER"), current_app.config.get("PUBLICATIONSTATS"))
    response = publication.run_query(elasticQueries[filters.scope]["tabulations"][filters.tabulation]["query"], filters = filters)
    response = response["aggregations"]["by_all"]["by_collection"]["buckets"]
    DATA = {}
    DATA["map"] = {}
    DATA["treemap"] = {}
    DATA["map"]["by_area"],DATA["treemap"]["by_area"] = data_process(response,"by_area",0)
    DATA["map"]["by_language"],DATA["treemap"]["by_language"] = data_process(response,"by_language",0)
    DATA["map"]["by_affiliation"],DATA["treemap"]["by_affiliation"] = data_process(response,"by_affiliation",0)
    DATA["map"]["by_journal"],DATA["treemap"]["by_journal"] = data_process(response,"by_journal",0)
    DATA["map"]["by_year"],DATA["treemap"]["by_year"] = data_process(response,"by_year",1)
    data2 = collection_graph(filters)
    return render_template("index/index.html", data = data2,filters=Filters())

#THEMATIC AREA COMO PIVOTE
def get_query2(filters = Filters()):
    publication = PublicationStats(current_app.config.get("SERVER"), current_app.config.get("PUBLICATIONSTATS"))
    response = publication.run_query(elasticQueries[filters.scope]["tabulations"][filters.tabulation]["query"], filters = filters)
    response = response["aggregations"]["by_all"]["by_area"]["buckets"]
    DATA = {}
    DATA["map"] = {}
    DATA["treemap"] = {}
    DATA["map"]["by_collection"],DATA["treemap"]["by_collection"] = data_process(response,"by_collection",0)
    DATA["map"]["by_language"],DATA["treemap"]["by_language"] = data_process(response,"by_language",0)
    DATA["map"]["by_affiliation"],DATA["treemap"]["by_affiliation"] = data_process(response,"by_affiliation",0)
    DATA["map"]["by_journal"],DATA["treemap"]["by_journal"] = data_process(response,"by_journal",0)
    DATA["map"]["by_year"],DATA["treemap"]["by_year"] = data_process(response,"by_year",1)

    return render_template("index/index.html", data = DATA,filters=Filters())

#LANGUAGE COMO PIVOTE
def get_query3(filters = Filters()):
    publication = PublicationStats(current_app.config.get("SERVER"), current_app.config.get("PUBLICATIONSTATS"))
    response = publication.run_query(elasticQueries[filters.scope]["tabulations"][filters.tabulation]["query"], filters = filters)
    response = response["aggregations"]["by_all"]["by_language"]["buckets"]
    DATA = {}
    DATA["map"] = {}
    DATA["treemap"] = {}
    DATA["map"]["by_collection"],DATA["treemap"]["by_collection"] = data_process(response,"by_collection",0)
    DATA["map"]["by_area"],DATA["treemap"]["by_area"] = data_process(response,"by_area",0)
    DATA["map"]["by_affiliation"],DATA["treemap"]["by_affiliation"] = data_process(response,"by_affiliation",0)
    DATA["map"]["by_journal"],DATA["treemap"]["by_journal"] = data_process(response,"by_journal",0)
    DATA["map"]["by_year"],DATA["treemap"]["by_year"] = data_process(response,"by_year",1)

    return render_template("index/index.html", data = DATA,filters=Filters())

#AFILIACIÓN DEL AUTOR COMO PIVOTE
def get_query4(filters = Filters()):
    publication = PublicationStats(current_app.config.get("SERVER"), current_app.config.get("PUBLICATIONSTATS"))
    response = publication.run_query(elasticQueries[filters.scope]["tabulations"][filters.tabulation]["query"], filters = filters)
    response = response["aggregations"]["by_all"]["by_affiliation"]["buckets"]
    DATA = {}
    DATA["map"] = {}
    DATA["treemap"] = {}
    DATA["map"]["by_collection"],DATA["treemap"]["by_collection"] = data_process(response,"by_collection",0)
    DATA["map"]["by_area"],DATA["treemap"]["by_area"] = data_process(response,"by_area",0)
    DATA["map"]["by_language"],DATA["treemap"]["by_language"] = data_process(response,"by_language",0)
    DATA["map"]["by_journal"],DATA["treemap"]["by_journal"] = data_process(response,"by_journal",0)
    DATA["map"]["by_year"],DATA["treemap"]["by_year"] = data_process(response,"by_year",1)

    return render_template("index/index.html", data = DATA,filters=Filters())

#JOURNAL COMO PIVOTE
def get_query5(filters = Filters()):
    publication = PublicationStats(current_app.config.get("SERVER"), current_app.config.get("PUBLICATIONSTATS"))
    response = publication.run_query(elasticQueries[filters.scope]["tabulations"][filters.tabulation]["query"], filters = filters)
    response = response["aggregations"]["by_all"]["by_journal"]["buckets"]
    DATA = {}
    DATA["map"] = {}
    DATA["treemap"] = {}
    DATA["map"]["by_collection"],DATA["treemap"]["by_collection"] = data_process(response,"by_collection",0)
    DATA["map"]["by_area"],DATA["treemap"]["by_area"] = data_process(response,"by_area",0)
    DATA["map"]["by_affiliation"],DATA["treemap"]["by_affiliation"] = data_process(response,"by_affiliation",0)
    DATA["map"]["by_language"],DATA["treemap"]["by_language"] = data_process(response,"by_language",0)
    DATA["map"]["by_year"],DATA["treemap"]["by_year"] = data_process(response,"by_year",1)

    return render_template("index/index.html", data = DATA,filters=Filters())

def coauthors_graph(filters = Filters):
#    if filters == None:
#        pass
    if os.path.exists(current_app.config.get("CACHE_FOLDER_PATH")+filters.md5+'.json'):
        with open(current_app.config.get("CACHE_FOLDER_PATH")+filters.md5+'.json') as data_file:    
            json_data = json.load(data_file)
        net = json_data['Net']
        source_edges = json_data['SourceEdges']
        G = json_data['G']
        collection_graph = json_data['CollectionGraph']
    else:
        publication = PublicationStats(current_app.config.get("SERVER"), current_app.config.get("PUBLICATIONSTATS"))
        response = publication.run_query2(elasticQueries["1"]["tabulations"]["7"]["query"], filters = filters)
        response = response["hits"]["hits"]
        net = {}
        COUNTRY_REL = {}
        for i in response:
            aux = i["fields"]["aff_countries"]
            year= i["fields"]["publication_year"]
            for j in aux:
                k = aux.index(j)+1
                while True:
                    if (k > len(aux)):
                        break
                    if str(aux[k-1]) != "undefined" and str(j) != "undefined":
                        if     str(aux[k-1]) in country_code_inv and str(j) in country_code_inv and str(aux[k-1]) not in not_collections and str(j) not in not_collections: #Poner restrincion not_collections en la query                 
                            if ( country_code_inv[str(aux[k-1])] not in net):                    
                                net[country_code_inv[str(aux[k-1])] ] = {}
                                COUNTRY_REL[country_code_inv[str(aux[k-1])]] = {}                                    
                            if ( country_code_inv[str(j)] not in net):
                                net[ country_code_inv[str(j)] ] = {}
                                COUNTRY_REL[country_code_inv[str(j)]] = {}
                            if ( country_code_inv[str(aux[k-1])] not in net[ country_code_inv[str(j)] ]):
                                net[ country_code_inv[str(j)] ][ country_code_inv[str(aux[k-1])] ] = 0
                            if ( country_code_inv[(str(j))] not in net[ country_code_inv[str(aux[k-1])] ] ):
                                net[ country_code_inv[str(aux[k-1])] ][ country_code_inv[j] ] = 0                            
                            if (str(year[0]) not in COUNTRY_REL[country_code_inv[str(aux[k-1])]]):
                                COUNTRY_REL[country_code_inv[str(aux[k-1])]][str(year[0])] = 0    
                            if (str(year[0]) not in COUNTRY_REL[country_code_inv[str(j)]]):
                                COUNTRY_REL[ country_code_inv[(str(j))]  ][str(year[0])] = 0
                            COUNTRY_REL[ country_code_inv[(str(j))]  ][str(year[0])] += 1
                            COUNTRY_REL[country_code_inv[str(aux[k-1])]][str(year[0])] += 1
                            net[ country_code_inv[str(aux[k-1])] ][ country_code_inv[str(j)] ]+=1
                            net[ country_code_inv[str(j)] ][ country_code_inv[str(aux[k-1])] ]+=1
                    k+=1
        collection_graph = {}
        for i in COUNTRY_REL:
            aux = []
            for y in COUNTRY_REL[i]:
                aux.append([y,COUNTRY_REL[i][y]])
            collection_graph[i] = [{"key":"Relations","values":aux}]                    
        minval = 100000000
        maxval = 0
        source_edges = {}
        for source in net:
            source_edges[source] = 0
            for target in net[source]:
                source_edges[source] += net[source][target]
        G = {"nodes":[],"edges":[]}    
        for source in source_edges:
            sum = 0
            for target in net[str(source)]:
                sum += net[str(source)][target]
            G["nodes"].append({"id": str(source),"label":country_name[country_code_rev[str(source)]],"x":random.random(),"y":random.random(),"size": sum })                    
        eid = 0
        auxedge = []
        for source in net:
            for target in net[source]:
                arraydata1 = source+"<->"+target
                arraydata2 = target+"<->"+source                    
                if (arraydata1 not in auxedge) and (arraydata2 not in auxedge):                        
                    auxedge.append(arraydata1)
                    G["edges"].append({"id":"e"+str(eid),"source":source,"target":target,"size":net[source][target]})
                    eid+=1
        #Creamos un json de cache
        j = {
            'Filters': filters.serialize,
            'Net': net,
            'SourceEdges': source_edges,
            'G': G,
            'CollectionGraph': collection_graph
        }
#        with open(current_app.config.get("CACHE_FOLDER_PATH")+filters.md5+'.json', 'w') as json_file:
#            json.dump(j, json_file, ensure_ascii=True, sort_keys=True, indent=4)    
    return net,source_edges,G,collection_graph

def collection_graph(filters = Filters):
    if os.path.exists(current_app.config.get("CACHE_FOLDER_PATH")+filters.md5+'.json'):
        #Obtenemos el json en cache
        with open(current_app.config.get("CACHE_FOLDER_PATH")+filters.md5+'.json') as data_file:    
            json_data = json.load(data_file)
        net = json_data['Net']
        source_edges = json_data['SourceEdges']
        choices = json_data['Choices']
        aux = json_data['Aux']
        collection_graph = json['CollectionGraph']
    else:
        publication = PublicationStats(current_app.config.get("SERVER"), current_app.config.get("PUBLICATIONSTATS"))    
        response = publication.run_query2(elasticQueries["1"]["tabulations"]["7b"]["query"], filters = filters)
        response = response["hits"]["hits"]
        net = {}
        COUNTRY_REL = {}
        for i in response:
            aux = i["fields"]["aff_countries"]
            year= i["fields"]["publication_year"]            
            if str(i["fields"]["collection"][0]) not in not_collections: #Poner restrincion not_collections en la query 
                target = country_collections[str(i["fields"]["collection"][0])]
                for j in aux:
                    if str(j) != "undefined" and  str(j) in country_code_inv:
                        source = country_code_inv[str(j)]        
                        if (source not in net):
                            net[source] = {}
                            COUNTRY_REL[source] = {}
                        if (target not in net[source]):
                            net[source][target] = 0
                        if (str(year[0]) not in COUNTRY_REL[source]):
                            COUNTRY_REL[source][str(year[0])] = 0                                            
                        net[source][target]+=1    
                        COUNTRY_REL[source][str(year[0])]+=1
        collection_graph = {}                    
        for i in COUNTRY_REL:
            aux = []
            for y in COUNTRY_REL[i]:
                aux.append([y,COUNTRY_REL[i][y]])
            collection_graph[i] = [{"key":"Relations","values":aux}]
        source_edges = {}
        for source in net:
            for target in net[source]:
                if target not in source_edges:
                    source_edges[target] = 0                    
                source_edges[target] += net[source][target]
        choices = {}
        for i in net:
            choices[i] = [len(net[i]),round((float(len(net[i]))/float(len(collections_dict)))*100,1)] #total de eleccion / total colecciones %
        aux = {}
        for i in source_edges:        
            aux[i] = []
            for j in net:
                if i in net[j]:
                    aux[i].append(j)
        #Creamos un json de caché
        j = {
            'Filters': filters.serialize,
            'Net': net,
            'SourceEdges': source_edges,
            'Choices': choices,
            'Aux': aux,
            'CollectionGraph': collection_graph
        }
#        with open(current_app.config.get("CACHE_FOLDER_PATH")+filters.md5+'.json', 'w') as json_file:
#            json.dump(j, json_file, ensure_ascii=True, sort_keys=True, indent=4)    
    return net,source_edges,choices,aux,collection_graph