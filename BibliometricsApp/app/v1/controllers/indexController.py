#!/usr/bin/env python
# -*- coding: utf-8 -*-
from flask import Blueprint, render_template,\
                  session, redirect, url_for,\
                  request, jsonify, make_response,\
                  flash, current_app
from datetime import datetime
from math import log
from app.v1.helpers.dicts import collections as collections_dict, country_collections, thematic_areas as areas, country_code_inv, country_name,country_code_rev,not_collections, collections
from app.v1.helpers.elasticQueries import elasticQueries
from app.v1.helpers.ISO_3166_ALPHA3 import ISO_3166_ALPHA3
from app.v1.classes.filters import Filters
from app.v1.classes.publicationStats import PublicationStats

import pyexcel as pe
import StringIO
import requests
import os.path
import random
import json

VERSION = "v1"
mod_index = Blueprint('indexController', __name__, url_prefix='/'+VERSION)

#-- ----------------------------------------------------------------

@mod_index.route('/')
def index():
    filters = Filters()
    publication = PublicationStats(current_app.config.get("SERVER"), current_app.config.get("PUBLICATIONSTATS"))
    response = publication.data_index()
    by_collection = response["aggregations"]["by_collection"]["buckets"]
    data = []
    for i in by_collection:
      if str(i["key"]) in country_collections and str(i["key"]) not in not_collections:       
        alpha3 = country_collections[str(i["key"])]
        data.append({
          "country-code": ISO_3166_ALPHA3[alpha3]["code"],
          "country-name": ISO_3166_ALPHA3[alpha3]["fullname"],
          "documents": i["doc_count"],
          "journals": len(i["by_journal"]["buckets"])
        })
    return render_template("v1/index/index.html", filters = filters, data = data)
    

#-- ----------------------------------------------------------------

@mod_index.route('/index_test')
def index_old():
    filters = Filters()
    publication = PublicationStats(current_app.config.get("SERVER"), current_app.config.get("PUBLICATIONSTATS"))
    response = publication.data_index()
    by_collection = response["aggregations"]["by_collection"]["buckets"]
    data = []
    for i in by_collection:
      if str(i["key"]) in country_collections and str(i["key"]) not in not_collections:       
        alpha3 = country_collections[str(i["key"])]
        data.append({
          "country-code": ISO_3166_ALPHA3[alpha3]["code"],
          "country-name": ISO_3166_ALPHA3[alpha3]["fullname"],
          "documents": i["doc_count"],
          "journals": len(i["by_journal"]["buckets"])
        })
    return render_template("v1/index/index_test.html", filters = filters, data = data)

#-- ----------------------------------------------------------------

@mod_index.route('/graph', methods = ['GET', 'POST'])
def graph():
    filters = Filters()
#    #Leemos Formulario y obtenemos filtros
#    form =  request.form
#
#    #Tipo Visualización
#    filters.vistype = int(form.get('vistype', 0))
#    
#    #Años
#    filters.years, filters.from_year, filters.to_year = 'ALL', 1900, datetime.now().year
#    filters.years = form.get('years')
#    if filters.years == 'ALL':
#        filters.to_year = datetime.now().year
#        filters.from_year = 1900
#    elif filters.years == 'RANGE':
#        filters.to_year = form.get('years[to]', datetime.now().year)
#        filters.from_year = form.get('years[from]', min(filters.to_year, datetime.now().year))
#
#    #Unidad de Estudio
#    filters.study_unit = int(form.get('study_unit', 1))
#    filters.collections = form.getlist('study_unit[collections][]')
#    if 'ALL' in filters.collections:
#        filters.collections = []
#    filters.journals = form.getlist('study_unit[journals][]')
#    if 'ALL' in filters.journals:
#        filters.journals = []
#    filters.document_type = form.get('study_unit[document-types]', None)
#    filters.document_type_details = form.getlist('study_unit[document_type_details][]')
#    if 'ALL' in filters.document_type_details:
#        filters.document_type_details = []
#    
#    #Áreas Temáticas
#    filters.thematic_areas = form.getlist('thematic_areas[]')
#    if 'ALL' in filters.thematic_areas:
#        filters.thematic_areas = []
#    filters.wos_thematic_areas = form.getlist('wos_thematic_areas[]')
#    if 'ALL' in filters.wos_thematic_areas:
#        filters.wos_thematic_areas = []
#
#    #Lenguajes
#    filters.languages = form.getlist('languages[]')
#    if u'ALL' in filters.languages:
#        filters.languages = []

    #retornamos visualización según corresponsa
#    if filters.vistype == 0: #Documents
#    return documents_graph(filters)
#    elif filters.vistype == 1: #Citations
#        return citations_graph(filters)
#    elif filters.vistype == 2: #Geo Co-Authors Network
    return coauthors_graph(filters)
#    elif filters.vistype == 3: #Colecction Choice Network
#        return collection_graph(filters)
#    else:
#        print "default"
#        return citations_graph(filters)

#-- ----------------------------------------------------------------

#Misma función para citations y publications
def citations_info(filters = Filters):
    publication = PublicationStats(current_app.config.get("SERVER"), current_app.config.get("PUBLICATIONSTATS"))
    response = publication.query_citations(filters)
    citations = response["aggregations"]["group_by_collection"]["by_collection"]["buckets"]
    cant, data, journals = {}, {}, {}
    maxval, minval, maxcit, mincit = 0, 10000000, 0, 10000000
    for i in citations:
        if str(i["key"]) not in not_collections: #Poner restrincion not_collections en la query          
            journals[country_collections[str(i["key"])]] = {}
            for j in i["by_journal"]["buckets"]:
                journals[country_collections[str(i["key"])]] [j["key"]]    = {}
                journals[country_collections[str(i["key"])]] [j["key"]]['citation']    = 0
                journals[country_collections[str(i["key"])]] [j["key"]]['publication'] = 0
                journals[country_collections[str(i["key"])]] [j["key"]]['publication'] = j["doc_count"]
                journals[country_collections[str(i["key"])]] [j["key"]]['citation'] = j["citation_sum"]["sum_other_doc_count"]
            cant[country_collections[str(i["key"])]] = i["doc_count"]
            citaux = 0
            for j in i["by_journal"]["buckets"]:
                citaux += j["citation_sum"]["sum_other_doc_count"]
            #ASIGNO CITAUX A 1 SI ES 0, YA QUE NO EXISTE LOG 0
            if citaux == 0:
                citaux = 1
            data[country_collections[str(i["key"])]] = citaux
            maxval = max([maxval, i["doc_count"]])
            minval = min([minval, i["doc_count"]])
            maxcit = max([maxcit, citaux])
            mincit = min([mincit, citaux])
    collection_graph = {}
    by_year = response["aggregations"]["group_by_collection"]["by_year"]["buckets"]
    for i in by_year:
        doc_count = []
        cit_count = []    
        if str(i["key"]) not in not_collections:                     
            collection_graph[country_collections[str(i["key"])]] = []                                     
            aux = i["by_year_documents"]["buckets"]
            for j in aux:
                cit_count_aux = 0                        
                aux2 = j["by_year_citation"]["buckets"]
                for k in aux2:
                    cit_count_aux += k["key"]*k["doc_count"]
                doc_count.append([int(j["key"]),j["doc_count"]])
                cit_count.append([int(j["key"]),cit_count_aux])                                         
            collection_graph[country_collections[str(i["key"])]].append({"key":"Documents","values":doc_count})
            collection_graph[country_collections[str(i["key"])]].append({"key":"Citations","values":cit_count})
    return data, cant, journals, collection_graph

#-- ----------------------------------------------------------------

def documents_graph(filters = Filters):
    if os.path.exists(current_app.config.get("CACHE_FOLDER_PATH")+filters.md5+'.json'):
        #Obtenemos el json en cache
        with open(current_app.config.get("CACHE_FOLDER_PATH")+filters.md5+'.json') as data_file:    
            json_data = json.load(data_file)
        data = json_data['Data']
        cant = json_data['Cant']
        journals = json_data['Journals']
        collection = json_data['Collection']
    else:
        data, cant, journals, collection = citations_info(filters)
        #Creamos un json de caché
        j = {
            'Filters': filters.serialize,
            'Data': data,
            'Cant': cant,
            'Journals': journals,
            'Collection': collection
        }
        with open(current_app.config.get("CACHE_FOLDER_PATH")+filters.md5+'.json', 'w') as json_file:
            json.dump(j, json_file, ensure_ascii=True, sort_keys=True, indent=4)
    return render_template("v1/graph/documents_graph.html", filters = filters, data = cant, journals=journals, collection=collection)

#-- ----------------------------------------------------------------

def citations_graph(filters = Filters):
    if os.path.exists(current_app.config.get("CACHE_FOLDER_PATH")+filters.md5+'.json'):
        #Obtenemos el json en cache
        with open(current_app.config.get("CACHE_FOLDER_PATH")+filters.md5+'.json') as data_file:    
            json_data = json.load(data_file)
        data = json_data['Data']
        cant = json_data['Cant']
        journals = json_data['Journals']
        collection = json_data['Collection']        
    else:
        data, cant, journals, collection = citations_info(filters)
        #Creamos un json de caché
        j = {
            'Filters': filters.serialize,
            'Data': data,
            'Cant': cant,
            'Journals': journals,
            'Collection': collection            
        }
        with open(current_app.config.get("CACHE_FOLDER_PATH")+filters.md5+'.json', 'w') as json_file:
            json.dump(j, json_file, ensure_ascii=True, sort_keys=True, indent=4)
    return render_template("v1/graph/citations_graph.html", filters = filters, data = data, journals=journals, collection=collection)

#-- ----------------------------------------------------------------

def coauthors_graph(filters = Filters):
    if os.path.exists(current_app.config.get("CACHE_FOLDER_PATH")+filters.md5+'.json'):
        #Obtenemos el json en cache
        with open(current_app.config.get("CACHE_FOLDER_PATH")+filters.md5+'.json') as data_file:    
            json_data = json.load(data_file)
        net = json_data['Net']
        source_edges = json_data['SourceEdges']
        G = json_data['G']
        collection_graph = json_data['CollectionGraph']
    else:
        publication = PublicationStats(current_app.config.get("SERVER"), current_app.config.get("PUBLICATIONSTATS"))    
        response  = publication.query_coauthors(filters)
        response = response["hits"]["hits"]
        net = {}
        COUNTRY_REL = {}
        for i in response:
            aux = i["fields"]["aff_countries"]
            year= i["fields"]["publication_year"]
            for j in aux:
                k = aux.index(j)+1
                while True:
                    if (k > len(aux)):
                        break
                    if str(aux[k-1]) != "undefined" and str(j) != "undefined":
                        if     str(aux[k-1]) in country_code_inv and str(j) in country_code_inv and str(aux[k-1]) not in not_collections and str(j) not in not_collections: #Poner restrincion not_collections en la query                 
                            if ( country_code_inv[str(aux[k-1])] not in net):                    
                                net[country_code_inv[str(aux[k-1])] ] = {}
                                COUNTRY_REL[country_code_inv[str(aux[k-1])]] = {}                                    
                            if ( country_code_inv[str(j)] not in net):
                                net[ country_code_inv[str(j)] ] = {}
                                COUNTRY_REL[country_code_inv[str(j)]] = {}
                            if ( country_code_inv[str(aux[k-1])] not in net[ country_code_inv[str(j)] ]):
                                net[ country_code_inv[str(j)] ][ country_code_inv[str(aux[k-1])] ] = 0
                            if ( country_code_inv[(str(j))] not in net[ country_code_inv[str(aux[k-1])] ] ):
                                net[ country_code_inv[str(aux[k-1])] ][ country_code_inv[j] ] = 0                            
                            if (str(year[0]) not in COUNTRY_REL[country_code_inv[str(aux[k-1])]]):
                                COUNTRY_REL[country_code_inv[str(aux[k-1])]][str(year[0])] = 0    
                            if (str(year[0]) not in COUNTRY_REL[country_code_inv[str(j)]]):
                                COUNTRY_REL[ country_code_inv[(str(j))]  ][str(year[0])] = 0
                            COUNTRY_REL[ country_code_inv[(str(j))]  ][str(year[0])] += 1
                            COUNTRY_REL[country_code_inv[str(aux[k-1])]][str(year[0])] += 1
                            net[ country_code_inv[str(aux[k-1])] ][ country_code_inv[str(j)] ]+=1
                            net[ country_code_inv[str(j)] ][ country_code_inv[str(aux[k-1])] ]+=1
                    k+=1
        collection_graph = {}
        for i in COUNTRY_REL:
            aux = []
            for y in COUNTRY_REL[i]:
                aux.append([y,COUNTRY_REL[i][y]])
            collection_graph[i] = [{"key":"Relations","values":aux}]                    
        minval = 100000000
        maxval = 0
        source_edges = {}
        for source in net:
            source_edges[source] = 0
            for target in net[source]:
                source_edges[source] += net[source][target]
        G = {"nodes":[],"edges":[]}    
        for source in source_edges:
            sum = 0
            for target in net[str(source)]:
                sum += net[str(source)][target]
            G["nodes"].append({"id": str(source),"label":country_name[country_code_rev[str(source)]],"x":random.random(),"y":random.random(),"size": sum })                    
        eid = 0
        auxedge = []
        for source in net:
            for target in net[source]:
                arraydata1 = source+"<->"+target
                arraydata2 = target+"<->"+source                    
                if (arraydata1 not in auxedge) and (arraydata2 not in auxedge):                        
                    auxedge.append(arraydata1)
                    G["edges"].append({"id":"e"+str(eid),"source":source,"target":target,"size":net[source][target]})
                    eid+=1
        #Creamos un json de cache
        j = {
            'Filters': filters.serialize,
            'Net': net,
            'SourceEdges': source_edges,
            'G': G,
            'CollectionGraph': collection_graph
        }
        with open(current_app.config.get("CACHE_FOLDER_PATH")+filters.md5+'.json', 'w') as json_file:
            json.dump(j, json_file, ensure_ascii=True, sort_keys=True, indent=4)    
    return render_template("v1/graph/coauthors_graph.html", filters = filters, data = [net,source_edges,G,collection_graph])                

#-- ----------------------------------------------------------------

def collection_graph(filters = Filters):
    if os.path.exists(current_app.config.get("CACHE_FOLDER_PATH")+filters.md5+'.json'):
        #Obtenemos el json en cache
        with open(current_app.config.get("CACHE_FOLDER_PATH")+filters.md5+'.json') as data_file:    
            json_data = json.load(data_file)
        net = json_data['Net']
        source_edges = json_data['SourceEdges']
        choices = json_data['Choices']
        aux = json_data['Aux']
        collection_graph = json['CollectionGraph']
    else:
        publication = PublicationStats(current_app.config.get("SERVER"), current_app.config.get("PUBLICATIONSTATS"))    
        response  = publication.query_collection(filters)
        response = response["hits"]["hits"]
        net = {}
        COUNTRY_REL = {}
        for i in response:
            aux = i["fields"]["aff_countries"]
            year= i["fields"]["publication_year"]            
            if str(i["fields"]["collection"][0]) not in not_collections: #Poner restrincion not_collections en la query 
                target = country_collections[str(i["fields"]["collection"][0])]
                for j in aux:
                    if str(j) != "undefined" and  str(j) in country_code_inv:
                        source = country_code_inv[str(j)]        
                        if (source not in net):
                            net[source] = {}
                            COUNTRY_REL[source] = {}
                        if (target not in net[source]):
                            net[source][target] = 0
                        if (str(year[0]) not in COUNTRY_REL[source]):
                            COUNTRY_REL[source][str(year[0])] = 0                                            
                        net[source][target]+=1    
                        COUNTRY_REL[source][str(year[0])]+=1
        collection_graph = {}                    
        for i in COUNTRY_REL:
            aux = []
            for y in COUNTRY_REL[i]:
                aux.append([y,COUNTRY_REL[i][y]])
            collection_graph[i] = [{"key":"Relations","values":aux}]
        source_edges = {}
        for source in net:
            for target in net[source]:
                if target not in source_edges:
                    source_edges[target] = 0                    
                source_edges[target] += net[source][target]
        choices = {}
        for i in net:
            choices[i] = [len(net[i]),round((float(len(net[i]))/float(len(collections_dict)))*100,1)] #total de eleccion / total colecciones %
        aux = {}
        for i in source_edges:        
            aux[i] = []
            for j in net:
                if i in net[j]:
                    aux[i].append(j)
        #Creamos un json de caché
        j = {
            'Filters': filters.serialize,
            'Net': net,
            'SourceEdges': source_edges,
            'Choices': choices,
            'Aux': aux,
            'CollectionGraph': collection_graph
        }
        with open(current_app.config.get("CACHE_FOLDER_PATH")+filters.md5+'.json', 'w') as json_file:
            json.dump(j, json_file, ensure_ascii=True, sort_keys=True, indent=4)    
    return render_template("v1/graph/collections_graph.html", filters = filters, data = [net,source_edges,choices,aux,collection_graph])


#-- ----------------------------------------------------------------
#Conjunto de métodos para crear el json de filtros avanzados
#-- ----------------------------------------------------------------
@mod_index.route('/gfilters')
def gfilters():
    #Obtenemos información para los filtros
    publication = PublicationStats(current_app.config.get("SERVER"), current_app.config.get("PUBLICATIONSTATS"))
    data = {}
    data['thematicAreas']  = get_subject_areas(publication)
    data['wosThematicAreas']  = get_wos_subject_areas(publication)
    data['collections'] = [ [ c, collections_dict[c][0] ] for c in get_collections(publication) ]
    data['documentTypes'] = get_document_types(publication)
    data['languages'] = get_languages(publication)
    data['journals'] = get_journals(publication)
    return jsonify(data)

#-- ----------------------------------------------------------------

def get_thematic_areas(publication):
    response = publication.query_3()
    return [ a['key'] for a in response["aggregations"]["by_area"]["buckets"] ]

def get_subject_areas(publication):
    response = publication.query_article_distinct_field("subject_areas")
    return [ t['key'] for t in response["aggregations"]["by_value"]["buckets"] ]

def get_wos_subject_areas(publication):
    response = publication.query_article_distinct_field("wos_subject_areas")
    return [ t['key'] for t in response["aggregations"]["by_value"]["buckets"] ]

def get_collections(publication):
    response = publication.query_article_distinct_field("collection")
    return [ c['key'] for c in response["aggregations"]["by_value"]["buckets"] ]

def get_document_types(publication):
    response = publication.query_article_distinct_field("document_type")
    return [ dt['key'] for dt in response["aggregations"]["by_value"]["buckets"] ]

def get_languages(publication):
    response = publication.query_article_distinct_field("languages")
    return [ l['key'] for l in response["aggregations"]["by_value"]["buckets"] ]

def get_journals(publication):
    response = publication.query_article_distinct_field("journal_title")
    return [ j['key'] for j in response["aggregations"]["by_value"]["buckets"] ]

#-- ----------------------------------------------------------------
#Fin métodos para crear el json de filtros avanzados
#-- ----------------------------------------------------------------
