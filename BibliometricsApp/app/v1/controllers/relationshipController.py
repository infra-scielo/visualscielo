#!/usr/bin/env python
# -*- coding: utf-8 -*-
from flask import Blueprint, render_template,\
                  session, redirect, url_for,\
                  request, jsonify, make_response,\
                  flash, current_app
from datetime import datetime
from math import log
from app.v1.helpers.dicts import collections, country_collections,\
                              thematic_areas as areas, country_code_inv,\
                              country_name, country_code_rev,\
                              not_collections
from app.v1.helpers.elasticQueries import elasticQueries
from app.v1.helpers.ISO_3166_ALPHA3 import ISO_3166_ALPHA3
from app.v1.helpers.ISO_639_ALPHA2 import ISO_639_ALPHA2
from app.v1.helpers.formHelper import *
from app.v1.classes.filters import Filters
from app.v1.classes.publicationStats import PublicationStats

import networkx as nx
import numpy as np
import random

VERSION = "v1"
mod_relationship = Blueprint('relationshipController', __name__, url_prefix='/'+VERSION+'/relationships')

#-- ----------------------------------------------------------------

@mod_relationship.route('/', methods = ['POST'])
def r_index():
  #Leemos Formulario y obtenemos filtros
  form =  request.form
  filters = process_filters_form(form)
  
  if(filters.tabulation == "r1"):
    return tabulation_r1(filters)
  elif(filters.tabulation == "r2"):
    return tabulation_r2(filters)
  elif(filters.tabulation == "r3"):
    return tabulation_r3(filters)
  else:
    flash('Impossible to answer the request at this time', 'error')
    return redirect(url_for("indexController.index"))

#-- ----------------------------------------------------------------

def tabulation_r1(filters = Filters):
  publication = PublicationStats(current_app.config.get("SERVER"), current_app.config.get("PUBLICATIONSTATS"))
  response = publication.run_query2(elasticQueries[filters.scope]["tabulations"][filters.tabulation]["query"], filters = filters)
  data_network = aff_network(response)

  #MAPA
  response  = publication.query_coauthors(filters)
  response = response["hits"]["hits"]  
  net = {}
  COUNTRY_REL = {}
  for i in response:
      aux = i["fields"]["aff_countries"]
      year= i["fields"]["publication_year"]
      for j in aux:
          k = aux.index(j)+1
          while True:
              if (k > len(aux)):
                  break
              if str(aux[k-1]) != "undefined" and str(j) != "undefined":
                  if     str(aux[k-1]) in country_code_inv and str(j) in country_code_inv and str(aux[k-1]) not in not_collections and str(j) not in not_collections: #Poner restrincion not_collections en la query                 
                      if ( country_code_inv[str(aux[k-1])] not in net):                    
                          net[country_code_inv[str(aux[k-1])] ] = {}
                          COUNTRY_REL[country_code_inv[str(aux[k-1])]] = {}                                    
                      if ( country_code_inv[str(j)] not in net):
                          net[ country_code_inv[str(j)] ] = {}
                          COUNTRY_REL[country_code_inv[str(j)]] = {}
                      if ( country_code_inv[str(aux[k-1])] not in net[ country_code_inv[str(j)] ]):
                          net[ country_code_inv[str(j)] ][ country_code_inv[str(aux[k-1])] ] = 0
                      if ( country_code_inv[(str(j))] not in net[ country_code_inv[str(aux[k-1])] ] ):
                          net[ country_code_inv[str(aux[k-1])] ][ country_code_inv[j] ] = 0                            
                      if (str(year[0]) not in COUNTRY_REL[country_code_inv[str(aux[k-1])]]):
                          COUNTRY_REL[country_code_inv[str(aux[k-1])]][str(year[0])] = 0    
                      if (str(year[0]) not in COUNTRY_REL[country_code_inv[str(j)]]):
                          COUNTRY_REL[ country_code_inv[(str(j))]  ][str(year[0])] = 0
                      COUNTRY_REL[ country_code_inv[(str(j))]  ][str(year[0])] += 1
                      COUNTRY_REL[country_code_inv[str(aux[k-1])]][str(year[0])] += 1
                      net[ country_code_inv[str(aux[k-1])] ][ country_code_inv[str(j)] ]+=1
                      net[ country_code_inv[str(j)] ][ country_code_inv[str(aux[k-1])] ]+=1
              k+=1
  collection_graph = {}
  for i in COUNTRY_REL:
      aux = []
      for y in COUNTRY_REL[i]:
          aux.append([y,COUNTRY_REL[i][y]])
      collection_graph[i] = [{"key":"Relations","values":aux}]                    
  minval = 100000000
  maxval = 0
  source_edges = {}
  for source in net:
      source_edges[source] = 0
      for target in net[source]:
          source_edges[source] += net[source][target]
  G = {"nodes":[],"edges":[]}    
  for source in source_edges:
      sum = 0
      for target in net[str(source)]:
          sum += net[str(source)][target]
      G["nodes"].append({"id": str(source),"label":country_name[country_code_rev[str(source)]],"x":random.random(),"y":random.random(),"size": sum })                    
  eid = 0
  auxedge = []
  for source in net:
      for target in net[source]:
          arraydata1 = source+"<->"+target
          arraydata2 = target+"<->"+source                    
          if (arraydata1 not in auxedge) and (arraydata2 not in auxedge):                        
              auxedge.append(arraydata1)
              G["edges"].append({"id":"e"+str(eid),"source":source,"target":target,"size":net[source][target]})
              eid+=1
  return render_template("v1/relationships/tabulation_r1.html", filters = filters, data_network = data_network, data_map = [net,source_edges,G,collection_graph])

#-- ----------------------------------------------------------------

def tabulation_r2(filters = Filters):
  publication = PublicationStats(current_app.config.get("SERVER"), current_app.config.get("PUBLICATIONSTATS"))
  response = publication.run_query2(elasticQueries[filters.scope]["tabulations"][filters.tabulation]["query"], filters = filters)
  data_rings = rings_for_type(response, "subject_areas")
  return render_template("v1/relationships/tabulation_r2.html", filters = filters, data_rings = data_rings)

#-- ----------------------------------------------------------------

def tabulation_r3(filters = Filters):
  publication = PublicationStats(current_app.config.get("SERVER"), current_app.config.get("PUBLICATIONSTATS"))
  response = publication.run_query2(elasticQueries[filters.scope]["tabulations"][filters.tabulation]["query"], filters = filters)
  data_rings = rings_for_collection(response)
  return render_template("v1/relationships/tabulation_r3.html", filters = filters, data_rings = data_rings)

#-- ----------------------------------------------------------------

def rings_for_collection(data):
  E = {}
  data = data["hits"]["hits"]
  for d in data:
    aff_countries = d["fields"]["aff_countries"]
    collections = d["fields"]["collection"]
    for i in aff_countries:
      if i != 'undefined':
        if i not in E:
          E[i] = {}
        for j in collections:
          if j in country_collections and j not in not_collections:
            j = ISO_3166_ALPHA3[country_collections[j]]["fullname"]
            if j not in E[i]:
              E[i][j] = 0
            E[i][j] += 1
    EDGES = []
    for source in E:
      for target in E[source]:
        print target
        EDGES.append({"strength": E[source][target], "source": source, "target": target})
  return {"edges": EDGES}

def rings_for_type(data, key):
  E = {}
  data = data["hits"]["hits"]
  for d in data:
    aff_countries = d["fields"]["aff_countries"]
    aux = d["fields"][key]
    for i in aff_countries:
      if i != 'undefined':
        if i not in E:
          E[i] = {}
        for j in aux:
          if j not in E[i]:
            E[i][j] = 0
          E[i][j] += 1
    EDGES = []
    for source in E:
      for target in E[source]:
        print target
        EDGES.append({"strength": E[source][target], "source": source, "target": target})
  return {"edges": EDGES}

#def rings(data,type):
##    type: collection, subject_areas
#  data = data["hits"]["hits"]
#  N = {}
#  E = {}
#  for d in data:
#    aff_countries = d["fields"]["aff_countries"]
##        subject_areas = d["fields"]["subject_areas"]
#    subject_areas = d["fields"][type]
#    for i in aff_countries:
#      if i != 'undefined':
#        if i not in N:
#          N[i] = 0
#        if i not in E:
#          E[i] = {}
#        for j in subject_areas:
#          if j not in N:
#            N[j] = 0
#          if j not in E[i]:
#            E[i][j] = 0
#          E[i][j] += 1
#    EDGES = []
#    NODES = []
#    for i in N:
#      NODES.append({"id":i})
#    for source in E:
#      for target in E[source]:
#        EDGES.append({"strength": E[source][target], "source": source, "target": target})

def aff_network(data):
  G = nx.Graph()
  data = data["hits"]["hits"]
  for d in data:
    aff_countries = d["fields"]["aff_countries"]
    for i in range(len(aff_countries)):
      j = i+1
      while True:
        if j >= len(aff_countries):
          break
        if aff_countries[i] != 'undefined' and aff_countries[j] != 'undefined':
          G.add_edge(aff_countries[i],aff_countries[j])
        j += 1

  pos = nx.spring_layout(G)
  spring_layout = []
  for i in pos:
    spring_layout.append({"name":i, "x":float(pos[i][0]), "y":float(pos[i][1])})
  circular_layout = []
  pos = nx.circular_layout(G)
  for i in pos:
    circular_layout.append({"name":i, "x":float(pos[i][0]), "y":float(pos[i][1])})
  connections = []
  for i in G.edges():
    connections.append({"source":i[0], "target":i[1]})
  sample_data = []
  deg = G.degree()
  for i in deg:
    sample_data.append({"name":i,"degree":deg[i]})
  return {"data":sample_data,"connections":connections,"circular_layout":circular_layout,"spring_layout":spring_layout}
