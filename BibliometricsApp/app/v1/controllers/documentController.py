#!/usr/bin/env python
# -*- coding: utf-8 -*-
from flask import Blueprint, render_template,\
                  session, redirect, url_for,\
                  request, jsonify, make_response,\
                  flash, current_app
from datetime import datetime
from math import log
from app.v1.helpers.dicts import collections, country_collections,\
                              thematic_areas as areas, country_code_inv,\
                              country_name, country_code_rev,\
                              not_collections
from app.v1.helpers.elasticQueries import elasticQueries
from app.v1.helpers.ISO_3166_ALPHA2 import ISO_3166_ALPHA2
from app.v1.helpers.ISO_3166_ALPHA3 import ISO_3166_ALPHA3
from app.v1.helpers.ISO_639_ALPHA2 import ISO_639_ALPHA2
from app.v1.helpers.formHelper import *
from app.v1.classes.filters import Filters
from app.v1.classes.publicationStats import PublicationStats

import os.path
import json

VERSION = "v1"
mod_document = Blueprint('documentController', __name__, url_prefix='/'+VERSION+'/documents')

#-- ----------------------------------------------------------------

@mod_document.route('/', methods = ['POST'])
def d_index():
  #Leemos Formulario y obtenemos filtros
  form =  request.form
  filters = process_filters_form(form)  
  
  if(filters.tabulation == "t1"):
    return tabulation_t1(filters)
  elif(filters.tabulation == "t2"):
    return tabulation_t2(filters)
  elif(filters.tabulation == "t3"):
    return tabulation_t1(filters)
  elif(filters.tabulation == "t4"):
    return tabulation_t4(filters)
  elif(filters.tabulation == "t5"):
    return tabulation_t5(filters)
  elif(filters.tabulation == "t6"):
    return tabulation_t1(filters)
  elif(filters.tabulation == "t7"):
    return tabulation_t7(filters)
  else:
    flash('Impossible to answer the request at this time', 'error')
    return redirect(url_for("indexController.index"))

#-- ----------------------------------------------------------------
#-- ----------------------------------------------------------------
#-- ----------------------------------------------------------------

def tabulation_t1(filters = Filters):
  if not os.path.exists(current_app.config.get("CACHE_FOLDER_PATH")+VERSION+'/'+filters.md5+'.json'):
    publication = PublicationStats(current_app.config.get("SERVER"), current_app.config.get("PUBLICATIONSTATS"))
    response = publication.run_query(elasticQueries[filters.scope]["tabulations"][filters.tabulation]["query"], filters = filters)
    data_treemap, data_table = {}, {}
    data_treemap["by_area"], data_table["by_area"] = data_process_treemap_table(response, "by_collection", "by_area", True)
    data_treemap["by_language"], data_table["by_language"] = data_process_treemap_table(response, "by_collection", "by_language", True)
    data_treemap["by_affiliation"], data_table["by_affiliation"] = data_process_treemap_table(response, "by_collection", "by_affiliation", True)
    data_treemap["by_journal"], data_table["by_journal"] = data_process_treemap_table(response, "by_collection", "by_journal", True)
    data_treemap["by_year"], data_table["by_year"] = data_process_treemap_table(response, "by_collection", "by_year", False)

    data_map = {"keys": ["documents"],
                "by_year": data_treemap["by_year"],
                "by_area": data_treemap["by_area"]}
    #Añadimos keys y areas a la estructura
    for d in data_map["by_area"]:
      if not d["sub-group"] in data_map["keys"]:
        data_map["keys"].append(d["sub-group"])
      d[d["sub-group"]] = d["documents"]

    #Creamos un json de caché
    j = {
        'Filters'     : filters.serialize,
        'data_treemap': data_treemap,
        'data_table'  : data_table,
        'data_map'    : data_map,
    }
    with open(current_app.config.get("CACHE_FOLDER_PATH")+VERSION+'/'+filters.md5+'.json', 'w') as json_file:
        json.dump(j, json_file, ensure_ascii=True, sort_keys=True, indent=4)
  data_treemap, data_table, data_map = data_from_cache(filters, True, True, True)
  return render_template("v1/document/tabulation_t1.html", filters = filters, data_treemap = data_treemap, data_table = data_table, data_map = data_map).encode( "utf-8" )

#-- ----------------------------------------------------------------

def tabulation_t2(filters = Filters):
  if not os.path.exists(current_app.config.get("CACHE_FOLDER_PATH")+VERSION+'/'+filters.md5+'.json'):
    publication = PublicationStats(current_app.config.get("SERVER"), current_app.config.get("PUBLICATIONSTATS"))
    response = publication.run_query(elasticQueries[filters.scope]["tabulations"][filters.tabulation]["query"], filters = filters)
    data_treemap, data_table = {}, {}
    data_treemap["by_collection"], data_table["by_collection"] = data_process_treemap_table(response, "by_area", "by_collection", True)
    data_treemap["by_language"], data_table["by_language"] = data_process_treemap_table(response, "by_area", "by_language", True)
    data_treemap["by_affiliation"], data_table["by_affiliation"] = data_process_treemap_table(response, "by_area", "by_affiliation", True)
    data_treemap["by_journal"], data_table["by_journal"] = data_process_treemap_table(response, "by_area", "by_journal", True)
    data_treemap["by_year"], data_table["by_year"] = data_process_treemap_table(response, "by_area", "by_year", False)

    data_map = {}
    for d in data_treemap["by_collection"]:
      if data_map.has_key(d["group"]):
        data_map[d["group"]].append(d)
      else:
        data_map[d["group"]] = [d]

    #Creamos un json de caché
    j = {
        'Filters'     : filters.serialize,
        'data_treemap': data_treemap,
        'data_table'  : data_table,
        'data_map'    : data_map,
    }
    with open(current_app.config.get("CACHE_FOLDER_PATH")+VERSION+'/'+filters.md5+'.json', 'w') as json_file:
        json.dump(j, json_file, ensure_ascii=True, sort_keys=True, indent=4)
  data_treemap, data_table, data_map = data_from_cache(filters, True, True, True)
  return render_template("v1/document/tabulation_t2.html", filters = filters, data_treemap = data_treemap, data_table = data_table, data_map = data_map).encode( "utf-8" )

#-- ----------------------------------------------------------------

def tabulation_t4(filters = Filters):
  if not os.path.exists(current_app.config.get("CACHE_FOLDER_PATH")+VERSION+'/'+filters.md5+'.json'):
    publication = PublicationStats(current_app.config.get("SERVER"), current_app.config.get("PUBLICATIONSTATS"))
    response = publication.run_query(elasticQueries[filters.scope]["tabulations"][filters.tabulation]["query"], filters = filters)
    data_treemap, data_table = {}, {}
    data_treemap["by_collection"], data_table["by_collection"] = data_process_treemap_table(response, "by_language", "by_collection", True)
    data_treemap["by_area"], data_table["by_area"] = data_process_treemap_table(response, "by_language", "by_area", True)
    data_treemap["by_affiliation"], data_table["by_affiliation"] = data_process_treemap_table(response, "by_language", "by_affiliation", True)
    data_treemap["by_journal"], data_table["by_journal"] = data_process_treemap_table(response, "by_language", "by_journal", True)
    data_treemap["by_year"], data_table["by_year"] = data_process_treemap_table(response, "by_language", "by_year", False)

    data_map = {}
    for d in data_treemap["by_collection"]:
      if data_map.has_key(d["group"]):
        data_map[d["group"]].append(d)
      else:
        data_map[d["group"]] = [d]

    #Creamos un json de caché
    j = {
        'Filters'     : filters.serialize,
        'data_treemap': data_treemap,
        'data_table'  : data_table,
        'data_map'    : data_map,
    }
    with open(current_app.config.get("CACHE_FOLDER_PATH")+VERSION+'/'+filters.md5+'.json', 'w') as json_file:
        json.dump(j, json_file, ensure_ascii=True, sort_keys=True, indent=4)
  data_treemap, data_table, data_map = data_from_cache(filters, True, True, True)
  return render_template("v1/document/tabulation_t4.html", filters = filters, data_treemap = data_treemap, data_table= data_table, data_map = data_map, ISO_639_ALPHA2 = ISO_639_ALPHA2).encode( "utf-8" )

#-- ----------------------------------------------------------------

def tabulation_t5(filters = Filters):
  if not os.path.exists(current_app.config.get("CACHE_FOLDER_PATH")+VERSION+'/'+filters.md5+'.json'):
    publication = PublicationStats(current_app.config.get("SERVER"), current_app.config.get("PUBLICATIONSTATS"))
    response = publication.run_query(elasticQueries[filters.scope]["tabulations"][filters.tabulation]["query"], filters = filters)
    data_treemap, data_table = {}, {}
    data_treemap["by_collection"], data_table["by_collection"] = data_process_treemap_table(response, "by_affiliation", "by_collection", True)
    data_treemap["by_area"], data_table["by_area"] = data_process_treemap_table(response, "by_affiliation", "by_area", True)
    data_treemap["by_language"], data_table["by_language"] = data_process_treemap_table(response, "by_affiliation", "by_language", True)
    data_treemap["by_journal"], data_table["by_journal"] = data_process_treemap_table(response, "by_affiliation", "by_journal", True)
    data_treemap["by_year"], data_table["by_year"] = data_process_treemap_table(response, "by_affiliation", "by_year", False)

    data_map = []
    for d in data_treemap["by_year"]:
      d["year"] = d["sub-group"]
      data_map.append(d)

    #Creamos un json de caché
    j = {
        'Filters'     : filters.serialize,
        'data_treemap': data_treemap,
        'data_table'  : data_table,
        'data_map'    : data_map,
    }
    with open(current_app.config.get("CACHE_FOLDER_PATH")+VERSION+'/'+filters.md5+'.json', 'w') as json_file:
        json.dump(j, json_file, ensure_ascii=True, sort_keys=True, indent=4)
  data_treemap, data_table, data_map = data_from_cache(filters, True, True, True)
  return render_template("v1/document/tabulation_t5.html", filters = filters, data_treemap = data_treemap, data_table = data_table, data_map = data_map).encode( "utf-8" )

#-- ----------------------------------------------------------------

def tabulation_t7(filters = Filters):
  if not os.path.exists(current_app.config.get("CACHE_FOLDER_PATH")+VERSION+'/'+filters.md5+'.json'):
    publication = PublicationStats(current_app.config.get("SERVER"), current_app.config.get("PUBLICATIONSTATS"))
    response = publication.run_query(elasticQueries[filters.scope]["tabulations"][filters.tabulation]["query"], filters = filters)
    data_treemap, data_table = {}, {}
    data_treemap["by_collection"], data_table["by_collection"] = data_process_treemap_table(response, "by_journal", "by_collection", True)
    data_treemap["by_area"], data_table["by_area"] = data_process_treemap_table(response, "by_journal", "by_area", True)
    data_treemap["by_language"], data_table["by_language"] = data_process_treemap_table(response, "by_journal", "by_language", True)
    data_treemap["by_affiliation"], data_table["by_affiliation"] = data_process_treemap_table(response, "by_journal", "by_affiliation", True)
    data_treemap["by_year"], data_table["by_year"] = data_process_treemap_table(response, "by_journal", "by_year", False)

    #Creamos un json de caché
    j = {
        'Filters'     : filters.serialize,
        'data_treemap': data_treemap,
        'data_table'  : data_table,
        #'data_map'    : data_map,
    }
    with open(current_app.config.get("CACHE_FOLDER_PATH")+VERSION+'/'+filters.md5+'.json', 'w') as json_file:
        json.dump(j, json_file, ensure_ascii=True, sort_keys=True, indent=4)
  data_treemap, data_table = data_from_cache(filters, True, True, False)
  return render_template("v1/document/tabulation_t7.html", filters = filters, data_treemap = data_treemap, data_table = data_table).encode( "utf-8" )

#-- ----------------------------------------------------------------
#-- ----------------------------------------------------------------
#-- ----------------------------------------------------------------









def data_process_treemap_table(RESPONSE, primary_key, second_key, by_year = False):
  TREEMAP = []
  TABLE = {}
  response = RESPONSE["aggregations"]["by_all"][primary_key]["buckets"]
  for r in response:
    group = r["key"].encode("utf8").replace("`","'")
    group, g_country_code, g_country_name, g_country_demonym = group_transform(group)
    t_group = g_country_demonym if primary_key == "by_affiliation" else group
    aux = r[second_key]["buckets"]
    for item in aux:
      key = item['key'].encode("utf8").replace("`","'")
      key, k_country_code, k_country_name, k_country_demonym = key_transform(key)
      t_key = k_country_demonym if second_key == "by_affiliation" else key

      if not TABLE.has_key(t_group):
        TABLE[t_group] = {}
      if not TABLE[t_group].has_key(t_key):
        TABLE[t_group][t_key] = 0
      
      if by_year:
        aux2 = item["by_year"]["buckets"]
        for item2 in aux2:
          year = item2['key'].encode("utf8").replace("`","'")
          TREEMAP.append({  "group"          : group,
                            "sub-group"      : key,
                            "group-country-code" : g_country_code,
                            "group-country-name" : g_country_name,
                            "group-country-demonym" : g_country_demonym,
                            "sub-group-country-code" : k_country_code,
                            "sub-group-country-name" : k_country_name,
                            "sub-group-country-demonym" : k_country_demonym,
                            "year"           : year,
                            "documents"      : item2["doc_count"]
                          })
          TABLE[t_group][t_key] += item2["doc_count"]
      else:
        TREEMAP.append({"group"        : group,
                        "sub-group"    : key,
                        "group-country-code" : g_country_code,
                        "group-country-name" : g_country_name,
                        "group-country-demonym" : g_country_demonym,
                        "sub-group-country-code" : k_country_code,
                        "sub-group-country-name" : k_country_name,
                        "sub-group-country-demonym" : k_country_demonym,
                        "documents"    : item["doc_count"]
                      })
        TABLE[t_group][t_key] += item["doc_count"]
  return TREEMAP, TABLE


def group_transform(group):
  g_country_code, g_country_name, g_country_demonym = "undefined", "undefined", "undefined"
  if group in country_collections and group not in not_collections:
    c = group
    g_country_code = ISO_3166_ALPHA3[country_collections[c]]["code"]
    g_country_name = ISO_3166_ALPHA3[country_collections[c]]["fullname"]
    group =  ISO_3166_ALPHA3[country_collections[c]]["fullname"]
  elif ISO_3166_ALPHA2.has_key(group):
    c = group
    g_country_code    = ISO_3166_ALPHA2[c]["code"]
    g_country_name    = ISO_3166_ALPHA2[c]["fullname"]
    g_country_demonym = ISO_3166_ALPHA2[c]["demonym"]
    group             = ISO_3166_ALPHA2[c]["fullname"]
  elif ISO_639_ALPHA2.has_key(group):
    group =  ISO_639_ALPHA2[group]["english"]

  return group, g_country_code, g_country_name, g_country_demonym

def key_transform(key):
  k_country_code, k_country_name, k_country_demonym = "undefined", "undefined", "undefined"
  if key in country_collections and key not in not_collections:
    c = key
    k_country_code = ISO_3166_ALPHA3[country_collections[c]]["code"]
    k_country_name = ISO_3166_ALPHA3[country_collections[c]]["fullname"]
    key =  ISO_3166_ALPHA3[country_collections[c]]["fullname"]
  elif ISO_3166_ALPHA2.has_key(key):
    c = key
    k_country_code    = ISO_3166_ALPHA2[c]["code"]
    k_country_name    = ISO_3166_ALPHA2[c]["fullname"]
    k_country_demonym = ISO_3166_ALPHA2[c]["demonym"]
    key =  ISO_3166_ALPHA2[c]["fullname"]
  elif ISO_639_ALPHA2.has_key(key):
    key =  ISO_639_ALPHA2[key]["english"]

  return key, k_country_code, k_country_name, k_country_demonym


def data_from_cache(filters = Filters, data_treemap = False, data_table = False, data_map = False):
  #Obtenemos el json en cache
  with open(current_app.config.get("CACHE_FOLDER_PATH")+VERSION+'/'+filters.md5+'.json') as data_file:    
      json_data = json.load(data_file)
  if data_treemap and data_table and data_map:
    return json_data['data_treemap'], json_data['data_table'], json_data['data_map']
  elif data_treemap and data_table:
    return json_data['data_treemap'], json_data['data_table']
  elif data_treemap and data_map:
    return json_data['data_treemap'], json_data['data_map']
  elif data_table and data_map:
    return json_data['data_table'], json_data['data_map']
  elif data_treemap:
    return json_data['data_treemap']
  elif data_table:
    return json_data['data_table']
  elif data_map:
    return json_data['data_map']