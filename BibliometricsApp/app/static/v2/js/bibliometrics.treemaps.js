function d3plus_treemap(container_id, data, id_tag, size_tag)
{
  visualization = d3plus.viz()
    .container(container_id)
    .data(data)
    .type("tree_map")
    .id(id_tag)
    .size(size_tag)
    .labels({"align": "left", "valign": "top"})
  return visualization
}

function simple_treemap(container_id, data, id_tag, size_tag)
{
  var visualization = d3plus.viz()
    .container(container_id)
    .data(data)
    .type("tree_map")
    .id(id_tag)
    .size(size_tag)
    .labels({"align": "left", "valign": "top"})
    .draw()
}

function simple_treemap_with_time(container_id, data, id_tag, size_tag, time_tag)
{
  var visualization = d3plus.viz()
    .container(container_id)
    .data(data)
    .type("tree_map")
    .id(id_tag)
    .size(size_tag)
    .labels({"align": "left", "valign": "top"})
    .time({"value": time_tag})
    .draw()
  
}