function simple_rings(container_id, data, focus)
{
	var visualization = d3plus.viz()
		.container(container_id) //contenedor de la vista
		.type("rings")     
		.edges({
		 	"size": "strength",
		 	"value": data
		})
	if(focus != false){
		visualization.focus({
				      "tooltip" : false,
				      "value": focus
				    })
	}
	visualization.draw()
}