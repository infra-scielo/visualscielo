function printSVG(container_id)
{
    var popUpAndPrint = function()
    {
        var width = parseFloat($(container_id).width());
        var height = parseFloat($(container_id).height());
        var printWindow = window.open('', 'PrintMap',
        'width=' + width + ',height=' + height);
        printWindow.document.writeln($(container_id).html());
        printWindow.document.close();
        printWindow.print();
        printWindow.close();
    };
    setTimeout(popUpAndPrint, 500);
};