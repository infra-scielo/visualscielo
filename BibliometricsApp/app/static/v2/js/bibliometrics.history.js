// Check browser support
if (typeof(Storage) !== "undefined") {
	$("#search-history-btn").show();
} else {
	$("#search-history-btn").hide();
    console.log("Sorry, your browser does not support Web Storage...");
}

//-- -----------------------------------------------------------------

function save_to_history(form_id)
{
    //block();
	//Aumentamos número de historial.
	localStorage["history"] ? localStorage["history"]++ : localStorage["history"] = 1;
	//Registramos formulario
	form = $(form_id).serializeObject();
	localStorage["h"+localStorage["history"]] = JSON.stringify(form);
}

//-- -----------------------------------------------------------------

$.fn.serializeObject = function()
{
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};