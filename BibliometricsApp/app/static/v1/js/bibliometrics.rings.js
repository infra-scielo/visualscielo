function simple_rings(container_id, data){
	var visualization = d3plus.viz()
		.container(container_id) //contenedor de la vista
		.type("rings")     
		.edges({
		 	"strength": "strength",
		 	"label": "strength",
		 	"value": data
		})
		.shape("circle")
		.draw()
}