function simple_treemap(container_id, data, id_tag, size_tag){
  var visualization = d3plus.viz()
    .container(container_id)
    .data(data)
    .type("tree_map")
    .id(id_tag)
    .size(size_tag)
    .labels({"align": "left", "valign": "top"})
    .draw()
}

function simple_treemap_with_time(container_id, data, id_tag, size_tag, time_tag){
  var visualization = d3plus.viz()
    .container(container_id)
    .data(data)
    .type("tree_map")
    .id(id_tag)
    .size(size_tag)
    .labels({"align": "left", "valign": "top"})
    .time({"value": time_tag})
    .draw()
}



function tabulation_3_treemap(data){
  var visualization = d3plus.viz()
    .container("#treemap")
    .data(data)
    .type("tree_map")
    .id("name")
    .size("documents")
    .labels({"align": "left", "valign": "top"})
    .time({"value": "year"})
    .draw()
}

function tabulation_30_treemap(data, id_tag, size_tag){
    var visualization = d3plus.viz()
        .container("#treemap")
        .data(data)
        .type("tree_map")
        .id(id_tag)
        .size(size_tag)
        .labels({"align": "left", "valign": "top"})
        .draw()
        
    $('#treemap').on({ click: function(o) {
        if(this.__data__.name[0].length == 1 && this.__data__.year[0].length != 1){
          $("#treemap-linechart").empty()
          treemap_linechart(this.__data__.year)
        }
        if(this.__data__.name[0].length == 1 && this.__data__.year[0].length == 1){
          $("#treemap-linechart").empty()
        }     
      }
    }, 'g.d3plus_rect rect');
}

function treemap_linechart(data){
   var visualization = d3plus.viz()
    .container("#treemap-linechart")    // container DIV to hold the visualization
    .data(data)       // data to use with the visualization
    .type("line")         // visualization type
    .id("name")           // key for which our data is unique on
    .text("name")         // key to use for display text
    .y("documents")         // key to use for y-axis
    .x("year")            // key to use for x-axis
    .y({"grid": false})
    .x({"grid": false})
    .time({"value": "year"})
    .draw()
}