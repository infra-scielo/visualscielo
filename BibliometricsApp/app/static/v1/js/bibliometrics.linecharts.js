function linechart(data){
	 var visualization = d3plus.viz()
		.container("#linechart")  	// container DIV to hold the visualization
		.data(data)  			// data to use with the visualization
		.type("line")       	// visualization type
		.id("name")         	// key for which our data is unique on
		.text("name")      	 	// key to use for display text
		.y("documents")         // key to use for y-axis
		.x("year")          	// key to use for x-axis
		.y({"grid": false})
		.x({"grid": false})
		.time({"value": "year"})
		.draw()
}