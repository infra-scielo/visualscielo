#!/usr/bin/env python
# -*- coding: utf-8 -*-
import json
from datetime import datetime, timedelta
from app.thrift_clients import clients
from app.v3.classes.filters import Filters


class ServerError(Exception):
  def __init__(self, value):
    self.message = 'Server Error: %s' % str(value)
  
  def __str__(self):
    return repr(self.message)


def publicationstats(host):
  address, port = host.split(':')
  return PublicationStats(address, port)


class PublicationStats(clients.PublicationStats):  
  def generate_filters(self, filters = Filters()):
    #Generamos Filtros
    dict_filters = {"bool":{ "must": []}}
    dict_filters["bool"]["must"].append({"range": {"publication_year": { "gte": filters.from_year, "lte": filters.to_year }}}) #Years Range
    #SciELO Thematic Areas
    if len(filters.thematic_areas) > 0:
      thematic_areas = {"bool": {}}
      operation = "must" if filters.thematic_areas_operation == "AND" else "should"
      thematic_areas["bool"][operation] = [{"terms": { "subject_areas": [ ta[0] for ta in filters.thematic_areas ] }}]
      dict_filters["bool"]["must"].append(thematic_areas)
    #WOS Thematic Areas
    if len(filters.wos_thematic_areas) > 0:
      wos_thematic_areas = {"bool": {}}
      operation = "must" if filters.wos_thematic_areas_operation == "AND" else "should"
      wos_thematic_areas["bool"][operation] = [{"terms": { "wos_subject_areas": [ wta[0] for wta in filters.wos_thematic_areas ] }}]
      dict_filters["bool"]["must"].append(wos_thematic_areas)
    #Collections
    if len(filters.collections) > 0:
      collections = {"bool": {}}
      operation = "must" if filters.collections_operation == "AND" else "should"
      collections["bool"][operation] = [{"terms": { "collection": [ c[0] for c in filters.collections ] }}]
      dict_filters["bool"]["must"].append(collections)
    #Languages
    if len(filters.languages) > 0:
      languages = {"bool": {}}
      operation = "must" if filters.languages_operation == "AND" else "should"
      languages["bool"][operation] = [{"terms": { "languages": [ l[0] for l in filters.languages ] }}]
      dict_filters["bool"]["must"].append(languages)
    #Document Type
    if filters.document_type == 'Citable':
      dict_filters["bool"]["must"].append({"terms": {"document_type": ["article-commentary", "brief-report", "case-report", "rapid-communication", "research-article", "review-article"]}})
    elif filters.document_type == 'non-Citable':
      dict_filters["bool"]["must"].append({"terms": {"document_type": ["editorial", "book-review", "abstract", "letter", "addendum", "news", "correction", "press-release"]}})
    #Document Type Details
    if len(filters.document_type_details) > 0:
      document_type_details = {"bool": {}}
      operation = "must" if filters.document_type_details_operation == "AND" else "should"
      document_type_details["bool"][operation] = [{"terms": { "document_type": [ dtd[0] for dtd in filters.document_type_details ] }}]
      dict_filters["bool"]["must"].append(document_type_details)
    #journals
    if len(filters.journals) > 0:
      journals = {"bool": {}}
      operation = "must" if filters.journals_operation == "AND" else "should"
      journals["bool"][operation] = [{"terms": { "journal_title": filters.journals }}]
      dict_filters["bool"]["must"].append(journals)
    #journal Status
    if filters.journals_status != 'ALL':
      journals = {"bool": {}}
      operation = "should"
      journals["bool"][operation] = [{"terms": { "journal_title": self.journals_title_by_status(filters.journals_status) }}]
      dict_filters["bool"]["must"].append(journals)
    return dict_filters

  #Obtener data para llenar el gráfico inicial
  def data_index(self):
    body = {"aggs" : {"by_collection" : { "terms" : { "field" : "collection", "size":1000000000 },"aggs":{"by_journal":{"terms":{"field":"journal_title","size":1000000000}}}}}}
    query_parameters = [
      clients.accessstats_thrift.kwargs('size', '0'),
    ]
    query_result = json.loads(self.client.search('article', json.dumps(body), query_parameters)) 
    return query_result    

  #Obtener revistas por estatus
  def journals_title_by_status(self, status):
    body = {
      "from": 0,
      "size": 10000,
      "_source": ["title", "status"],
      "query": {
        "bool": {
          "must": {
            "term": {
              "status": status
            }
          }
        }
      }
    }
    query_parameters = []    
    query_result = json.loads(self.client.search('journal', json.dumps(body), query_parameters))
    journals = [j['_source']['title'] for j in query_result['hits']['hits']]
    return journals

  #Ejecutar query a elastic
  def run_query(self, body, filters = Filters()):
    body["aggs"]["filtered"]["filter"] = self.generate_filters(filters)
    query_parameters = [clients.accessstats_thrift.kwargs('size', '0')]
    query_result = json.loads(self.client.search('article', json.dumps(body), query_parameters))
    return query_result
  
  #Ejecutar query a elastic
  def run_query2(self, body, filters = Filters()):
    body["filter"] = self.generate_filters(filters)
    query_parameters = [clients.accessstats_thrift.kwargs('size', '10000')]    
    query_result = json.loads(self.client.search('article', json.dumps(body), query_parameters))
    return query_result
