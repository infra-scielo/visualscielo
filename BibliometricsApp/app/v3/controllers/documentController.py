#!/usr/bin/env python
# -*- coding: utf-8 -*-
from flask import Blueprint, render_template,\
                  session, redirect, url_for,\
                  request, jsonify, make_response,\
                  flash, current_app
from datetime import datetime
from math import log
from app.v3.helpers.dicts import collections, country_collections,\
                              country_code_inv, country_code_rev,\
                              not_collections
from app.v3.helpers.elasticQueries import elasticQueries
from app.v3.helpers.ISO_3166_ALPHA2 import ISO_3166_ALPHA2
from app.v3.helpers.ISO_3166_ALPHA3 import ISO_3166_ALPHA3
from app.v3.helpers.ISO_639_ALPHA2 import ISO_639_ALPHA2
from app.v3.helpers.SciELO_THEMATIC_AREA import SciELO_THEMATIC_AREA
from app.v3.helpers.formHelper import *
from app.v3.classes.filters import Filters
from app.v3.classes.publicationStats import PublicationStats

import os.path
import urllib
import json


VERSION = "v3"
mod_document = Blueprint('v3_documentController', __name__, url_prefix='/'+VERSION+'/documents')

#-- ----------------------------------------------------------------

@mod_document.route('/', methods = ['GET', 'POST'])
def d_index(filters = Filters, shared = False):
  if request.method == 'POST':
    #Leemos Formulario y obtenemos filtros
    form =  request.form
    if not shared:
      filters = process_filters_form(form)  
    
    if(filters.tabulation == "1"):
      return tabulation_1(filters)
    elif(filters.tabulation == "2"):
      return tabulation_2(filters)
    elif(filters.tabulation == "3"):
      return tabulation_3(filters)
    elif(filters.tabulation == "4"):
      return tabulation_4(filters)
    elif(filters.tabulation == "5"):
      return tabulation_5(filters)
    else:
      flash('Impossible to answer the request at this time', 'error')
      return redirect(url_for("v3_indexController.index"))
  else:
    flash('Impossible to answer the request at this time', 'error')
    return redirect(url_for("v3_indexController.index"))

#-- ----------------------------------------------------------------

@mod_document.route('/shared/<cache_filename>')
def shared(cache_filename):
  cache_filename = current_app.config.get("CACHE_FOLDER_PATH")+VERSION+'/'+cache_filename+'.json'
  if not os.path.exists(cache_filename):
    return "Visualization not found"
  else:
    with open(cache_filename, 'r') as json_file:
      json_data = json.load(json_file)
      filters = process_filters_form_dict(json_data['Filters'])
      return d_index(filters, True)

#-- ----------------------------------------------------------------
#-- ----------------------------------------------------------------
#-- ----------------------------------------------------------------

def tabulation_1(filters = Filters):
  url = urllib.quote(current_app.config.get("HOSTNAME")+url_for('v3_documentController.shared', cache_filename=filters.md5))
  cache_filename = current_app.config.get("CACHE_FOLDER_PATH")+VERSION+'/'+filters.md5+'.json'
  if not os.path.exists(cache_filename):
    publication = PublicationStats(current_app.config.get("SERVER"), current_app.config.get("PUBLICATIONSTATS"))
    response = publication.run_query(elasticQueries[filters.scope]["tabulations"][filters.tabulation]["query"], filters = filters)
    
    data_treemap, data_table, total_docs = {}, {}, 0
    data_treemap["by_year"], data_table["by_year"], total_docs = data_process_treemap_table(response, "by_collection", "by_year", False, filters, "t1")
    data_map = data_treemap["by_year"]

    #Creamos un json de caché
    j = {
        'Filters'     : filters.serialize,
        'data_treemap': data_treemap,
        'data_table'  : data_table,
        'data_map'    : data_map,
        'total_docs'  : total_docs,
    }
    with open(cache_filename, 'w') as json_file:
        json.dump(j, json_file, ensure_ascii=True, sort_keys=True, indent=4)
  data_treemap, data_table, data_map, total_docs = data_from_cache(filters, True, True, True)
  return render_template("v3/document/tabulation_1.html", filters = filters, url = url, data_treemap = data_treemap, data_table = data_table, data_map = data_map, total_docs = total_docs).encode( "utf-8" )

#-- ----------------------------------------------------------------

def tabulation_2(filters = Filters):
  url = urllib.quote(current_app.config.get("HOSTNAME")+url_for('v3_documentController.shared', cache_filename=filters.md5))
  cache_filename = current_app.config.get("CACHE_FOLDER_PATH")+VERSION+'/'+filters.md5+'.json'
  if not os.path.exists(cache_filename):
    publication = PublicationStats(current_app.config.get("SERVER"), current_app.config.get("PUBLICATIONSTATS"))
    response = publication.run_query(elasticQueries[filters.scope]["tabulations"][filters.tabulation]["query"], filters = filters)
    
    data_treemap, data_table, total_docs = {}, {}, 0
    data_treemap["by_collection"], data_table["by_collection"], aux = data_process_treemap_table(response, "by_area", "by_collection", True, filters, "t2")
    data_treemap["by_year"], data_table["by_year"], total_docs = data_process_treemap_table(response, "by_area", "by_year", False, filters, "t2")

    data_map = {"keys": ["documents"], "map": []}
    for d in data_treemap["by_collection"]:
      if not d["group"] in data_map["keys"]:
        data_map["keys"].append(d["group"])
      d[d["group"]] = d["documents"]
    data_map["map"] = data_treemap["by_collection"]

    #Creamos un json de caché
    j = {
        'Filters'     : filters.serialize,
        'data_treemap': data_treemap,
        'data_table'  : data_table,
        'data_map'    : data_map,
        'total_docs'  : total_docs
    }
    with open(cache_filename, 'w') as json_file:
        json.dump(j, json_file, ensure_ascii=True, sort_keys=True, indent=4)
  data_treemap, data_table, data_map, total_docs = data_from_cache(filters, True, True, True)
  return render_template("v3/document/tabulation_2.html", filters = filters, url = url, data_treemap = data_treemap, data_table = data_table, data_map = data_map, total_docs = total_docs).encode( "utf-8" )

#-- ----------------------------------------------------------------

def tabulation_3(filters = Filters):
  url = urllib.quote(current_app.config.get("HOSTNAME")+url_for('v3_documentController.shared', cache_filename=filters.md5))
  cache_filename = current_app.config.get("CACHE_FOLDER_PATH")+VERSION+'/'+filters.md5+'.json'
  if not os.path.exists(cache_filename):
    publication = PublicationStats(current_app.config.get("SERVER"), current_app.config.get("PUBLICATIONSTATS"))
    response = publication.run_query(elasticQueries[filters.scope]["tabulations"][filters.tabulation]["query"], filters = filters)
    
    data_treemap, data_table, total_docs = {}, {}, 0
    data_treemap["by_collection"], data_table["by_collection"], aux = data_process_treemap_table(response, "by_language", "by_collection", True, filters, "t3")
    data_treemap["by_year"], data_table["by_year"], total_docs = data_process_treemap_table(response, "by_language", "by_year", False, filters, "t3")

    data_map = {"keys": ["documents"], "map": []}
    for d in data_treemap["by_collection"]:
      if not d["group"] in data_map["keys"]:
        data_map["keys"].append(d["group"])
      d[d["group"]] = d["documents"]
    data_map["map"] = data_treemap["by_collection"]

    #Creamos un json de caché
    j = {
        'Filters'     : filters.serialize,
        'data_treemap': data_treemap,
        'data_table'  : data_table,
        'data_map'    : data_map,
        'total_docs'  : total_docs
    }
    with open(cache_filename, 'w') as json_file:
        json.dump(j, json_file, ensure_ascii=True, sort_keys=True, indent=4)
  data_treemap, data_table, data_map, total_docs = data_from_cache(filters, True, True, True)
  return render_template("v3/document/tabulation_3.html", filters = filters, url = url, data_treemap = data_treemap, data_table= data_table, data_map = data_map, total_docs = total_docs, ISO_639_ALPHA2 = ISO_639_ALPHA2).encode( "utf-8" )

#-- ----------------------------------------------------------------

def tabulation_4(filters = Filters):
  url = urllib.quote(current_app.config.get("HOSTNAME")+url_for('v3_documentController.shared', cache_filename=filters.md5))
  cache_filename = current_app.config.get("CACHE_FOLDER_PATH")+VERSION+'/'+filters.md5+'.json'
  if not os.path.exists(cache_filename):
    publication = PublicationStats(current_app.config.get("SERVER"), current_app.config.get("PUBLICATIONSTATS"))
    response = publication.run_query(elasticQueries[filters.scope]["tabulations"][filters.tabulation]["query"], filters = filters)
    
    data_treemap, data_table, total_docs = {}, {}, 0
    data_treemap["by_collection"], data_table["by_collection"], aux = data_process_treemap_table(response, "by_affiliation", "by_collection", True, filters, "t4")
    data_treemap["by_year"], data_table["by_year"], total_docs = data_process_treemap_table(response, "by_affiliation", "by_year", False, filters, "t4")

    data_map = {"keys": ["documents"], "by_collection": [], "by_year": []}
    for d in data_treemap["by_year"]:
      d["year"] = d["sub-group"]
      data_map["by_year"].append(d)

    for d in data_treemap["by_collection"]:
      if not d["group"] in data_map["keys"]:
        data_map["keys"].append(d["group"])
      d[d["group"]] = d["documents"]
    data_map["by_collection"] = data_treemap["by_collection"]

    #Creamos un json de caché
    j = {
        'Filters'     : filters.serialize,
        'data_treemap': data_treemap,
        'data_table'  : data_table,
        'data_map'    : data_map,
        'total_docs'  : total_docs
    }
    with open(cache_filename, 'w') as json_file:
        json.dump(j, json_file, ensure_ascii=True, sort_keys=True, indent=4)
  data_treemap, data_table, data_map, total_docs = data_from_cache(filters, True, True, True)
  return render_template("v3/document/tabulation_4.html", filters = filters, url = url, data_treemap = data_treemap, data_table = data_table, data_map = data_map, total_docs = total_docs).encode( "utf-8" )

#-- ----------------------------------------------------------------

def tabulation_5(filters = Filters):
  url = urllib.quote(current_app.config.get("HOSTNAME")+url_for('v3_documentController.shared', cache_filename=filters.md5))
  cache_filename = current_app.config.get("CACHE_FOLDER_PATH")+VERSION+'/'+filters.md5+'.json'
  if not os.path.exists(cache_filename):
    publication = PublicationStats(current_app.config.get("SERVER"), current_app.config.get("PUBLICATIONSTATS"))
    response = publication.run_query(elasticQueries[filters.scope]["tabulations"][filters.tabulation]["query"], filters = filters)
    
    data_treemap, data_table, total_docs = {}, {}, 0
    data_treemap["by_collection"], data_table["by_collection"], aux = data_process_treemap_table(response, "by_journal", "by_collection", True, filters, "t5")
    data_treemap["by_year"], data_table["by_year"], total_docs = data_process_treemap_table(response, "by_journal", "by_year", False, filters, "t5")

    data_map = {"keys": ["documents"], "map": []}
    for d in data_treemap["by_collection"]:
      if not d["group"] in data_map["keys"]:
        data_map["keys"].append(d["group"])
      d[d["group"]] = d["documents"]
    data_map["map"] = data_treemap["by_collection"]

    #Creamos un json de caché
    j = {
        'Filters'     : filters.serialize,
        'data_treemap': data_treemap,
        'data_table'  : data_table,
        'data_map'    : data_map,
        'total_docs'  : total_docs
    }
    with open(cache_filename, 'w') as json_file:
        json.dump(j, json_file, ensure_ascii=True, sort_keys=True, indent=4)
  data_treemap, data_table, data_map, total_docs = data_from_cache(filters, True, True, True)
  return render_template("v3/document/tabulation_5.html", filters = filters, url = url, data_treemap = data_treemap, data_table = data_table, data_map = data_map, total_docs = total_docs).encode( "utf-8" )

#-- ----------------------------------------------------------------
#-- ----------------------------------------------------------------
#-- ----------------------------------------------------------------

def data_process_treemap_table(RESPONSE, primary_key, second_key, by_year = False, filters = Filters, tabulation = None):
  TREEMAP = []
  TABLE = {}
  TOTAL_DOCS = 0

  f_languages = [l[0] for l in filters.languages]
  f_thematic_areas = [ta[0] for ta in filters.thematic_areas]

  response = RESPONSE["aggregations"]["filtered"][primary_key]["buckets"]
  for r in response:
    valid = True
    group = r["key"].encode("utf8").replace("`","'")
    TOTAL_DOCS += r["doc_count"]
    #Solo filtrados
    if tabulation == "t2":
      if filters.thematic_areas != []:
        if group not in f_thematic_areas:
          valid = False
    if tabulation == "t3":
      if f_languages != []:
        if group not in f_languages:
          valid = False

    if valid:
      group, g_country_code, g_country_name, g_country_demonym = group_transform(group, filters)
      t_group = g_country_demonym if primary_key == "by_affiliation" else group
      aux = r[second_key]["buckets"]
      for item in aux:
        key = item['key'].encode("utf8").replace("`","'")
        key, k_country_code, k_country_name, k_country_demonym = key_transform(key, filters)
        t_key = k_country_demonym if second_key == "by_affiliation" else key

        if not TABLE.has_key(t_group):
          TABLE[t_group] = {}
        if not TABLE[t_group].has_key(t_key):
          TABLE[t_group][t_key] = {"documents": 0}
        
        if by_year:
          aux2 = item["by_year"]["buckets"]
          for item2 in aux2:
            year = item2['key'].encode("utf8").replace("`","'")
            TREEMAP.append({  "group" : group,
                              "sub-group" : key,
                              "group-country-code" : g_country_code,
                              "group-country-name" : g_country_name,
                              "group-country-demonym" : g_country_demonym,
                              "sub-group-country-code" : k_country_code,
                              "sub-group-country-name" : k_country_name,
                              "sub-group-country-demonym" : k_country_demonym,
                              "year" : year,
                              "documents" : item2["doc_count"]
                            })
            TABLE[t_group][t_key]['documents'] += item2["doc_count"]
        else:
          by_thematic_area = {"Health Sciences": 0, 
                              "Human Sciences": 0, 
                              "Biological Sciences": 0, 
                              "Agricultural Sciences": 0, 
                              "Applied Social Sciences": 0, 
                              "Exact and Earth Sciences": 0, 
                              "Engineering": 0, 
                              "Linguistics, Letters and Arts": 0, 
                              "undefined": 0,
                              "total": 0}
          for ta in item['by_subject_area']['buckets']:
            if by_thematic_area.has_key(ta['key']):
              by_thematic_area[ta['key']] += ta['doc_count']
            else:
              by_thematic_area['undefined'] += ta['doc_count']
            by_thematic_area['total'] += ta['doc_count']

          by_language = {'en': 0, 'es': 0, 'pt': 0, 'other': 0, 'total': 0}
          for l in item['by_language']['buckets']:
            if by_language.has_key(l['key']):
              by_language[l['key']] += l['doc_count']
            else:
              by_language['other'] += l['doc_count']
            by_language['total'] += l['doc_count']

          by_citable_doc = {"research-article": 0, 
                            "editorial": 0, 
                            "book-review": 0, 
                            "case-report": 0, 
                            "review-article": 0, 
                            "rapid-communication": 0, 
                            "letter": 0, 
                            "brief-report": 0, 
                            "article-commentary": 0, 
                            "abstract": 0,
                            "total": 0}
          for cd in item['by_citable_docs']['by_citable_docs2']['buckets']:
            if by_citable_doc.has_key(cd['key']):
              by_citable_doc[cd['key']] += cd['doc_count']
            else:
              by_citable_doc['other'] += cd['doc_count']
            by_citable_doc['total'] += cd['doc_count']
          TREEMAP.append({"group"        : group,
                          "sub-group"    : key,
                          "group-country-code" : g_country_code,
                          "group-country-name" : g_country_name,
                          "group-country-demonym" : g_country_demonym,
                          "sub-group-country-code" : k_country_code,
                          "sub-group-country-name" : k_country_name,
                          "sub-group-country-demonym" : k_country_demonym,
                          "documents"    : item["doc_count"]
                        })
          TABLE[t_group][t_key]['documents'] += item["doc_count"]
          TABLE[t_group][t_key]['by_thematic_area'] = by_thematic_area
          TABLE[t_group][t_key]['by_language'] = by_language
          TABLE[t_group][t_key]['by_citable_doc'] = by_citable_doc
  return TREEMAP, TABLE, TOTAL_DOCS


def group_transform(group, filters = Filters):
  g_country_code, g_country_name, g_country_demonym = "undefined", "undefined", "undefined"
  #Collection
  if group in country_collections and group not in not_collections:
    c = group
    g_country_code = ISO_3166_ALPHA3[country_collections[c]]["code"]
    if ISO_3166_ALPHA3[country_collections[c]].has_key(filters.lang):
      g_country_name = ISO_3166_ALPHA3[country_collections[c]][filters.lang]
      group          = ISO_3166_ALPHA3[country_collections[c]][filters.lang]
    else:
      g_country_name = ISO_3166_ALPHA3[country_collections[c]]["fullname"]
      group          = ISO_3166_ALPHA3[country_collections[c]]["fullname"]
  elif ISO_3166_ALPHA2.has_key(group):
    c = group
    g_country_code    = ISO_3166_ALPHA2[c]["code"]
    g_country_demonym = ISO_3166_ALPHA2[c]["demonym"]
    if ISO_3166_ALPHA2[c].has_key(filters.lang):
      g_country_name    = ISO_3166_ALPHA2[c][filters.lang]
      group           = ISO_3166_ALPHA2[c][filters.lang]
    else:
      g_country_name    = ISO_3166_ALPHA2[c]["fullname"]
      group           = ISO_3166_ALPHA2[c]["fullname"]
  #Language
  elif ISO_639_ALPHA2.has_key(group):
    if ISO_639_ALPHA2[group].has_key(filters.lang):
      group =  ISO_639_ALPHA2[group][filters.lang]
    else:
      group =  ISO_639_ALPHA2[group]["english"]
  #Thematic Area
  elif SciELO_THEMATIC_AREA.has_key(group):
    if SciELO_THEMATIC_AREA[group].has_key(filters.lang):
      group =  SciELO_THEMATIC_AREA[group][filters.lang]
  return group, g_country_code, g_country_name, g_country_demonym


def key_transform(key, filters = Filters):
  key, k_country_code, k_country_name, k_country_demonym = group_transform(key, filters)
  return key, k_country_code, k_country_name, k_country_demonym


def data_from_cache(filters = Filters, data_treemap = False, data_table = False, data_map = False):
  #Obtenemos el json en cache
  with open(current_app.config.get("CACHE_FOLDER_PATH")+VERSION+'/'+filters.md5+'.json') as data_file:    
      json_data = json.load(data_file)
  if data_treemap and data_table and data_map:
    return json_data['data_treemap'], json_data['data_table'], json_data['data_map'], json_data['total_docs']
  elif data_treemap and data_table:
    return json_data['data_treemap'], json_data['data_table'], json_data['total_docs']
  elif data_treemap and data_map:
    return json_data['data_treemap'], json_data['data_map'], json_data['total_docs']
  elif data_table and data_map:
    return json_data['data_table'], json_data['data_map'], json_data['total_docs']
  elif data_treemap:
    return json_data['data_treemap'], json_data['total_docs']
  elif data_table:
    return json_data['data_table'], json_data['total_docs']
  elif data_map:
    return json_data['data_map'], json_data['total_docs']