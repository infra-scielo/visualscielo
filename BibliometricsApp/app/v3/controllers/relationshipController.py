#!/usr/bin/env python
# -*- coding: utf-8 -*-
from flask import Blueprint, render_template,\
                  session, redirect, url_for,\
                  request, jsonify, make_response,\
                  flash, current_app
from datetime import datetime
from math import log
from app.v3.helpers.dicts import collections, country_collections,\
                              country_code_inv, country_code_rev,\
                              not_collections
from app.v3.helpers.elasticQueries import elasticQueries
from app.v3.helpers.ISO_3166_ALPHA2 import ISO_3166_ALPHA2
from app.v3.helpers.ISO_3166_ALPHA2_DEMONYM import ISO_3166_ALPHA2_DEMONYM
from app.v3.helpers.ISO_3166_ALPHA3 import ISO_3166_ALPHA3
from app.v3.helpers.ISO_639_ALPHA2 import ISO_639_ALPHA2
from app.v3.helpers.formHelper import *
from app.v3.classes.filters import Filters
from app.v3.classes.publicationStats import PublicationStats

import networkx as nx
import numpy as np
import os.path
import random
import json

VERSION = "v3"
mod_relationship = Blueprint('v3_relationshipController', __name__, url_prefix='/'+VERSION+'/relationships')

#-- ----------------------------------------------------------------

@mod_relationship.route('/', methods = ['GET', 'POST'])
def r_index(filters = Filters, shared = False):
  if request.method == 'POST':
    #Leemos Formulario y obtenemos filtros
    form =  request.form

    if not shared:
      filters = process_filters_form(form)
    
    if(filters.tabulation == "1"):
      return tabulation_1(filters)
    elif(filters.tabulation == "2"):
      return tabulation_2(filters)
    elif(filters.tabulation == "3"):
      return tabulation_3(filters)
    else:
      flash('Impossible to answer the request at this time', 'error')
      return redirect(url_for("v3_indexController.index"))
  else:
    flash('Impossible to answer the request at this time', 'error')
    return redirect(url_for("v3_indexController.index"))

#-- ----------------------------------------------------------------

@mod_relationship.route('/shared/<cache_filename>')
def shared(cache_filename):
  cache_filename = current_app.config.get("CACHE_FOLDER_PATH")+VERSION+'/'+cache_filename+'.json'
  if not os.path.exists(cache_filename):
    return "Visualization not found"
  else:
    with open(cache_filename, 'r') as json_file:
      json_data = json.load(json_file)
      filters = process_filters_form_dict(json_data['Filters'])
      r_index(filters, True)

#-- ----------------------------------------------------------------

def tabulation_1(filters = Filters):
  if not os.path.exists(current_app.config.get("CACHE_FOLDER_PATH")+VERSION+'/'+filters.md5+'.json'):
    limitm = filters.frequency
    limits = 99999999999999999

    publication = PublicationStats(current_app.config.get("SERVER"), current_app.config.get("PUBLICATIONSTATS"))
    response = publication.run_query2(elasticQueries[filters.scope]["tabulations"][filters.tabulation]["query"], filters = filters)
    response_aux = response
    

    #MAPA
    response = response["hits"]["hits"]
    net = {}
    COUNTRY_REL = {}
    for i in response:
        aux = i["fields"]["aff_countries"]
        year= i["fields"]["publication_year"]
        for j in aux:
            k = aux.index(j)+1
            while True:
                if (k > len(aux)):
                    break
                if str(aux[k-1]) != "undefined" and str(j) != "undefined":
                    if     str(aux[k-1]) in country_code_inv and str(j) in country_code_inv and str(aux[k-1]) not in not_collections and str(j) not in not_collections: #Poner restrincion not_collections en la query                 
                        if ( country_code_inv[str(aux[k-1])] not in net):                    
                            net[country_code_inv[str(aux[k-1])] ] = {}
                            COUNTRY_REL[country_code_inv[str(aux[k-1])]] = {}                                    
                        if ( country_code_inv[str(j)] not in net):
                            net[ country_code_inv[str(j)] ] = {}
                            COUNTRY_REL[country_code_inv[str(j)]] = {}
                        if ( country_code_inv[str(aux[k-1])] not in net[ country_code_inv[str(j)] ]):
                            net[ country_code_inv[str(j)] ][ country_code_inv[str(aux[k-1])] ] = 0
                        if ( country_code_inv[(str(j))] not in net[ country_code_inv[str(aux[k-1])] ] ):
                            net[ country_code_inv[str(aux[k-1])] ][ country_code_inv[j] ] = 0                            
                        if (str(year[0]) not in COUNTRY_REL[country_code_inv[str(aux[k-1])]]):
                            COUNTRY_REL[country_code_inv[str(aux[k-1])]][str(year[0])] = 0    
                        if (str(year[0]) not in COUNTRY_REL[country_code_inv[str(j)]]):
                            COUNTRY_REL[ country_code_inv[(str(j))]  ][str(year[0])] = 0
                        COUNTRY_REL[ country_code_inv[(str(j))]  ][str(year[0])] += 1
                        COUNTRY_REL[country_code_inv[str(aux[k-1])]][str(year[0])] += 1
                        net[ country_code_inv[str(aux[k-1])] ][ country_code_inv[str(j)] ]+=1
                        net[ country_code_inv[str(j)] ][ country_code_inv[str(aux[k-1])] ]+=1
                k+=1
    collection_graph = {}
    for i in COUNTRY_REL:
        aux = []
        for y in COUNTRY_REL[i]:
            aux.append([y,COUNTRY_REL[i][y]])
        collection_graph[i] = [{"key":"Relations","values":aux}]                    
    
    minval = 100000000
    maxval = 0
    source_edges = {}
    for source in net:
        source_edges[source] = 0
        for target in net[source]:
            source_edges[source] += net[source][target]
    G = {"nodes":[],"edges":[]}    
    for source in source_edges:
        sum = 0
        for target in net[str(source)]:
            sum += net[str(source)][target]
        if sum >= limitm and sum <= limits:
            G["nodes"].append({"id": str(source),
                               "label": ISO_3166_ALPHA3[str(source)][filters.lang] if ISO_3166_ALPHA3[str(source)].has_key(filters.lang) else ISO_3166_ALPHA3[str(source)]["fullname"],
                               "x":random.random(),
                               "y":random.random(),
                               "size": sum })                    
    eid = 0
    auxedge = []
    for source in net:
        for target in net[source]:
            if net[source][target] >= limitm and net[source][target] <= limits :
                arraydata1 = source+"<->"+target
                arraydata2 = target+"<->"+source                    
                if (arraydata1 not in auxedge) and (arraydata2 not in auxedge):                        
                    auxedge.append(arraydata1)
                    G["edges"].append({"id":"e"+str(eid),"source":source,"target":target,"size":net[source][target]})
                    eid+=1
                    
    
    net_aux = {}
    for i in net:
        for j in net[i]:
            if net[i][j] >= limitm and net[i][j] <= limits:
                if i not in net_aux:
                    net_aux[i] = {}
                net_aux[i][j] = net[i][j]
    net = net_aux
    
    source_edges_aux = {}
    for i in source_edges:
        if source_edges[i] >= limitm and source_edges[i] <= limits:
            source_edges_aux[i] = source_edges[i]
    source_edges = source_edges_aux
    
    
    data_network = aff_network(response_aux,limitm,limits,net, filters.lang)
    #Creamos un json de caché
    j = {
        'Filters'     : filters.serialize,
        'data_network': data_network,
        'data_map'    : [net,source_edges,G,collection_graph]
    }
    with open(current_app.config.get("CACHE_FOLDER_PATH")+VERSION+'/'+filters.md5+'.json', 'w') as json_file:
        json.dump(j, json_file, ensure_ascii=True, sort_keys=True, indent=4)  
  #cache
  with open(current_app.config.get("CACHE_FOLDER_PATH")+VERSION+'/'+filters.md5+'.json') as data_file:    
      json_data = json.load(data_file)
  data_network = json_data['data_network']
  data_map = json_data['data_map']

  return render_template("v3/relationships/tabulation_1.html", filters = filters, data_network = data_network, data_map = data_map)

#-- ----------------------------------------------------------------

def tabulation_2(filters = Filters):
  if not os.path.exists(current_app.config.get("CACHE_FOLDER_PATH")+VERSION+'/'+filters.md5+'.json'):
    publication = PublicationStats(current_app.config.get("SERVER"), current_app.config.get("PUBLICATIONSTATS"))
    response = publication.run_query2(elasticQueries[filters.scope]["tabulations"][filters.tabulation]["query"], filters = filters)
    
    data_rings = rings_for_type(response, "subject_areas", filters.frequency, filters.lang)
    data_table = data_table_from_ring2(data_rings)


    #Creamos un json de caché
    j = {
        'Filters'     : filters.serialize,
        'data_rings'  : data_rings,
        'data_table'  : data_table
    }
    with open(current_app.config.get("CACHE_FOLDER_PATH")+VERSION+'/'+filters.md5+'.json', 'w') as json_file:
        json.dump(j, json_file, ensure_ascii=True, sort_keys=True, indent=4)
  #cache
  with open(current_app.config.get("CACHE_FOLDER_PATH")+VERSION+'/'+filters.md5+'.json') as data_file:    
      json_data = json.load(data_file)
  data_rings = json_data['data_rings']
  data_table = json_data['data_table']
  return render_template("v3/relationships/tabulation_2.html", filters = filters, data_rings = data_rings, data_table = data_table)

#-- ----------------------------------------------------------------

def tabulation_3(filters = Filters):
  if not os.path.exists(current_app.config.get("CACHE_FOLDER_PATH")+VERSION+'/'+filters.md5+'.json'):
    publication = PublicationStats(current_app.config.get("SERVER"), current_app.config.get("PUBLICATIONSTATS"))
    response = publication.run_query2(elasticQueries[filters.scope]["tabulations"][filters.tabulation]["query"], filters = filters)
    
    data_rings = rings_for_collection(response, 'collection', filters.frequency, filters.lang)
    data_table = data_table_from_ring2(data_rings)

    #Creamos un json de caché
    j = {
        'Filters'     : filters.serialize,
        'data_rings'  : data_rings,
        'data_table'  : data_table
    }
    with open(current_app.config.get("CACHE_FOLDER_PATH")+VERSION+'/'+filters.md5+'.json', 'w') as json_file:
        json.dump(j, json_file, ensure_ascii=True, sort_keys=True, indent=4)
  #cache
  with open(current_app.config.get("CACHE_FOLDER_PATH")+VERSION+'/'+filters.md5+'.json') as data_file:    
      json_data = json.load(data_file)
  data_rings = json_data['data_rings']
  data_table = json_data['data_table']
  return render_template("v3/relationships/tabulation_3.html", filters = filters, data_rings = data_rings, data_table = data_table)

#-- ----------------------------------------------------------------

def rings_for_collection(data, key, limitm,lang):
  limits = 99999999999999999
  E = {}
  EDGES = []
  data = data["hits"]["hits"]
  for d in data:
    aff_countries = d["fields"]["aff_countries"]
    collections = d["fields"][key]
    for i in aff_countries:
      if i != 'undefined':
        if i not in E:
          E[i] = {}
        for j in collections:
          if j in country_collections and j not in not_collections:
            j = ISO_3166_ALPHA3[country_collections[j]]["fullname"]
            if j not in E[i]:
              E[i][j] = 0
            E[i][j] += 1
    EDGES = []
    for source in E:
      country_name, country_alpha3, country_demonym = "---", "---", "---"
      if ISO_3166_ALPHA2.has_key(source.upper()):
        country_alpha3 = ISO_3166_ALPHA2[source.upper()]["alpha3"]
        country_name = ISO_3166_ALPHA2[source.upper()][lang] if ISO_3166_ALPHA2[source.upper()].has_key(lang) else ISO_3166_ALPHA2[source.upper()]["fullname"]
        country_demonym = ISO_3166_ALPHA2_DEMONYM[source.upper()]["demonym"]
      for target in E[source]:

        if E[source][target] >= limitm and E[source][target] <= limits:
            
            EDGES.append({"strength": E[source][target], 
                      "source": '👤'+country_name if country_name != "---" else '👤'+str(source), #"source": country_demonym if country_demonym != "---" else source, 
                      "target": target,
                      "country_name": country_name,
                      "country_demonym": country_demonym,
                      "country_alpha3": country_alpha3})

  return {"edges": EDGES}

def rings_for_type(data, key, limitm,lang):
  limits = 99999999999999999
  E = {}
  EDGES = []
  data = data["hits"]["hits"]
  for d in data:
    aff_countries = d["fields"]["aff_countries"]
    aux = d["fields"][key]
    for i in aff_countries:
      if i != 'undefined':
        if i not in E:
          E[i] = {}
        for j in aux:
          if j not in E[i]:
            E[i][j] = 0
          E[i][j] += 1
    EDGES = []
    for source in E:
      country_name, country_alpha3 = "---", "---"
      if ISO_3166_ALPHA2.has_key(source.upper()):
        country_alpha3 = ISO_3166_ALPHA2[source.upper()]["alpha3"]
        country_name = ISO_3166_ALPHA2[source.upper()][lang] if ISO_3166_ALPHA2[source.upper()].has_key(lang) else ISO_3166_ALPHA2[source.upper()]["fullname"]
      for target in E[source]:
        if E[source][target] >= limitm and E[source][target] <= limits:
            EDGES.append({"strength": E[source][target],
                      "source": country_name if country_name != "---" else source,
                      "target": target,
                      "country_name": country_name,
                      "country_alpha3": country_alpha3})
  return {"edges": EDGES}


def aff_network(data,limitm,limits,net, lang):
  G = nx.Graph()
  
  data = data["hits"]["hits"]
  for d in data:
    aff_countries = d["fields"]["aff_countries"]
    for i in range(len(aff_countries)):
      j = i+1
      while True:
        if j >= len(aff_countries):
          break
        if aff_countries[i] != 'undefined' and aff_countries[j] != 'undefined':
          if ISO_3166_ALPHA2.has_key(aff_countries[i]) and ISO_3166_ALPHA2.has_key(aff_countries[j]):
              n1_alpha3 = ISO_3166_ALPHA2[aff_countries[i]]['alpha3']
              n1_name = ISO_3166_ALPHA2[aff_countries[i]]['fullname']
              n2_alpha3 = ISO_3166_ALPHA2[aff_countries[j]]['alpha3']
              n2_name = ISO_3166_ALPHA2[aff_countries[j]]['fullname']
              flag = 0
              if n1_alpha3 in net:
                if n2_alpha3 in net[n1_alpha3]:
                  flag = 1
              if n2_alpha3 in net:
                if n1_alpha3 in net[n2_alpha3]:
                  flag = 1
              if flag == 1:
                G.add_edge(n1_name,n2_name)
        j += 1

  pos = nx.spring_layout(G)
  spring_layout = []
  for i in pos:
    spring_layout.append({"name": i,
                          "x":float(pos[i][0]),
                          "y":float(pos[i][1])})
  circular_layout = []
  pos = nx.circular_layout(G)
  for i in pos:
    circular_layout.append({"name":i,
                            "x":float(pos[i][0]),
                            "y":float(pos[i][1])})
  connections = []
  for i in G.edges():
    connections.append({"source":i[0], "target":i[1]})
  sample_data = []
  deg = G.degree()
  for i in deg:
    sample_data.append({"name":i,"degree":deg[i]})
  return {"data":sample_data,"connections":connections,"circular_layout":circular_layout,"spring_layout":spring_layout}


def data_table_from_ring2(data_rings):
  data_table = {}
  for d in data_rings["edges"]:
    if data_table.has_key(d["target"]):
      if ISO_3166_ALPHA2.has_key(d["country_alpha3"].upper()):
        data_table[d["target"]]["countries"].append([ISO_3166_ALPHA2[d["country_alpha3"].upper()]["alpha3"], d["source"]])
        data_table[d["target"]]["authors"] += int(d["strength"])
      else:
        data_table[d["target"]]["countries"].append([d["country_alpha3"], d["source"]])
        data_table[d["target"]]["authors"] += int(d["strength"])
    else:
      if ISO_3166_ALPHA2.has_key(d["country_alpha3"].upper()):
        data_table[d["target"]] = {"countries": [[ISO_3166_ALPHA2[d["country_alpha3"].upper()]["alpha3"], d["source"]]], "authors": int(d["strength"])}
      else:
        data_table[d["target"]] = {"countries": [[d["country_alpha3"], d["source"]]], "authors": int(d["strength"])}
  return data_table