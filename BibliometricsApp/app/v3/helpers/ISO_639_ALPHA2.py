#!/usr/bin/env python
# -*- coding: utf-8 -*-
ISO_639_ALPHA2 = {
	'gv': {
		'alpha3': 'glv',
		'en': 'Manx',
		'es': 'Manx',
		'pt': 'Manx',
		'english': 'Manx'
	},
	'gu': {
		'alpha3': 'guj',
		'en': 'Gujarati',
		'es': 'Gujarati',
		'pt': 'Gujarati',
		'english': 'Gujarati'
	},
	'gd': {
		'alpha3': 'gla',
		'en': 'Gaelic - Scottish Gaelic',
		'es': 'Ga\xc3\xa9lico escoc\xc3\xa9s, ga\xc3\xa9lico',
		'pt': 'Ga\xc3\xa9lico escoc\xc3\xa9s, ga\xc3\xa9lico',
		'english': 'Gaelic - Scottish Gaelic'
	},
	'ga': {
		'alpha3': 'gle',
		'en': 'Irish',
		'es': 'Irlanda',
		'pt': 'Irlanda',
		'english': 'Irish'
	},
	'gn': {
		'alpha3': 'grn',
		'en': 'Guarani',
		'es': 'Guaran\xc3\xad',
		'pt': 'Guaran\xc3\xad',
		'english': 'Guarani'
	},
	'gl': {
		'alpha3': 'glg',
		'en': 'Galician',
		'es': 'Galicia',
		'pt': 'Galicia',
		'english': 'Galician'
	},
	'ty': {
		'alpha3': 'tah',
		'en': 'Tahitian',
		'es': 'Tahitian',
		'pt': 'Tahitian',
		'english': 'Tahitian'
	},
	'tw': {
		'alpha3': 'twi',
		'en': 'Twi',
		'es': 'Twi',
		'pt': 'Twi',
		'english': 'Twi'
	},
	'tt': {
		'alpha3': 'tat',
		'en': 'Tatar',
		'es': 'T\xc3\xa1rtara',
		'pt': 'T\xc3\xa1rtara',
		'english': 'Tatar'
	},
	'tr': {
		'alpha3': 'tur',
		'en': 'Turkish',
		'es': 'Turco',
		'pt': 'Turco',
		'english': 'Turkish'
	},
	'ts': {
		'alpha3': 'tso',
		'en': 'Tsonga',
		'es': 'Tsonga',
		'pt': 'Tsonga',
		'english': 'Tsonga'
	},
	'tn': {
		'alpha3': 'tsn',
		'en': 'Tswana',
		'es': 'Tswana',
		'pt': 'Tswana',
		'english': 'Tswana'
	},
	'to': {
		'alpha3': 'ton',
		'en': 'Tonga (Tonga Islands)',
		'es': 'Tonga (Islas Tonga)',
		'pt': 'Tonga (Islas Tonga)',
		'english': 'Tonga (Tonga Islands)'
	},
	'tl': {
		'alpha3': 'tgl',
		'en': 'Tagalog',
		'es': 'Tagalo',
		'pt': 'Tagalo',
		'english': 'Tagalog'
	},
	'tk': {
		'alpha3': 'tuk',
		'en': 'Turkmen',
		'es': 'Turkmenist\xc3\xa1n',
		'pt': 'Turkmenist\xc3\xa1n',
		'english': 'Turkmen'
	},
	'th': {
		'alpha3': 'tha',
		'en': 'Thai',
		'es': 'Tailandia',
		'pt': 'Tailandia',
		'english': 'Thai'
	},
	'ti': {
		'alpha3': 'tir',
		'en': 'Tigrinya',
		'es': 'Tigrinya',
		'pt': 'Tigrinya',
		'english': 'Tigrinya'
	},
	'tg': {
		'alpha3': 'tgk',
		'en': 'Tajik',
		'es': 'Tayikist\xc3\xa1n',
		'pt': 'Tayikist\xc3\xa1n',
		'english': 'Tajik'
	},
	'te': {
		'alpha3': 'tel',
		'en': 'Telugu',
		'es': 'Telugu',
		'pt': 'Telugu',
		'english': 'Telugu'
	},
	'ta': {
		'alpha3': 'tam',
		'en': 'Tamil',
		'es': 'Tamil',
		'pt': 'Tamil',
		'english': 'Tamil'
	},
	'de': {
		'alpha3': 'ger',
		'en': 'German',
		'es': 'Alem\xc3\xa1n',
		'pt': 'Alemão',
		'english': 'German'
	},
	'da': {
		'alpha3': 'dan',
		'en': 'Danish',
		'es': 'Dan\xc3\xa9s',
		'pt': 'Dan\xc3\xa9s',
		'english': 'Danish'
	},
	'dz': {
		'alpha3': 'dzo',
		'en': 'Dzongkha',
		'es': 'Dzongkha',
		'pt': 'Dzongkha',
		'english': 'Dzongkha'
	},
	'dv': {
		'alpha3': 'div',
		'en': 'Divehi - Dhivehi - Maldivian',
		'es': 'Divehi, Dhivehi, Maldivas',
		'pt': 'Divehi, Dhivehi, Maldivas',
		'english': 'Divehi - Dhivehi - Maldivian'
	},
	'qu': {
		'alpha3': 'que',
		'en': 'Quechua',
		'es': 'Quechua',
		'pt': 'Quechua',
		'english': 'Quechua'
	},
	'zh': {
		'alpha3': 'chi',
		'en': 'Chinese',
		'es': 'Chino',
		'pt': 'Chino',
		'english': 'Chinese'
	},
	'za': {
		'alpha3': 'zha',
		'en': 'Zhuang - Chuang',
		'es': 'Zhuang, Chuang',
		'pt': 'Zhuang, Chuang',
		'english': 'Zhuang - Chuang'
	},
	'zu': {
		'alpha3': 'zul',
		'en': 'Zulu',
		'es': 'Zulu',
		'pt': 'Zulu',
		'english': 'Zulu'
	},
	'wa': {
		'alpha3': 'wln',
		'en': 'Walloon',
		'es': 'Valonia',
		'pt': 'Valonia',
		'english': 'Walloon'
	},
	'wo': {
		'alpha3': 'wol',
		'en': 'Wolof',
		'es': 'Wolof',
		'pt': 'Wolof',
		'english': 'Wolof'
	},
	'jv': {
		'alpha3': 'jav',
		'en': 'Javanese',
		'es': 'Javan\xc3\xa9s',
		'pt': 'Javan\xc3\xa9s',
		'english': 'Javanese'
	},
	'ja': {
		'alpha3': 'jpn',
		'en': 'Japanese',
		'es': 'Japon\xc3\xa9s',
		'pt': 'Japon\xc3\xa9s',
		'english': 'Japanese'
	},
	'ch': {
		'alpha3': 'cha',
		'en': 'Chamorro',
		'es': 'Chamorro',
		'pt': 'Chamorro',
		'english': 'Chamorro'
	},
	'co': {
		'alpha3': 'cos',
		'en': 'Corsican',
		'es': 'Corso',
		'pt': 'Corso',
		'english': 'Corsican'
	},
	'ca': {
		'alpha3': 'cat',
		'en': 'Catalan - Valencian',
		'es': 'Catal\xc3\xa1n',
		'pt': 'Catal\xc3\xa1n',
		'english': 'Catalan - Valencian'
	},
	'ce': {
		'alpha3': 'che',
		'en': 'Chechen',
		'es': 'Chechenio',
		'pt': 'Chechenio',
		'english': 'Chechen'
	},
	'cy': {
		'alpha3': 'wel',
		'en': 'Welsh',
		'es': 'Gal\xc3\xa9s',
		'pt': 'Gal\xc3\xa9s',
		'english': 'Welsh'
	},
	'cs': {
		'alpha3': 'cze',
		'en': 'Czech',
		'es': 'Checo',
		'pt': 'Checo',
		'english': 'Czech'
	},
	'cr': {
		'alpha3': 'cre',
		'en': 'Cree',
		'es': 'Cree',
		'pt': 'Cree',
		'english': 'Cree'
	},
	'cv': {
		'alpha3': 'chv',
		'en': 'Chuvash',
		'es': 'Chuvashia',
		'pt': 'Chuvashia',
		'english': 'Chuvash'
	},
	'cu': {
		'alpha3': 'chu',
		'en': 'Church Slavic - Old Slavonic - Church Slavonic - Old Bulgarian - Old Church Slavonic',
		'es': 'Antiguo eslavo eclesi\xc3\xa1stico, Iglesia eslava, eslavo eclesi\xc3\xa1stico, antiguo B\xc3\xbalgaro, Esclavo viejo',
		'pt': 'Antiguo eslavo eclesi\xc3\xa1stico, Iglesia eslava, eslavo eclesi\xc3\xa1stico, antiguo B\xc3\xbalgaro, Esclavo viejo',
		'english': 'Church Slavic - Old Slavonic - Church Slavonic - Old Bulgarian - Old Church Slavonic'
	},
	'ps': {
		'alpha3': 'pus',
		'en': 'Pushto - Pashto',
		'es': 'Pashto, Pushto',
		'pt': 'Pashto, Pushto',
		'english': 'Pushto - Pashto'
	},
	'pt': {
		'alpha3': 'por',
		'en': 'Portuguese',
		'es': 'Portugu\xc3\xa9s',
		'pt': 'Português',
		'english': 'Portuguese'
	},
	'pa': {
		'alpha3': 'pan',
		'en': 'Panjabi - Punjabi',
		'es': 'Panjabi, Punjabi',
		'pt': 'Panjabi, Punjabi',
		'english': 'Panjabi - Punjabi'
	},
	'pi': {
		'alpha3': 'pli',
		'en': 'Pali',
		'es': 'P\xc4\x81li',
		'pt': 'P\xc4\x81li',
		'english': 'Pali'
	},
	'pl': {
		'alpha3': 'pol',
		'en': 'Polish',
		'es': 'Polaco',
		'pt': 'Polaco',
		'english': 'Polish'
	},
	'mg': {
		'alpha3': 'mlg',
		'en': 'Malagasy',
		'es': 'Madagascar',
		'pt': 'Madagascar',
		'english': 'Malagasy'
	},
	'ml': {
		'alpha3': 'mal',
		'en': 'Malayalam',
		'es': 'Malayalam',
		'pt': 'Malayalam',
		'english': 'Malayalam'
	},
	'mn': {
		'alpha3': 'mon',
		'en': 'Mongolian',
		'es': 'Mongolia',
		'pt': 'Mongolia',
		'english': 'Mongolian'
	},
	'mi': {
		'alpha3': 'mao',
		'en': 'Maori',
		'es': 'M\xc4\x81ori',
		'pt': 'M\xc4\x81ori',
		'english': 'Maori'
	},
	'mh': {
		'alpha3': 'mah',
		'en': 'Marshallese',
		'es': 'De las Islas Marshall',
		'pt': 'De las Islas Marshall',
		'english': 'Marshallese'
	},
	'mk': {
		'alpha3': 'mac',
		'en': 'Macedonian',
		'es': 'Macedonia',
		'pt': 'Macedonia',
		'english': 'Macedonian'
	},
	'mt': {
		'alpha3': 'mlt',
		'en': 'Maltese',
		'es': 'Malt\xc3\xa9s',
		'pt': 'Malt\xc3\xa9s',
		'english': 'Maltese'
	},
	'ms': {
		'alpha3': 'may',
		'en': 'Malay',
		'es': 'Malayo',
		'pt': 'Malayo',
		'english': 'Malay'
	},
	'mr': {
		'alpha3': 'mar',
		'en': 'Marathi',
		'es': 'Marat\xc3\xad (Marathi)',
		'pt': 'Marat\xc3\xad (Marathi)',
		'english': 'Marathi'
	},
	'my': {
		'alpha3': 'bur',
		'en': 'Burmese',
		'es': 'Burmese',
		'pt': 'Burmese',
		'english': 'Burmese'
	},
	've': {
		'alpha3': 'ven',
		'en': 'Venda',
		'es': 'Venda',
		'pt': 'Venda',
		'english': 'Venda'
	},
	'vi': {
		'alpha3': 'vie',
		'en': 'Vietnamese',
		'es': 'Vietnamita',
		'pt': 'Vietnamita',
		'english': 'Vietnamese'
	},
	'is': {
		'alpha3': 'ice',
		'en': 'Icelandic',
		'es': 'Island\xc3\xa9s',
		'pt': 'Island\xc3\xa9s',
		'english': 'Icelandic'
	},
	'iu': {
		'alpha3': 'iku',
		'en': 'Inuktitut',
		'es': 'Inuktitut',
		'pt': 'Inuktitut',
		'english': 'Inuktitut'
	},
	'it': {
		'alpha3': 'ita',
		'en': 'Italian',
		'es': 'Italiano',
		'pt': 'Italiano',
		'english': 'Italian'
	},
	'vo': {
		'alpha3': 'vol',
		'en': 'Volap\xc3\xbck',
		'es': 'Volap\xc3\xbck',
		'pt': 'Volap\xc3\xbck',
		'english': 'Volap\xc3\xbck'
	},
	'ii': {
		'alpha3': 'iii',
		'en': 'Sichuan Yi - Nuosu',
		'es': 'Nuosu',
		'pt': 'Nuosu',
		'english': 'Sichuan Yi - Nuosu'
	},
	'ik': {
		'alpha3': 'ipk',
		'en': 'Inupiaq',
		'es': 'Inupiaq',
		'pt': 'Inupiaq',
		'english': 'Inupiaq'
	},
	'io': {
		'alpha3': 'ido',
		'en': 'Ido',
		'es': 'Ido',
		'pt': 'Ido',
		'english': 'Ido'
	},
	'ia': {
		'alpha3': 'ina',
		'en': 'Interlingua (International Auxiliary Language Association)',
		'es': 'Interlingua',
		'pt': 'Interlingua',
		'english': 'Interlingua (International Auxiliary Language Association)'
	},
	'ie': {
		'alpha3': 'ile',
		'en': 'Interlingue - Occidental',
		'es': 'Interlingue',
		'pt': 'Interlingue',
		'english': 'Interlingue - Occidental'
	},
	'id': {
		'alpha3': 'ind',
		'en': 'Indonesian',
		'es': 'Indonesio',
		'pt': 'Indonesio',
		'english': 'Indonesian'
	},
	'ig': {
		'alpha3': 'ibo',
		'en': 'Igbo',
		'es': 'Igbo',
		'pt': 'Igbo',
		'english': 'Igbo'
	},
	'fr': {
		'alpha3': 'fre',
		'en': 'French',
		'es': 'Franc\xc3\xa9s',
		'pt': 'Francês',
		'english': 'French'
	},
	'fy': {
		'alpha3': 'fry',
		'en': 'Western Frisian',
		'es': 'Oeste de Frisia',
		'pt': 'Oeste de Frisia',
		'english': 'Western Frisian'
	},
	'fa': {
		'alpha3': 'per',
		'en': 'Persian',
		'es': 'Persa',
		'pt': 'Persa',
		'english': 'Persian'
	},
	'ff': {
		'alpha3': 'ful',
		'en': 'Fulah',
		'es': 'Fula, Fulah, Pulaar, Pular',
		'pt': 'Fula, Fulah, Pulaar, Pular',
		'english': 'Fulah'
	},
	'fi': {
		'alpha3': 'fin',
		'en': 'Finnish',
		'es': 'Finland\xc3\xa9s',
		'pt': 'Finland\xc3\xa9s',
		'english': 'Finnish'
	},
	'fj': {
		'alpha3': 'fij',
		'en': 'Fijian',
		'es': 'Fiji',
		'pt': 'Fiji',
		'english': 'Fijian'
	},
	'fo': {
		'alpha3': 'fao',
		'en': 'Faroese',
		'es': 'Faroese',
		'pt': 'Faroese',
		'english': 'Faroese'
	},
	'ss': {
		'alpha3': 'ssw',
		'en': 'Swati',
		'es': 'Swati',
		'pt': 'Swati',
		'english': 'Swati'
	},
	'sr': {
		'alpha3': 'srp',
		'en': 'Serbian',
		'es': 'Serbio',
		'pt': 'Serbio',
		'english': 'Serbian'
	},
	'sq': {
		'alpha3': 'alb',
		'en': 'Albanian',
		'es': 'Albania',
		'pt': 'Albania',
		'english': 'Albanian'
	},
	'sw': {
		'alpha3': 'swa',
		'en': 'Swahili',
		'es': 'Swahili',
		'pt': 'Swahili',
		'english': 'Swahili'
	},
	'sv': {
		'alpha3': 'swe',
		'en': 'Swedish',
		'es': 'Sueco',
		'pt': 'Sueco',
		'english': 'Swedish'
	},
	'su': {
		'alpha3': 'sun',
		'en': 'Sundanese',
		'es': 'Sundanese',
		'pt': 'Sundanese',
		'english': 'Sundanese'
	},
	'st': {
		'alpha3': 'sot',
		'en': 'Sotho, Southern',
		'es': 'Southern Sotho',
		'pt': 'Southern Sotho',
		'english': 'Sotho, Southern'
	},
	'sk': {
		'alpha3': 'slo',
		'en': 'Slovak',
		'es': 'Eslovaca',
		'pt': 'Eslovaca',
		'english': 'Slovak'
	},
	'si': {
		'alpha3': 'sin',
		'en': 'Sinhala - Sinhalese',
		'es': 'Cingal\xc3\xa9s, singal\xc3\xa9s',
		'pt': 'Cingal\xc3\xa9s, singal\xc3\xa9s',
		'english': 'Sinhala - Sinhalese'
	},
	'so': {
		'alpha3': 'som',
		'en': 'Somali',
		'es': 'Somal\xc3\xad',
		'pt': 'Somal\xc3\xad',
		'english': 'Somali'
	},
	'sn': {
		'alpha3': 'sna',
		'en': 'Shona',
		'es': 'Shona',
		'pt': 'Shona',
		'english': 'Shona'
	},
	'sm': {
		'alpha3': 'smo',
		'en': 'Samoan',
		'es': 'Samoa',
		'pt': 'Samoa',
		'english': 'Samoan'
	},
	'sl': {
		'alpha3': 'slv',
		'en': 'Slovenian',
		'es': 'Esloveno',
		'pt': 'Esloveno',
		'english': 'Slovenian'
	},
	'sc': {
		'alpha3': 'srd',
		'en': 'Sardinian',
		'es': 'Sardo',
		'pt': 'Sardo',
		'english': 'Sardinian'
	},
	'sa': {
		'alpha3': 'san',
		'en': 'Sanskrit',
		'es': 'S\xc3\xa1nscrito (samskrta)',
		'pt': 'S\xc3\xa1nscrito (samskrta)',
		'english': 'Sanskrit'
	},
	'sg': {
		'alpha3': 'sag',
		'en': 'Sango',
		'es': 'Sango',
		'pt': 'Sango',
		'english': 'Sango'
	},
	'se': {
		'alpha3': 'sme',
		'en': 'Northern Sami',
		'es': 'Sami del norte',
		'pt': 'Sami del norte',
		'english': 'Northern Sami'
	},
	'sd': {
		'alpha3': 'snd',
		'en': 'Sindhi',
		'es': 'Sindhi',
		'pt': 'Sindhi',
		'english': 'Sindhi'
	},
	'lg': {
		'alpha3': 'lug',
		'en': 'Ganda',
		'es': 'Luganda',
		'pt': 'Luganda',
		'english': 'Ganda'
	},
	'lb': {
		'alpha3': 'ltz',
		'en': 'Luxembourgish - Letzeburgesch',
		'es': 'Luxemburgu\xc3\xa9s, Luxemburgu\xc3\xa9s',
		'pt': 'Luxemburgu\xc3\xa9s, Luxemburgu\xc3\xa9s',
		'english': 'Luxembourgish - Letzeburgesch'
	},
	'la': {
		'alpha3': 'lat',
		'en': 'Latin',
		'es': 'Latin',
		'pt': 'Latin',
		'english': 'Latin'
	},
	'ln': {
		'alpha3': 'lin',
		'en': 'Lingala',
		'es': 'Lingala',
		'pt': 'Lingala',
		'english': 'Lingala'
	},
	'lo': {
		'alpha3': 'lao',
		'en': 'Lao',
		'es': 'Lao',
		'pt': 'Lao',
		'english': 'Lao'
	},
	'li': {
		'alpha3': 'lim',
		'en': 'Limburgan - Limburger - Limburgish',
		'es': 'Limburgu\xc3\xa9s, Limburgan, Limburger',
		'pt': 'Limburgu\xc3\xa9s, Limburgan, Limburger',
		'english': 'Limburgan - Limburger - Limburgish'
	},
	'lv': {
		'alpha3': 'lav',
		'en': 'Latvian',
		'es': 'Letonia',
		'pt': 'Letonia',
		'english': 'Latvian'
	},
	'lt': {
		'alpha3': 'lit',
		'en': 'Lithuanian',
		'es': 'Lituano',
		'pt': 'Lituano',
		'english': 'Lithuanian'
	},
	'lu': {
		'alpha3': 'lub',
		'en': 'Luba-Katanga',
		'es': 'Luba-Katanga',
		'pt': 'Luba-Katanga',
		'english': 'Luba-Katanga'
	},
	'yi': {
		'alpha3': 'yid',
		'en': 'Yiddish',
		'es': 'Yiddish',
		'pt': 'Yiddish',
		'english': 'Yiddish'
	},
	'yo': {
		'alpha3': 'yor',
		'en': 'Yoruba',
		'es': 'Yoruba',
		'pt': 'Yoruba',
		'english': 'Yoruba'
	},
	'el': {
		'alpha3': 'gre',
		'en': 'Greek, Modern (1453-)',
		'es': 'Griego Moderno',
		'pt': 'Griego Moderno',
		'english': 'Greek, Modern (1453-)'
	},
	'eo': {
		'alpha3': 'epo',
		'en': 'Esperanto',
		'es': 'Esperanto',
		'pt': 'Esperanto',
		'english': 'Esperanto'
	},
	'en': {
		'alpha3': 'eng',
		'en': 'English',
		'es': 'Ingl\xc3\xa9s',
		'pt': 'Inglês',
		'english': 'English'
	},
	'ee': {
		'alpha3': 'ewe',
		'en': 'Ewe',
		'es': 'Ewe',
		'pt': 'Ewe',
		'english': 'Ewe'
	},
	'eu': {
		'alpha3': 'baq',
		'en': 'Basque',
		'es': 'Vasco',
		'pt': 'Vasco',
		'english': 'Basque'
	},
	'et': {
		'alpha3': 'est',
		'en': 'Estonian',
		'es': 'Estonia',
		'pt': 'Estonia',
		'english': 'Estonian'
	},
	'es': {
		'alpha3': 'spa',
		'en': 'Spanish',
		'es': 'Espa\xc3\xb1ol',
		'pt': 'Espanhol',
		'english': 'Spanish'
	},
	'ru': {
		'alpha3': 'rus',
		'en': 'Russian',
		'es': 'Ruso',
		'pt': 'Ruso',
		'english': 'Russian'
	},
	'rw': {
		'alpha3': 'kin',
		'en': 'Kinyarwanda',
		'es': 'Kinyarwanda',
		'pt': 'Kinyarwanda',
		'english': 'Kinyarwanda'
	},
	'rm': {
		'alpha3': 'roh',
		'en': 'Romansh',
		'es': 'Romanche',
		'pt': 'Romanche',
		'english': 'Romansh'
	},
	'rn': {
		'alpha3': 'run',
		'en': 'Rundi',
		'es': 'Kirundi',
		'pt': 'Kirundi',
		'english': 'Rundi'
	},
	'ro': {
		'alpha3': 'rum',
		'en': 'Romanian - Moldavian - Moldovan',
		'es': 'Rumania, Moldavia, Moldavan',
		'pt': 'Rumania, Moldavia, Moldavan',
		'english': 'Romanian - Moldavian - Moldovan'
	},
	'be': {
		'alpha3': 'bel',
		'en': 'Belarusian',
		'es': 'Belar\xc3\xbas',
		'pt': 'Belar\xc3\xbas',
		'english': 'Belarusian'
	},
	'bg': {
		'alpha3': 'bul',
		'en': 'Bulgarian',
		'es': 'B\xc3\xbalgaro',
		'pt': 'B\xc3\xbalgaro',
		'english': 'Bulgarian'
	},
	'ba': {
		'alpha3': 'bak',
		'en': 'Bashkir',
		'es': 'Bashkir',
		'pt': 'Bashkir',
		'english': 'Bashkir'
	},
	'bm': {
		'alpha3': 'bam',
		'en': 'Bambara',
		'es': 'Bambara',
		'pt': 'Bambara',
		'english': 'Bambara'
	},
	'bn': {
		'alpha3': 'ben',
		'en': 'Bengali',
		'es': 'Bengal\xc3\xad',
		'pt': 'Bengal\xc3\xad',
		'english': 'Bengali'
	},
	'bo': {
		'alpha3': 'tib',
		'en': 'Tibetan',
		'es': 'Tibetano est\xc3\xa1ndar, Tibetano, Central',
		'pt': 'Tibetano est\xc3\xa1ndar, Tibetano, Central',
		'english': 'Tibetan'
	},
	'bh': {
		'alpha3': 'bih',
		'en': 'Bihari languages',
		'es': 'Bihari',
		'pt': 'Bihari',
		'english': 'Bihari languages'
	},
	'bi': {
		'alpha3': 'bis',
		'en': 'Bislama',
		'es': 'Bislama',
		'pt': 'Bislama',
		'english': 'Bislama'
	},
	'br': {
		'alpha3': 'bre',
		'en': 'Breton',
		'es': 'Breton',
		'pt': 'Breton',
		'english': 'Breton'
	},
	'bs': {
		'alpha3': 'bos',
		'en': 'Bosnian',
		'es': 'Bosnia',
		'pt': 'Bosnia',
		'english': 'Bosnian'
	},
	'om': {
		'alpha3': 'orm',
		'en': 'Oromo',
		'es': 'Oromo',
		'pt': 'Oromo',
		'english': 'Oromo'
	},
	'oj': {
		'alpha3': 'oji',
		'en': 'Ojibwa',
		'es': 'Ojibwe, Ojibwa',
		'pt': 'Ojibwe, Ojibwa',
		'english': 'Ojibwa'
	},
	'oc': {
		'alpha3': 'oci',
		'en': 'Occitan (post 1500) - Proven\xc3\xa7al',
		'es': 'Occitano',
		'pt': 'Occitano',
		'english': 'Occitan (post 1500) - Proven\xc3\xa7al'
	},
	'os': {
		'alpha3': 'oss',
		'en': 'Ossetian - Ossetic',
		'es': 'Osetia del Sur, osetio',
		'pt': 'Osetia del Sur, osetio',
		'english': 'Ossetian - Ossetic'
	},
	'or': {
		'alpha3': 'ori',
		'en': 'Oriya',
		'es': 'Oriya',
		'pt': 'Oriya',
		'english': 'Oriya'
	},
	'xh': {
		'alpha3': 'xho',
		'en': 'Xhosa',
		'es': 'Xhosa',
		'pt': 'Xhosa',
		'english': 'Xhosa'
	},
	'hz': {
		'alpha3': 'her',
		'en': 'Herero',
		'es': 'Herero',
		'pt': 'Herero',
		'english': 'Herero'
	},
	'hy': {
		'alpha3': 'arm',
		'en': 'Armenian',
		'es': 'Armenio',
		'pt': 'Armenio',
		'english': 'Armenian'
	},
	'hr': {
		'alpha3': 'hrv',
		'en': 'Croatian',
		'es': 'Croacia',
		'pt': 'Croacia',
		'english': 'Croatian'
	},
	'ht': {
		'alpha3': 'hat',
		'en': 'Haitian - Haitian Creole',
		'es': 'Haitiano, creole haitiano',
		'pt': 'Haitiano, creole haitiano',
		'english': 'Haitian - Haitian Creole'
	},
	'hu': {
		'alpha3': 'hun',
		'en': 'Hungarian',
		'es': 'H\xc3\xbangaro',
		'pt': 'H\xc3\xbangaro',
		'english': 'Hungarian'
	},
	'hi': {
		'alpha3': 'hin',
		'en': 'Hindi',
		'es': 'Hindi',
		'pt': 'Hindi',
		'english': 'Hindi'
	},
	'ho': {
		'alpha3': 'hmo',
		'en': 'Hiri Motu',
		'es': 'Hiri Motu',
		'pt': 'Hiri Motu',
		'english': 'Hiri Motu'
	},
	'ha': {
		'alpha3': 'hau',
		'en': 'Hausa',
		'es': 'Hausa',
		'pt': 'Hausa',
		'english': 'Hausa'
	},
	'he': {
		'alpha3': 'heb',
		'en': 'Hebrew',
		'es': 'Hebreo (moderno)',
		'pt': 'Hebreo (moderno)',
		'english': 'Hebrew'
	},
	'uz': {
		'alpha3': 'uzb',
		'en': 'Uzbek',
		'es': 'Uzbeko',
		'pt': 'Uzbeko',
		'english': 'Uzbek'
	},
	'ur': {
		'alpha3': 'urd',
		'en': 'Urdu',
		'es': 'Urdu',
		'pt': 'Urdu',
		'english': 'Urdu'
	},
	'uk': {
		'alpha3': 'ukr',
		'en': 'Ukrainian',
		'es': 'Ucrania',
		'pt': 'Ucrania',
		'english': 'Ukrainian'
	},
	'ug': {
		'alpha3': 'uig',
		'en': 'Uighur - Uyghur',
		'es': 'Uighur, Uyghur',
		'pt': 'Uighur, Uyghur',
		'english': 'Uighur - Uyghur'
	},
	'aa': {
		'alpha3': 'aar',
		'en': 'Afar',
		'es': 'Afar',
		'pt': 'Afar',
		'english': 'Afar'
	},
	'ab': {
		'alpha3': 'abk',
		'en': 'Abkhazian',
		'es': 'Abkhaz',
		'pt': 'Abkhaz',
		'english': 'Abkhazian'
	},
	'ae': {
		'alpha3': 'ave',
		'en': 'Avestan',
		'es': 'Avestan',
		'pt': 'Avestan',
		'english': 'Avestan'
	},
	'af': {
		'alpha3': 'afr',
		'en': 'Afrikaans',
		'es': 'Africanos',
		'pt': 'Africaner',
		'english': 'Afrikaans'
	},
	'ak': {
		'alpha3': 'aka',
		'en': 'Akan',
		'es': 'Akan',
		'pt': 'Akan',
		'english': 'Akan'
	},
	'am': {
		'alpha3': 'amh',
		'en': 'Amharic',
		'es': 'Am\xc3\xa1rico',
		'pt': 'Am\xc3\xa1rico',
		'english': 'Amharic'
	},
	'an': {
		'alpha3': 'arg',
		'en': 'Aragonese',
		'es': 'Aragon\xc3\xa9s',
		'pt': 'Aragon\xc3\xa9s',
		'english': 'Aragonese'
	},
	'as': {
		'alpha3': 'asm',
		'en': 'Assamese',
		'es': 'Assamese',
		'pt': 'Assamese',
		'english': 'Assamese'
	},
	'ar': {
		'alpha3': 'ara',
		'en': 'Arabic',
		'es': '\xc3\x81rabe',
		'pt': '\xc3\x81rabe',
		'english': 'Arabic'
	},
	'av': {
		'alpha3': 'ava',
		'en': 'Avaric',
		'es': 'Avaric',
		'pt': 'Avaric',
		'english': 'Avaric'
	},
	'ay': {
		'alpha3': 'aym',
		'en': 'Aymara',
		'es': 'Aymara',
		'pt': 'Aymara',
		'english': 'Aymara'
	},
	'az': {
		'alpha3': 'aze',
		'en': 'Azerbaijani',
		'es': 'Azerbaiy\xc3\xa1n',
		'pt': 'Azerbaiy\xc3\xa1n',
		'english': 'Azerbaijani'
	},
	'nl': {
		'alpha3': 'dut',
		'en': 'Dutch - Flemish',
		'es': 'Holand\xc3\xa9s',
		'pt': 'Holandês',
		'english': 'Dutch - Flemish'
	},
	'nn': {
		'alpha3': 'nno',
		'en': 'Norwegian Nynorsk - Nynorsk, Norwegian',
		'es': 'Noruego Nynorsk',
		'pt': 'Noruego Nynorsk',
		'english': 'Norwegian Nynorsk - Nynorsk, Norwegian'
	},
	'no': {
		'alpha3': 'nor',
		'en': 'Norwegian',
		'es': 'Noruego',
		'pt': 'Noruego',
		'english': 'Norwegian'
	},
	'na': {
		'alpha3': 'nau',
		'en': 'Nauru',
		'es': 'Nauru',
		'pt': 'Nauru',
		'english': 'Nauru'
	},
	'nb': {
		'alpha3': 'nob',
		'en': 'Bokm\xc3\xa5l, Norwegian - Norwegian Bokm\xc3\xa5l',
		'es': 'Noruego Bokm\xc3\xa5l',
		'pt': 'Noruego Bokm\xc3\xa5l',
		'english': 'Bokm\xc3\xa5l, Norwegian - Norwegian Bokm\xc3\xa5l'
	},
	'nd': {
		'alpha3': 'nde',
		'en': 'North Ndebele',
		'es': 'Ndebele del Norte',
		'pt': 'Ndebele del Norte',
		'english': 'North Ndebele'
	},
	'ne': {
		'alpha3': 'nep',
		'en': 'Nepali',
		'es': 'Nepali',
		'pt': 'Nepali',
		'english': 'Nepali'
	},
	'ng': {
		'alpha3': 'ndo',
		'en': 'Ndonga',
		'es': 'Ndonga',
		'pt': 'Ndonga',
		'english': 'Ndonga'
	},
	'ny': {
		'alpha3': 'nya',
		'en': 'Chichewa - Chewa - Nyanja',
		'es': 'Chichewa, Chewa, Nyanja',
		'pt': 'Chichewa, Chewa, Nyanja',
		'english': 'Chichewa - Chewa - Nyanja'
	},
	'nr': {
		'alpha3': 'nbl',
		'en': 'Ndebele, South - South Ndebele',
		'es': 'Ndebele del sur',
		'pt': 'Ndebele del sur',
		'english': 'Ndebele, South - South Ndebele'
	},
	'nv': {
		'alpha3': 'nav',
		'en': 'Navajo - Navaho',
		'es': 'Navajo, Navaho',
		'pt': 'Navajo, Navaho',
		'english': 'Navajo - Navaho'
	},
	'ka': {
		'alpha3': 'geo',
		'en': 'Georgian',
		'es': 'Georgiano',
		'pt': 'Georgiano',
		'english': 'Georgian'
	},
	'kg': {
		'alpha3': 'kon',
		'en': 'Kongo',
		'es': 'Kongo',
		'pt': 'Kongo',
		'english': 'Kongo'
	},
	'kk': {
		'alpha3': 'kaz',
		'en': 'Kazakh',
		'es': 'Kazajst\xc3\xa1n',
		'pt': 'Kazajst\xc3\xa1n',
		'english': 'Kazakh'
	},
	'kj': {
		'alpha3': 'kua',
		'en': 'Kuanyama - Kwanyama',
		'es': 'Kwanyama, Kuanyama',
		'pt': 'Kwanyama, Kuanyama',
		'english': 'Kuanyama - Kwanyama'
	},
	'ki': {
		'alpha3': 'kik',
		'en': 'Kikuyu - Gikuyu',
		'es': 'Kikuyu, Gikuyu',
		'pt': 'Kikuyu, Gikuyu',
		'english': 'Kikuyu - Gikuyu'
	},
	'ko': {
		'alpha3': 'kor',
		'en': 'Korean',
		'es': 'Corea',
		'pt': 'Corea',
		'english': 'Korean'
	},
	'kn': {
		'alpha3': 'kan',
		'en': 'Kannada',
		'es': 'Canar\xc3\xa9s',
		'pt': 'Canar\xc3\xa9s',
		'english': 'Kannada'
	},
	'km': {
		'alpha3': 'khm',
		'en': 'Central Khmer',
		'es': 'Khmer',
		'pt': 'Khmer',
		'english': 'Central Khmer'
	},
	'kl': {
		'alpha3': 'kal',
		'en': 'Kalaallisut - Greenlandic',
		'es': 'Kalaallisut, Groenlandia',
		'pt': 'Kalaallisut, Groenlandia',
		'english': 'Kalaallisut - Greenlandic'
	},
	'ks': {
		'alpha3': 'kas',
		'en': 'Kashmiri',
		'es': 'Cachemira',
		'pt': 'Cachemira',
		'english': 'Kashmiri'
	},
	'kr': {
		'alpha3': 'kau',
		'en': 'Kanuri',
		'es': 'Kanuri',
		'pt': 'Kanuri',
		'english': 'Kanuri'
	},
	'kw': {
		'alpha3': 'cor',
		'en': 'Cornish',
		'es': 'Cornualles',
		'pt': 'Cornualles',
		'english': 'Cornish'
	},
	'kv': {
		'alpha3': 'kom',
		'en': 'Komi',
		'es': 'Komi',
		'pt': 'Komi',
		'english': 'Komi'
	},
	'ku': {
		'alpha3': 'kur',
		'en': 'Kurdish',
		'es': 'Kurdo',
		'pt': 'Kurdo',
		'english': 'Kurdish'
	},
	'ky': {
		'alpha3': 'kir',
		'en': 'Kirghiz - Kyrgyz',
		'es': 'Kirguises, Kirguist\xc3\xa1n',
		'pt': 'Kirguises, Kirguist\xc3\xa1n',
		'english': 'Kirghiz - Kyrgyz'
	}
}