#!/usr/bin/env python
# -*- coding: utf-8 -*-
from datetime import datetime

import hashlib
import json

class Filters():
	def __init__(self, scope = '1', tabulation = "t1",
				 visualization = 'map', frequency = 1,
				 from_year = 1909, to_year = datetime.now().year,
				 thematic_areas = ['ALL'], wos_thematic_areas = ['ALL'],
				 collections = [['ALL', 'ALL']],
				 journals = ['ALL'], languages = [['ALL', 'ALL']],
				 document_type = 'ALL', document_type_details = ['ALL'],
				 collections_operation = 'OR', journals_operation = 'OR',
				 languages_operation = 'OR', thematic_areas_operation = 'OR',
				 wos_thematic_areas_operation = 'OR', document_type_details_operation = 'OR',
				 lang = 'en'):
		self.scope = scope
		self.tabulation = tabulation
		self.visualization = visualization
		self.frequency = frequency
		self.from_year = from_year
		self.to_year = to_year
		self.thematic_areas = thematic_areas
		self.wos_thematic_areas = wos_thematic_areas
		self.collections = collections
		self.journals = journals
		self.languages = languages
		self.document_type = document_type
		self.document_type_details = document_type_details
		self.collections_operation = collections_operation
		self.journals_operation = journals_operation
		self.languages_operation = languages_operation
		self.thematic_areas_operation = thematic_areas_operation
		self.wos_thematic_areas_operation = wos_thematic_areas_operation
		self.document_type_details_operation = document_type_details_operation
		self.lang = lang
	
	@property
	def serialize(self):
		return {
			'scope': self.scope,
			'tabulation': self.tabulation,
			'visualization': self.visualization,
			'frequency': self.frequency,
			'from_year': self.from_year, 
			'to_year': self.to_year,
			'thematic_areas': self.thematic_areas, 
			'wos_thematic_areas': self.wos_thematic_areas,
			'collections': self.collections, 
			'journals': self.journals, 
			'languages': self.languages, 
			'document_type': self.document_type, 
			'document_type_details': self.document_type_details,
			'collections_operation': self.collections_operation,
			'journals_operation': self.journals_operation,
			'languages_operation': self.languages_operation,
			'thematic_areas_operation': self.thematic_areas_operation,
			'wos_thematic_areas_operation': self.wos_thematic_areas_operation,
			'document_type_details_operation': self.document_type_details_operation,
			'lang': self.lang,
		}

	@property
	def md5(self):
		fstr = json.dumps(self.serialize, indent=4).encode('utf8')
		return hashlib.md5(fstr).hexdigest()