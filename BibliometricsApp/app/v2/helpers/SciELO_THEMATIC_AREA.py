#!/usr/bin/env python
# -*- coding: utf-8 -*-
SciELO_THEMATIC_AREA = {
    "Health Sciences": {
      "en": "Health Sciences",
      "es": "Ciencias de la Salud",
      "pt": "Ciências da Saúde",
    },
    "Human Sciences": {
      "en": "Human Sciences",
      "es": "Humanidades",
      "pt": "Ciências Humanas",
    },
    "Agricultural Sciences": {
      "en": "Agricultural Sciences",
      "es": "Ciencias Agrícolas",
      "pt": "Ciências Agrárias",
    },
    "Biological Sciences": {
      "en": "Biological Sciences",
      "es": "Ciencias Biológicas",
      "pt": "Ciências Biológicas",
    },
    "Applied Social Sciences": {
      "en": "Applied Social Sciences",
      "es": "Ciencias Sociales Aplicadas",
      "pt": "Ciências Sociais Aplicadas",
    },
    "Exact and Earth Sciences": {
      "en": "Exact and Earth Sciences",
      "es": "Ciencias Exactas y de la Tierra",
      "pt": "Ciências Exatas e da Terra",
    },
    "Engineering": {
      "en": "Engineering",
      "es": "Ingenierias",
      "pt": "Engenharias",
    },
    "Multidisciplinary": {
      "en": "Multidisciplinary",
      "es": "Multidisciplinaria",
      "pt": "Multidisciplinar",
    },
    "Literature and Arts": {
      "en": "Literature and Arts",
      "es": "Lingüística, Letras y Artes",
      "pt": "Lingüística, Letras e Artes",
    }
}