#!/usr/bin/env python
# -*- coding: utf-8 -*-
from app.v2.classes.filters import Filters
from datetime import datetime
from app.v2.helpers.dicts import country_collections, not_collections
from app.v2.helpers.ISO_3166_ALPHA3 import ISO_3166_ALPHA3
from app.v2.helpers.ISO_639_ALPHA2 import ISO_639_ALPHA2
from app.v2.helpers.SciELO_THEMATIC_AREA import SciELO_THEMATIC_AREA
from app.v2.helpers.WOS_THEMATIC_AREA import WOS_THEMATIC_AREA
from app.v2.helpers.DOCUMENT_TYPE import DOCUMENT_TYPE


def process_filters_form(form):
  filters = Filters()
  #Scope
  filters.scope = form.get('scope', '1')
  #Tabulation
  filters.tabulation = form.get('tabulation', '1')
  ##Visualization
  filters.visualization = form.get('visualization', 'treemap')
  #Lang
  filters.lang = form.get('lang', 'en')
  #Frecuency
  filters.frequency = int(form.get('frequency', 1))
  #Years
  filters.from_year = form.get('years[from]', "1909")
  filters.to_year = form.get('years[to]', min(filters.from_year, datetime.now().year))
  #Collections
  collections = form.getlist('collections[]')
  filters.collections = []
  for c in collections:
    if c in country_collections and c not in not_collections:
      filters.collections.append( [c, ISO_3166_ALPHA3[country_collections[c]][filters.lang]] if ISO_3166_ALPHA3[country_collections[c]].has_key(filters.lang) else ISO_3166_ALPHA3[country_collections[c]]["fullname"] )
    else:
      filters.collections.append( [c, c] )
  filters.collections_operation = form.get('collections_operation', 'OR')
  #Journals
  filters.journals = form.getlist('journals[]')
  filters.journals_operation = form.get('journals_operation', 'OR')

  #Document Type
  filters.document_type = form.get('study_unit[document-type]', None)
  #Document Type Details
  document_type_details = form.getlist('document_type_details[]')
  filters.document_type_details = []
  for dtd in document_type_details:
    if DOCUMENT_TYPE.has_key(dtd):
      filters.document_type_details.append( [dtd, DOCUMENT_TYPE[dtd][filters.lang]] if DOCUMENT_TYPE[dtd].has_key(filters.lang) else [dtd, dtd] )
    else:
      filters.document_type_details.append( [dtd, dtd] )
  filters.document_type_details_operation = form.get('document_type_details_operation', 'OR')

  #Thematic Areas
  thematic_areas = form.getlist('thematic_areas[]')
  filters.thematic_areas = []
  for ta in thematic_areas:
    if SciELO_THEMATIC_AREA.has_key(ta):
      filters.thematic_areas.append( [ta, SciELO_THEMATIC_AREA[ta][filters.lang]] if SciELO_THEMATIC_AREA[ta].has_key(filters.lang) else [ta, ta] )
    else:
      filters.thematic_areas.append( [ta, ta] )
  filters.thematic_areas_operation = form.get('thematic_areas_operation', 'OR')

  #WOS Thematic Areas
  wos_thematic_areas = form.getlist('wos_thematic_areas[]')
  filters.wos_thematic_areas = []
  for wta in wos_thematic_areas:
    if WOS_THEMATIC_AREA.has_key(wta):
      filters.wos_thematic_areas.append( [wta, WOS_THEMATIC_AREA[wta][filters.lang]] if WOS_THEMATIC_AREA[wta].has_key(filters.lang) else [wta, wta] )
    else:
      filters.wos_thematic_areas.append( [wta, wta] )
  filters.wos_thematic_areas_operation = form.get('wos_thematic_areas_operation', 'OR')

  #Languages
  languages = form.getlist('languages[]')
  filters.languages = []
  for l in languages:
    if ISO_639_ALPHA2.has_key(l):
      filters.languages.append( [l, ISO_639_ALPHA2[l][filters.lang]] if ISO_639_ALPHA2[l].has_key(filters.lang) else [l, l])
    else:
      filters.languages.append( [l, l] )
  filters.languages_operation = form.get('languages_operation', 'OR')

  return filters


def process_filters_form_dict(form):
  filters = Filters()
  #Scope
  filters.scope = form['scope'] if form.has_key('scope') else '1'
  #Tabulation
  filters.tabulation = form['tabulation'] if form.has_key('tabulation') else '1'
  ##Visualization
  filters.visualization = form['visualization'] if form.has_key('visualization') else 'treemap'
  #Frequency
  filters.visualization = form['frequency'] if form.has_key('frequency') else 1
  #Years
  filters.to_year = form.get('years[to]', datetime.now().year)
  filters.from_year = form.get('years[from]', min(filters.to_year, datetime.now().year))
  #Collections
  filters.collections = form['collections']
  filters.collections_operation = form['collections_operation']
  #Journals
  filters.journals = form['journals']
  filters.journals_operation = form['journals_operation']

  #Document Type
  filters.document_type = form['document_type']
  #Document Type Details
  filters.document_type_details = form['document_type_details']
  filters.document_type_details_operation = form['document_type_details_operation']

  #Thematic Areas
  filters.thematic_areas = form['thematic_areas']
  filters.thematic_areas_operation = form['thematic_areas_operation']

  #WOS Thematic Areas
  filters.wos_thematic_areas = form['wos_thematic_areas']
  filters.wos_thematic_areas_operation = form['wos_thematic_areas_operation']

  #Languages
  filters.languages = form['languages']
  filters.languages_operation = form['languages_operation']

  return filters