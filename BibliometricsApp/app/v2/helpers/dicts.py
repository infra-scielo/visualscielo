#!/usr/bin/env python
# -*- coding: utf-8 -*-
CREATIVE_COMMONS_TEXTS = {
    "BY": "Attribution",
    "BY-ND": "Attribution-NoDerivatives",
    "BY-SA": "Attribution-ShareAlike",
    "BY-NC": "Attribution-NonCommercial",
    "BY-NC-ND": "Attribution-NonCommercial-NoDerivatives",
    "BY-NC-SA": "Attribution-NonCommercial-ShareAlike"
}

article_types = {
    'ab': 'abstract',
    'an': 'news',
    'ax': 'addendum',
    'co': 'article-commentary',
    'cr': 'case-report',
    'ct': 'research-article',
    'ed': 'editorial',
    'er': 'correction',
    'in': 'editorial',
    'le': 'letter',
    'mt': 'research-article',
    'nd': 'undefined',
    'oa': 'research-article',
    'pr': 'press-release',
    'pv': 'editorial',
    'rc': 'book-review',
    'rn': 'brief-report',
    'ra': 'review-article',
    'sc': 'rapid-communication',
    'tr': 'research-article',
    'up': 'undefined'
}

periodicity = {
    u'M': u'Monthly',
    u'B': u'Bimonthly (every two months)',
    u'Q': u'Quarterly',
    u'T': u'Three times a year',
    u'F': u'Semiannual (twice a year)',
    u'A': u'Annual',
    u'K': u'Irregular (know to be so)',
    u'Z': u'Other frequencies'
}

periodicity_in_months = {
    u'M': u'12',
    u'B': u'6',
    u'Q': u'4',
    u'T': u'3',
    u'F': u'2',
    u'A': u'1',
    u'K': u'undefined',
    u'Z': u'undefined'
}

collections = {
    'scl': ['Brazil', 'www.scielo.br'],
    'arg': ['Argentina', 'www.scielo.org.ar'],
    'cub': ['Cuba', 'scielo.sld.cu'],
    'esp': ['Spain', 'scielo.isciii.es'],
    'col': ['Colombia', 'www.scielo.org.co'],
    'sss': ['Social Sciences', 'socialsciences.scielo.org'],
    'spa': ['Public Health', 'www.scielosp.org'],
    'mex': ['Mexico', 'www.scielo.org.mx'],
    'prt': ['Portugal', 'www.scielo.mec.pt'],
    'cri': ['Costa Rica', 'www.scielo.sa.cr'],
    'ven': ['Venezuela', 'www.scielo.org.ve'],
    'ury': ['Uruguay', 'www.scielo.edu.uy'],
    'per': ['Peru', 'www.scielo.org.pe'],
    'chl': ['Chile', 'www.scielo.cl'],
    'sza': ['South Africa', 'www.scielo.org.za'],
    'bol': ['Bolivia', 'www.scielo.org.bo'],
    'pry': ['Paraguay', 'scielo.iics.una.py'],
    'psi': ['PEPSIC', 'pepsic.bvsalud.org'],
    'ppg': ['PPEGEO', 'ppegeo.igc.usp.br'],
    'rve': ['RevOdonto', 'revodonto.bvsalud.org'],
    'edc': ['Educa', 'educa.fcc.org.br'],
    'inv': [u'Inovação', 'inovacao.scielo.br'],
    'cic': [u'Ciência e Cultura', 'cienciaecultura.bvs.br'],
    'cci': [u'ComCiência', 'comciencia.scielo.br'],
    'wid': ['West Indians', 'caribbean.scielo.org'],
    'pro': ['Proceedings', 'www.proceedings.scielo.br']
}

not_collections = ['sss','spa','psi','ppg','rve','edc','inv','cic','cci','wid','pro']

journal_status = {
    'c': u'current',
    'd': u'deceased',
    '?': u'inprogress',
    'p': u'inprogress',
    's': u'suspended'
}

journal_standard = {
    u'iso690': u'iso 690/87 - international standard',
    u'nbr6023': u'nbr 6023/89 - associação nacional',
    u'other': u'other standard',
    u'vancouv': u'the vancouver group - uniform',
    u'apa': u'American Psychological Association'
}

journal_ctrl_vocabulary = {
    u'decs': u'Health Sciences Descriptors',
    u'nd': u'No Descriptor'
}

journal_publication_level = {
    u'DI': u'Divulgation',
    u'CT': u'Scientific Technical'
}

journal_title_category = {
    u'paralleltitle': u'Parallel Title',
    u'other': u'Other',
    u'abbrev_scopus': u'Scopus (abbreviated)',
    u'abbrev_wos': u'Web of Science (abbreviated)',
    u'abbrev_nlm': u'National Library of Medicine (abbreviated)',
}

country_collections = {
                    'chl':'CHL',
                    'scl':'BRA',
                    'arg':'ARG',
                    'esp':'ESP',
                    'col':'COL',
                    'mex':'MEX',
                    'prt':'PRT',
                    'cri':'CRI',
                    'ven':'VEN',
                    'ury':'URY',
                    'per':'PER',
                    'sza':'ZAF',
                    'bol':'BOL',
                    'pry':'PRY',
                    'cub':'CUB'
                    }

country_code_inv = {"AF":"AFG","AL":"ALB","DZ":"DZA","AS":"ASM","AD":"AND","AO":"AGO","AI":"AIA","AQ":"ATA","AG":"ATG","AR":"ARG","AM":"ARM","AW":"ABW","AU":"AUS","AT":"AUT","AZ":"AZE","BS":"BHS","BH":"BHR","BD":"BGD","BB":"BRB","BY":"BLR","BE":"BEL","BZ":"BLZ","BJ":"BEN","BM":"BMU","BT":"BTN","BO":"BOL","BQ":"BES","BA":"BIH","BW":"BWA","BV":"BVT","BR":"BRA","IO":"IOT","BN":"BRN","BG":"BGR","BF":"BFA","BI":"BDI","KH":"KHM","CM":"CMR","CA":"CAN","CV":"CPV","KY":"CYM","CF":"CAF","TD":"TCD","CL":"CHL","CN":"CHN","CX":"CXR","CC":"CCK","CO":"COL","KM":"COM","CG":"COG","CD":"COD","CK":"COK","CR":"CRI","HR":"HRV","CU":"CUB","CW":"CUW","CY":"CYP","CZ":"CZE","CI":"CIV","DK":"DNK","DJ":"DJI","DM":"DMA","DO":"DOM","EC":"ECU","EG":"EGY","SV":"SLV","GQ":"GNQ","ER":"ERI","EE":"EST","ET":"ETH","FK":"FLK","FO":"FRO","FJ":"FJI","FI":"FIN","FR":"FRA","GF":"GUF","PF":"PYF","TF":"ATF","GA":"GAB","GM":"GMB","GE":"GEO","DE":"DEU","GH":"GHA","GI":"GIB","GR":"GRC","GL":"GRL","GD":"GRD","GP":"GLP","GU":"GUM","GT":"GTM","GG":"GGY","GN":"GIN","GW":"GNB","GY":"GUY","HT":"HTI","HM":"HMD","VA":"VAT","HN":"HND","HK":"HKG","HU":"HUN","IS":"ISL","IN":"IND","ID":"IDN","IR":"IRN","IQ":"IRQ","IE":"IRL","IM":"IMN","IL":"ISR","IT":"ITA","JM":"JAM","JP":"JPN","JE":"JEY","JO":"JOR","KZ":"KAZ","KE":"KEN","KI":"KIR","KP":"PRK","KR":"KOR","KW":"KWT","KG":"KGZ","LA":"LAO","LV":"LVA","LB":"LBN","LS":"LSO","LR":"LBR","LY":"LBY","LI":"LIE","LT":"LTU","LU":"LUX","MO":"MAC","MK":"MKD","MG":"MDG","MW":"MWI","MY":"MYS","MV":"MDV","ML":"MLI","MT":"MLT","MH":"MHL","MQ":"MTQ","MR":"MRT","MU":"MUS","YT":"MYT","MX":"MEX","FM":"FSM","MD":"MDA","MC":"MCO","MN":"MNG","ME":"MNE","MS":"MSR","MA":"MAR","MZ":"MOZ","MM":"MMR","NA":"NAM","NR":"NRU","NP":"NPL","NL":"NLD","NC":"NCL","NZ":"NZL","NI":"NIC","NE":"NER","NG":"NGA","NU":"NIU","NF":"NFK","MP":"MNP","NO":"NOR","OM":"OMN","PK":"PAK","PW":"PLW","PS":"PSE","PA":"PAN","PG":"PNG","PY":"PRY","PE":"PER","PH":"PHL","PN":"PCN","PL":"POL","PT":"PRT","PR":"PRI","QA":"QAT","RO":"ROU","RU":"RUS","RW":"RWA","RE":"REU","BL":"BLM","SH":"SHN","KN":"KNA","LC":"LCA","MF":"MAF","PM":"SPM","VC":"VCT","WS":"WSM","SM":"SMR","ST":"STP","SA":"SAU","SN":"SEN","RS":"SRB","SC":"SYC","SL":"SLE","SG":"SGP","SX":"SXM","SK":"SVK","SI":"SVN","SB":"SLB","SO":"SOM","ZA":"ZAF","GS":"SGS","SS":"SSD","ES":"ESP","LK":"LKA","SD":"SDN","SR":"SUR","SJ":"SJM","SZ":"SWZ","SE":"SWE","CH":"CHE","SY":"SYR","TW":"TWN","TJ":"TJK","TZ":"TZA","TH":"THA","TL":"TLS","TG":"TGO","TK":"TKL","TO":"TON","TT":"TTO","TN":"TUN","TR":"TUR","TM":"TKM","TC":"TCA","TV":"TUV","UG":"UGA","UA":"UKR","AE":"ARE","GB":"GBR","US":"USA","UM":"UMI","UY":"URY","UZ":"UZB","VU":"VUT","VE":"VEN","VN":"VNM","VG":"VGB","VI":"VIR","WF":"WLF","EH":"ESH","YE":"YEM","ZM":"ZMB","ZW":"ZWE","AX":"ALA"}
country_code_rev = {"AFG":"AF","ALB":"AL","DZA":"DZ","ASM":"AS","AND":"AD","AGO":"AO","AIA":"AI","ATA":"AQ","ATG":"AG","ARG":"AR","ARM":"AM","ABW":"AW","AUS":"AU","AUT":"AT","AZE":"AZ","BHS":"BS","BHR":"BH","BGD":"BD","BRB":"BB","BLR":"BY","BEL":"BE","BLZ":"BZ","BEN":"BJ","BMU":"BM","BTN":"BT","BOL":"BO","BES":"BQ","BIH":"BA","BWA":"BW","BVT":"BV","BRA":"BR","IOT":"IO","BRN":"BN","BGR":"BG","BFA":"BF","BDI":"BI","KHM":"KH","CMR":"CM","CAN":"CA","CPV":"CV","CYM":"KY","CAF":"CF","TCD":"TD","CHL":"CL","CHN":"CN","CXR":"CX","CCK":"CC","COL":"CO","COM":"KM","COG":"CG","COD":"CD","COK":"CK","CRI":"CR","HRV":"HR","CUB":"CU","CUW":"CW","CYP":"CY","CZE":"CZ","CIV":"CI","DNK":"DK","DJI":"DJ","DMA":"DM","DOM":"DO","ECU":"EC","EGY":"EG","SLV":"SV","GNQ":"GQ","ERI":"ER","EST":"EE","ETH":"ET","FLK":"FK","FRO":"FO","FJI":"FJ","FIN":"FI","FRA":"FR","GUF":"GF","PYF":"PF","ATF":"TF","GAB":"GA","GMB":"GM","GEO":"GE","DEU":"DE","GHA":"GH","GIB":"GI","GRC":"GR","GRL":"GL","GRD":"GD","GLP":"GP","GUM":"GU","GTM":"GT","GGY":"GG","GIN":"GN","GNB":"GW","GUY":"GY","HTI":"HT","HMD":"HM","VAT":"VA","HND":"HN","HKG":"HK","HUN":"HU","ISL":"IS","IND":"IN","IDN":"ID","IRN":"IR","IRQ":"IQ","IRL":"IE","IMN":"IM","ISR":"IL","ITA":"IT","JAM":"JM","JPN":"JP","JEY":"JE","JOR":"JO","KAZ":"KZ","KEN":"KE","KIR":"KI","PRK":"KP","KOR":"KR","KWT":"KW","KGZ":"KG","LAO":"LA","LVA":"LV","LBN":"LB","LSO":"LS","LBR":"LR","LBY":"LY","LIE":"LI","LTU":"LT","LUX":"LU","MAC":"MO","MKD":"MK","MDG":"MG","MWI":"MW","MYS":"MY","MDV":"MV","MLI":"ML","MLT":"MT","MHL":"MH","MTQ":"MQ","MRT":"MR","MUS":"MU","MYT":"YT","MEX":"MX","FSM":"FM","MDA":"MD","MCO":"MC","MNG":"MN","MNE":"ME","MSR":"MS","MAR":"MA","MOZ":"MZ","MMR":"MM","NAM":"NA","NRU":"NR","NPL":"NP","NLD":"NL","NCL":"NC","NZL":"NZ","NIC":"NI","NER":"NE","NGA":"NG","NIU":"NU","NFK":"NF","MNP":"MP","NOR":"NO","OMN":"OM","PAK":"PK","PLW":"PW","PSE":"PS","PAN":"PA","PNG":"PG","PRY":"PY","PER":"PE","PHL":"PH","PCN":"PN","POL":"PL","PRT":"PT","PRI":"PR","QAT":"QA","ROU":"RO","RUS":"RU","RWA":"RW","REU":"RE","BLM":"BL","SHN":"SH","KNA":"KN","LCA":"LC","MAF":"MF","SPM":"PM","VCT":"VC","WSM":"WS","SMR":"SM","STP":"ST","SAU":"SA","SEN":"SN","SRB":"RS","SYC":"SC","SLE":"SL","SGP":"SG","SXM":"SX","SVK":"SK","SVN":"SI","SLB":"SB","SOM":"SO","ZAF":"ZA","SGS":"GS","SSD":"SS","ESP":"ES","LKA":"LK","SDN":"SD","SUR":"SR","SJM":"SJ","SWZ":"SZ","SWE":"SE","CHE":"CH","SYR":"SY","TWN":"TW","TJK":"TJ","TZA":"TZ","THA":"TH","TLS":"TL","TGO":"TG","TKL":"TK","TON":"TO","TTO":"TT","TUN":"TN","TUR":"TR","TKM":"TM","TCA":"TC","TUV":"TV","UGA":"UG","UKR":"UA","ARE":"AE","GBR":"GB","USA":"US","UMI":"UM","URY":"UY","UZB":"UZ","VUT":"VU","VEN":"VE","VNM":"VN","VGB":"VG","VIR":"VI","WLF":"WF","ESH":"EH","YEM":"YE","ZMB":"ZM","ZWE":"ZW","ALA":"AX"}

worldmap_code = {"ATG":"naatg","DZA":"afdza","AZE":"asaze","ALB":"eualb","ARM":"asarm","AGO":"afago","ASM":"ocasm","ARG":"saarg","AUS":"ocaus","BHR":"asbhr","BRB":"nabrb","BMU":"nabmu","BHS":"nabhs","BGD":"asbgd","BLZ":"nablz","BIH":"eubih","BOL":"sabol","MMR":"asmmr","BEN":"afben","SLB":"ocslb","BRA":"sabra","BGR":"eubgr","BRN":"asbrn","CAN":"nacan","KHM":"askhm","LKA":"aslka","COG":"afcog","COD":"afcod","BDI":"afbdi","CHN":"aschn","AFG":"asafg","BTN":"asbtn","CHL":"sachl","CYM":"nacym","CMR":"afcmr","TCD":"aftcd","COM":"afcom","COL":"sacol","CRI":"nacri","CAF":"afcaf","CUB":"nacub","CPV":"afcpv","COK":"occok","CYP":"ascyp","DNK":"eudnk","DJI":"afdji","DMA":"nadma","DOM":"nadom","ECU":"saecu","EGY":"afegy","IRL":"euirl","GNQ":"afgnq","EST":"euest","ERI":"aferi","SLV":"naslv","ETH":"afeth","AUT":"euaut","CZE":"eucze","GUF":"saguf","FIN":"eufin","FJI":"ocfji","FLK":"saflk","FSM":"ocfsm","PYF":"ocpyf","FRA":"eufra","GMB":"afgmb","GAB":"afgab","GEO":"asgeo","GHA":"afgha","GRD":"nagrd","GRL":"nagrl","DEU":"eudeu","GUM":"ocgum","GRC":"eugrc","GTM":"nagtm","GIN":"afgin","GUY":"saguy","HTI":"nahti","HND":"nahnd","HRV":"euhrv","HUN":"euhun","ISL":"euisl","IND":"asind","IRN":"asirn","ISR":"asisr","ITA":"euita","CIV":"afciv","IRQ":"asirq","JPN":"asjpn","JAM":"najam","JOR":"asjor","KEN":"afken","KGZ":"askgz","PRK":"asprk","KIR":"ockir","KOR":"askor","KWT":"askwt","KAZ":"askaz","LAO":"aslao","LBN":"aslbn","LVA":"eulva","BLR":"eublr","LTU":"eultu","LBR":"aflbr","SVK":"eusvk","LIE":"eulie","LBY":"aflby","MDG":"afmdg","MTQ":"namtq","MNG":"asmng","MSR":"namsr","MKD":"eumkd","MLI":"afmli","MAR":"afmar","MUS":"afmus","MRT":"afmrt","MLT":"eumlt","OMN":"asomn","MDV":"asmdv","MEX":"namex","MYS":"asmys","MOZ":"afmoz","MWI":"afmwi","NCL":"ocncl","NIU":"ocniu","NER":"afner","ABW":"naabw","AIA":"naaia","BEL":"eubel","HKG":"ashkg","MNP":"ocmnp","FRO":"eufro","AND":"euand","GIB":"eugib","IMN":"euimn","LUX":"eulux","MAC":"asmac","MCO":"eumco","PSE":"aspse","MNE":"eumne","MYT":"afmyt","ALA":"ALA","NFK":"ocnfk","CCK":"ascck","ATA":"anata","BVT":"anbvt","ATF":"anatf","HMD":"anhmd","IOT":"asiot","CXR":"ascxr","UMI":"naumi","VUT":"ocvut","NGA":"afnga","NLD":"eunld","NOR":"eunor","NPL":"asnpl","NRU":"ocnru","SUR":"sasur","NIC":"nanic","NZL":"ocnzl","PRY":"sapry","PER":"saper","PAK":"aspak","POL":"eupol","PAN":"napan","PRT":"euprt","PNG":"ocpng","GNB":"afgnb","QAT":"asqat","REU":"afreu","ROU":"eurou","MDA":"eumda","PHL":"asphl","PRI":"napri","RUS":"asrus","RWA":"afrwa","SAU":"assau","KNA":"nakna","SYC":"afsyc","ZAF":"afzaf","LSO":"aflso","BWA":"afbwa","SEN":"afsen","SVN":"eusvn","SLE":"afsle","SGP":"assgp","SOM":"afsom","ESP":"euesp","LCA":"nalca","SDN":"afsdn","SWE":"euswe","SYR":"assyr","CHE":"euche","TTO":"natto","THA":"astha","TJK":"astjk","TKL":"octkl","TON":"octon","TGO":"aftgo","STP":"afstp","TUN":"aftun","TUR":"astur","TUV":"octuv","TKM":"astkm","TZA":"aftza","UGA":"afuga","GBR":"eugbr","UKR":"euukr","USA":"nausa","BFA":"afbfa","URY":"saury","UZB":"asuzb","VCT":"navct","VEN":"saven","VGB":"navgb","VNM":"asvnm","VIR":"navir","NAM":"afnam","WLF":"ocwlf","WSM":"ocwsm","SWZ":"afswz","YEM":"asyem","ZMB":"afzmb","ZWE":"afzwe","IDN":"asidn","GLP":"ocglp","ANT":"naant","ARE":"asare","TLS":"astls","PCN":"ocpcn","PLW":"ocplw","MHL":"ocmhl","SPM":"naspm","SHN":"afshn","SMR":"eusmr","TCA":"natca","ESH":"afesh","SRB":"eusrb","SJM":"eusjm","MAF":"namaf","BLM":"BLM","GGY":"GGY","JEY":"JEY","SGS":"ansgs","TWN":"astwn"}