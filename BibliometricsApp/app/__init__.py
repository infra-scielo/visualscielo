#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Import flask and template operators
from flask import Flask, Blueprint, render_template,\
                  session, redirect, url_for,\
                  request, jsonify, make_response,\
                  flash
from flask_compress import Compress
from flask_babel import Babel, refresh

# Define the WSGI application object
app = Flask(__name__)
Compress(app)
# Configurations
app.config.from_object('config')
#Babel
babel = Babel(app)


#VERSION 1
#-------------------------------------------------------------------------------------------
# Import a module / component using its blueprint handler variable (mod_xyz)
from app.v1.controllers.indexController import mod_index as index_module
app.register_blueprint(index_module)
from app.v1.controllers.documentController import mod_document as document_module
app.register_blueprint(document_module)
from app.v1.controllers.relationshipController import mod_relationship as relationship_module
app.register_blueprint(relationship_module)
from app.v1.controllers.exporterController import mod_export as export_module
app.register_blueprint(export_module)
from app.v1.controllers.testController import mod_test as test_module
app.register_blueprint(test_module)
#-------------------------------------------------------------------------------------------

#VERSION 2
#-------------------------------------------------------------------------------------------
# Import a module / component using its blueprint handler variable (mod_xyz)
from app.v2.controllers.indexController import mod_index as index_v2_module
app.register_blueprint(index_v2_module)
from app.v2.controllers.documentController import mod_document as document_v2_module
app.register_blueprint(document_v2_module)
from app.v2.controllers.relationshipController import mod_relationship as relationship_v2_module
app.register_blueprint(relationship_v2_module)
from app.v2.controllers.exporterController import mod_export as export_v2_module
app.register_blueprint(export_v2_module)
#-------------------------------------------------------------------------------------------

#VERSION 3
#-------------------------------------------------------------------------------------------
# Import a module / component using its blueprint handler variable (mod_xyz)
from app.v3.controllers.indexController import mod_index as index_v3_module
app.register_blueprint(index_v3_module)
from app.v3.controllers.documentController import mod_document as document_v3_module
app.register_blueprint(document_v3_module)
from app.v3.controllers.journalController import mod_journal as journal_v3_module
app.register_blueprint(journal_v3_module)
from app.v3.controllers.relationshipController import mod_relationship as relationship_v3_module
app.register_blueprint(relationship_v3_module)
from app.v3.controllers.exporterController import mod_export as export_v3_module
app.register_blueprint(export_v3_module)
#-------------------------------------------------------------------------------------------

# Error handling
@app.errorhandler(404)
def not_found(error):
    return render_template('404.html'), 404
@app.errorhandler(500)
def internal_error(error):
    return render_template('500.html'), 500


#Babel local selector
LANGUAGES = {
    'en': 'English',
    'es': 'Español',
    'pt': "Português"
}

@babel.localeselector
def get_locale():
    if not session.has_key('lang'):
        session['lang'] = request.accept_languages.best_match(LANGUAGES.keys())
    if request.args.get('lang'):
        session['lang'] = request.args.get('lang')
    elif request.form.get('lang'):
        session['lang'] = request.form.get('lang')
    if session['lang'] is None:
        session['lang'] = 'en'
    return session.get('lang')