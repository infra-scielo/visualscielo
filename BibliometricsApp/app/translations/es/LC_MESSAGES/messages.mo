��    y      �              �     �  
   �  
   �  	   �     �     �     �     �     �            <   -     j     }     �     �     �     �     �     �     �     �     �  
   	     	     "	  �   *	  n   �	     <
     D
     I
     R
     [
     i
     ~
  	   �
     �
     �
  	   �
     �
     �
     �
               .     H  
   M     X     h     p     u     �     �     �     �     �     �     �     �     �     �  g        m  �   �     G  
   K     V     i     �     �     �     �     �  
   �  ;   �  R     V   _     �  *   �     �               +     9     H     c     v     �     �     �     �  	   �     �  F   �       	   %  
   /  <   :  P   w  /  �  -  �     &     4     9     <  �   B  �   �     c  	   h     r     �  	   �     �     �     �  
   �     �     �     �     �  �  �  
     
   �     �  	   �     �     �     �     �     �     �     �  F        J     _     h     }     �     �     �     �     �     �     �  
   �  	          �     |   �     U     ]  	   c  	   m     w     �     �  
   �     �  %   �  	   
          !     )     I     \     x     �     �     �  	   �     �     �     �     �     �     �               !     >     E     \  �   b     �  �        �     �     �  #   �  !   
     ,     ?     A     J  
   O  @   Z  ]   �  f   �     `  /   w     �  	   �     �     �     �                5     I     `     h          �     �  Q   �        
            <   #   [   `   �   �   :  {!     �"     �"     �"     �"  ~   �"  �   U#     $  
   $     $     &$  
   C$     N$     b$     j$     o$     ~$     �$     �$     �$   # Documents # Journals #Documents #Journals AND Abstract Accept Active screen Agricultural Sciences All Applied Social Sciences Are you sure you want to <b>delete</b> saved visualizations? Article Commentary At least Author's Country Biological Sciences Book Review Brief Report Cancel Case Report Citable Documents Type Close Co-authors from </th>     Collection Continue Country Country color represents <b class='scope-tabulation'></b>. Mouse over a country additional information is shown. Mouse over a country display number of documents. Country color represents <b class='scope-tabulation'></b>. Mouse over on a country for additional information. Current Date Deceased Document Document Type Document Type Detail Document Type Details Documents Documents by Language Documents by Thematic Area Editorial Engineering English Exact and Earth Sciences Filter Selected Frecuency (link weight) Frequency of Relationship From Go to Home Health Sciences History Home Human Sciences ID In Progress Journal Journal Status Journals Journals by Language Journals by Thematic Area Language Last Visualization Letter Linechart represents <b class='scope-tabulation'></b> in each year. Click on a line to filter the data. Linguistics, Letters and Arts Network-Ring shows the connections between <b class='scope-tabulation'></b>, focused on 1 node (selected) in the network. It displays primary and secondary connections of a specific node. New New Search Next Visualization No visualization in history Number of Author's Countries Number of Authors OR Options Other Portuguese Press <b>Cancel</b> to return to the current visualization. Press <b>Continue</b> to start new search, without save the current visualization. Press <b>Save and Continue</b> to save the current visualization and start new search. Previous Visualization Provided number of co-author relationships Rapid Communication Research Article Reset History Reset filters Review Article Save Current Visualization Save Visualization Save and Continue SciELO Thematic Area Scope Search history Selected Filters Show Data Show all Sorry, your browser does not support Web Storage for this funcionality Spanish Suspended Tabulation The nationalities of the authors are marked with <b>👤</b> The visualization shows the coauthor relationships between authors of documents. The visualization shows the coauthor relationships between authors of documents.
                        When the mouse is hovering over the country, links between countries appear. These links represents coauthor relationships between authors of the selected country and authors of the other countries. The visualization shows the coauthor relationships between authors of documents.
                      When the mouse is hovering over the country, links between countries appear. These links represents coauthor relationships between authors of the selected country and authors of the other countries. Thematic Area Time To Total Treemap distributes <b class='scope-tabulation'></b>. Click on a treemap box to filter the data of the country per year of publication. Treemap distributes <b class='scope-tabulation'></b>. Mouse over on a treemap box for additional information. Click on a treemap box to filter the data. Type Undefined Visualization Visualization History Visualize WOS Thematic Area Working<br>Process Year Year Range new remove removed selected Project-Id-Version: PROJECT VERSION
Report-Msgid-Bugs-To: EMAIL@ADDRESS
POT-Creation-Date: 2017-04-13 14:01-0300
PO-Revision-Date: 2017-01-18 11:24-0300
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language: es
Language-Team: es <LL@li.org>
Plural-Forms: nplurals=2; plural=(n != 1)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.3.4
 Documentos # Journals #Documentos #Revistas Y Resumen Aceptar Pantalla Activa Ciencias Agrícolas Todos Ciencias Sociales Aplicadas ¿Está seguro que quiere <b>borrar</b> las visualizaciones guardadas? Artículo-comentario Al menos País de los autores Ciencias Biológicas Reseña de Libro Informe Breve Cancelar Informe de Caso Documentos Citables Cerrar País de Co-autores Colección Continuar País Color de los países representa <b class='scope-tabulation'></b>. Pasando el cursor sobre el país se muestra información adicional. Los datos pueden ser filtrados haciendo click en los países. Color de los países representa <b class='scope-tabulation'></b>.Pase el cursor sobre el país para información adicional.  Vigente Fecha Terminada Documento Tipo de Documento Tipo de Documento (detalle) Tipo de Documento (Detalle) Documentos Documentos por Lenguaje Documentos por Área Temática SciELO Editorial Ingenierías Inglés Ciencias Exactas y de la Tierra Filtrar Selección Frecuencia (peso de enlace) Frecuencia de Relación Desde Ir al inicio Ciencias de la Salud Historial Inicio Humanidades ID En progreso Revista Estatus de la Revista Revistas Revistas por Lenguaje Revistas por Área Temática Idioma Última Visualización Carta El gráfico de líneas muestra <b class='scope-tabulation'></b> en cada año. Los datos pueden ser filtrados haciendo click en las líneas. Lingüística, Letras y Artes Las Redes Anillo muestran las conexiones entre <b class='scope-tabulation'></b>. Haciendo click en un nodo, este se centra mostrando sus conexiones primarias y secundarias. New Nueva Búsqueda Siguiente Visualización Sin visualizaciones en el historial Número de países de los autores Número de Autores O Opciones Otro Portugués Presiona <b>Cancelar</b> para volver a la visualización actual. Presiona <b>Continuar</b> para ir a la nueva búsqueda, sin guardar la visualización actual. Presiona <b>Guardar y Continuar</b> para guardar la visualización actual y hacer una nueva búsqueda. Visualiación Anterior Número de relaciones de co-autoría provistas. Comunicación Rápida Artículo Limpiar Historial Limpiar filtros Artículo de Revisión Guardar Visualización Actual Guardar Visualización Guardar y Continuar Área Temática SciELO Ámbito Historial de búsqueda Filtros Seleccionados Mostrar Datos Mostrar todos Lo sentimos, su navegador no soporta Web Storage para utilizar esta funcionalidad Español Suspendida Tabulación The nationalities of the authors are marked with <b>👤</b> La visualización muestra las relaciones de coautoría entre los autores de los documentos. La visualización muestra las relaciones de coautoría entre los autores de los documentos según país de afiliación. Pasando el cursor sobre el país, se muestran vínculos entre países. La visualización muestra la relación de coautoría entre los autores de los documentos.
                      Al posicíonar el mouse sobre un país, aparecerán enlaces entre países. Estos enlaces representan las relaciones de coautoría entre los autores del país seleccionado y autores de los otros países. Area Temática Hora Hasta Total Treemap distribuye <b class='scope-tabulation'></b>. Los datos pueden ser filtrados haciendo click en los cuadros del treemap. El Treemap distribuye  <b class='scope-tabulation'></b>. Pase el cursor sobre una caja del Treemap para información adicional. Haga click sobre una caja para filtrar los datos. Tipo Indefinido Visualización Historial de Visualizaciones Visualizar Área Temática WOS Proceso Año Rango de Años nuevo eliminar removido seleccionado 