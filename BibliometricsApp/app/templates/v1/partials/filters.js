<script type="text/javascript">
	//VISTYPE
	$('#vistype-select').val({{ filters.vistype }});


  //YEARS
  current_year = new Date().getFullYear();
  for(y = parseInt(current_year); y >= 1900; y--){
    //$('#year-select').append($('<option>', {value:y, text: y}));          
    $('#year-select-from').append($('<option>', {value:y, text: y}));         
    $('#year-select-to').append($('<option>', {value:y, text: y}));         
  }
  $('#year-select').val('{{ filters.years }}');
  {% if filters.years == 'RANGE' %}
    $('#year-select-from').val({{ filters.from_year }});
    $('#year-select-to').val({{ filters.to_year }});
  {% endif %}

  //STUDY UNIT
  $('#study-unit-select').val('{{ filters.study_unit }}');

  //TEMATIC AREAS
  thematic_areas = filtersJson['thematicAreas'];
  for(i in thematic_areas){
    $('#thematic-area-select').append($('<option>', {value: thematic_areas[i], text: thematic_areas[i]}));
  }
  {% for ta in filters.thematic_areas %}
  	$("#thematic-area-select option[value='{{ ta }}']").prop('selected', true);
  {% endfor%}

  //WOS TEMATIC AREAS
  wos_thematic_areas = filtersJson['wosThematicAreas']
  for(i in wos_thematic_areas){
    $('#wos-thematic-area-select').append($('<option>', {value: wos_thematic_areas[i], text: wos_thematic_areas[i]}));
  }
  {% for wta in filters.wos_thematic_areas %}
  	$("#wos-thematic-area-select option[value='{{ wta }}']").prop('selected', true);
  {% endfor%}

  //COLLECTIONS
  collections = filtersJson['collections']
  for(i in collections){
    $('#collections-select').append($('<option>', {value: collections[i][0], text: collections[i][1]}));
  }
  {% for c in filters.collections %}
  	$("#collections-select option[value={{ c }}]").prop('selected', true);
  {% endfor%}

  //JOURNALS
  journals = filtersJson['journals']
  for(i in journals){
    $('#journal-select').append($('<option>', {value: journals[i], text: journals[i]}));
  }
  {% for j in filters.journals %}
  	$("#journal-select option[value='{{ j }}']").prop('selected', true);
  {% endfor%}

  //
  $("#document-type-select").val('{{ filters.document_type }}')
  //DOCUMENT TYPE DETAILS
  document_types = filtersJson['documentTypes']
  for(i in document_types){
    $('#document-type-detail-select').append($('<option>', {value: document_types[i], text: document_types[i]}));
  }
  {% for dtd in filters.document_type_details %}
  	$("#document-type-detail-select option[value={{ dtd }}]").prop('selected', true);
  {% endfor%}

  //LANGUAGES
  languages = filtersJson['languages']
  for(i in languages){
    $('#language-select').append($('<option>', {value: languages[i][0], text: languages[i][1]}));
  }
  {% for l in filters.languages %}
  	$("#language-select option[value={{ l }}]").prop('selected', true);
  {% endfor%}

  /* Mostrar/Ocultar panel de filtros */
  $('#filter-laucher').on('click', function(){
    $('#filter-panel').slideToggle("slow");
  });
  $('#filter-btn-panel').on('click', function(){
    $('#filter-panel').slideToggle("slow");
  });

  
  /*Operaciones selector de años*/
  /*----------------------------*/
  $('#year-select').on('change', function(){
    year($(this).val());
  });

  function year(value){
    $('.year-option').hide();
    if(value == "RANGE"){
      $('#year-range-container').show();
    }else{
      $('#year-select-from option:selected').removeAttr("selected");
      $('#year-select-to option:selected').removeAttr("selected");
    }
    $('select').material_select();
  }
  //Mantener concordancia de los años
  $('#year-select-from').on('change', function(){
    if( $('#year-select-to').val() < $(this).val()){
      $('#year-select-to').val($(this).val());
      $('select').material_select();
    }
  });
  $('#year-select-to').on('change', function(){
    if( $('#year-select-from').val() > $(this).val()){
      $('#year-select-from').val($(this).val());
      $('select').material_select();
    }
  });
  /* END Operaciones selector de años*/

  /*Operaciones unidades de estudio*/
  /*-------------------------------*/
  $('#study-unit-select').on('change', function(){
    study_unit($(this).val());
  });
  function study_unit(value){
    $('.study-unit-option').hide();
    if(value == 1){
      $('#collections-select-container').show();
      $('#journal-select option:selected').removeAttr("selected");
      $('#document-type-select option:selected').removeAttr("selected");
      $('#document-type-detail-select option:selected').removeAttr("selected");
    }else if(value == 2){
      $('#journal-select-container').show();
      $('#collections-select option:selected').removeAttr("selected");
      $('#document-type-select option:selected').removeAttr("selected");
      $('#document-type-detail-select option:selected').removeAttr("selected");
    }else{
      $('#document-select-container').show();
      $('#journal-select option:selected').removeAttr("selected");
      $('#collections-select option:selected').removeAttr("selected");
    }
    $('select').material_select();
  }

  //Materialize
  $('select').material_select();

  //Valores desde filtros
  year($('#year-select').val());
  study_unit($('#study-unit-select').val())
</script>