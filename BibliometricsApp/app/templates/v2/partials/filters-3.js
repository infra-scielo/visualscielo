<script type="text/javascript">
  var JSON_CURRENT_FILTERS = {{ filters.serialize|tojson }}
  var LANG = "{{ filters.lang|safe }}"

  if (typeof(Storage) !== "undefined") {
    if(localStorage["history"] !== undefined){
      $("#historical-id").text("#"+localStorage["history"]);
      $('.scope-tabulation-fe').html(stFilters["{{filters.scope}}"]["text"][LANG]+" "+stFilters["{{filters.scope}}"]["tabulations"]["{{filters.tabulation}}"]["text"][LANG]);
    }
  }
  /*FIRST BLOCK FILTERS*/
	//SCOPE
  scope();
  $('#scope-select').val({{ filters.scope }});
  $('#filters-form').attr('action', stFilters[{{ filters.scope }}]["endpoint"]);
  $('#filters-form-url').val(stFilters[{{ filters.scope }}]["endpoint"]);
  $('#advanced-filters-form').attr('action', stFilters[{{ filters.scope }}]["endpoint"]);
  $('#advanced-filters-form-url').val(stFilters[{{ filters.scope }}]["endpoint"]);
  $('#advanced-filters-form-url-aux').val(stFilters[{{ filters.scope }}]["endpoint"]);

  //Tabulations
  scope_tabulations()
  $('#scope-tabulations-select').val("{{ filters.tabulation }}");

  //Visualizations
  scope_tabulations_visualizations();
  $('#scope-tabulation-visualizations-select').val("{{ filters.visualization }}");

  //FUNCTIONS
  //CHANGE SCOPE
  $('#scope-select').on('change', function(){
    scope = $('#scope-select').val();
    //Change form action
    $('#filters-form').attr('action', stFilters[scope]["endpoint"]);
    $('#filters-form-url').val(stFilters[scope]["endpoint"]);
    //Update selects
    scope_tabulations();
    scope_tabulations_visualizations();
    $('select').material_select();
  });

  //CHANGE TABULATION
  $('#scope-tabulations-select').on('change', function(){
    scope = $('#scope-select').val();
    tabulation = $('#scope-tabulations-select').val();
    //Update selects
    scope_tabulations_visualizations();
    $('select').material_select();

    //Visualization
    $('#scope-tabulations-select').val() == "{{ filters.tabulation }}" ? $("#vis-label").addClass("parpadea") : $("#vis-label").removeClass("parpadea");
  });

  function scope(){
    $('#scope-select').find('option').remove().end();
    for(var scope in stFilters){
      $('#scope-select').append($('<option>', {value:scope, text: stFilters[scope]["text"][LANG]}))
    }
    //Change form action
    scope = $('#scope-select').val();
    $('#filters-form').attr('action', stFilters[scope]["endpoint"]);
  }

  function scope_tabulations(){
    scope = $('#scope-select').val();
    $('#scope-tabulations-select').find('option').remove().end();
    for(tabulation in stFilters[scope]["tabulations"]){
      $('#scope-tabulations-select').append($('<option>', {value:tabulation, text: stFilters[scope]["tabulations"][tabulation]["text"][LANG]}))
    }
  }

  function scope_tabulations_visualizations(){
    scope = $('#scope-select').val();
    tabulation = $('#scope-tabulations-select').val();
    $('#scope-tabulation-visualizations-select').find('option').remove().end();
    for(visualization in stFilters[scope]["tabulations"][tabulation]["visualizations"][LANG]){
      $('#scope-tabulation-visualizations-select').append($('<option>', {value: stFilters[scope]["tabulations"][tabulation]["visualizations"][LANG][visualization][0], text: stFilters[scope]["tabulations"][tabulation]["visualizations"][LANG][visualization][1]}))
    }
  }
  /*END FIRST BLOCK FILTERS*/



  //YEARS
  current_year = new Date().getFullYear()
  for(y = parseInt(current_year); y >= 1909; y--){          
    $('#year-select-from').append($('<option>', {value:y, text: y}));         
    $('#year-select-to').append($('<option>', {value:y, text: y}));         
  }
  $('#year-select-from').val({{ filters.from_year }});
  $('#year-select-to').val({{ filters.to_year }});

  //TEMATIC AREAS
  thematic_areas = filtersJson['thematicAreas'];
  filters_ta = {{ filters.thematic_areas|tojson }}.map(function(ta){ return ta[0]});
  console.log(filters_ta);
  prepare_checkboxes(thematic_areas, filters_ta, 1, "#thematic-area-checkboxes", "#af-thematic-area-modal", 4, "thematic_areas[]", "ta", LANG);

  //WOS TEMATIC AREAS
  wos_thematic_areas = filtersJson['wosThematicAreas']
  filters_wta = {{ filters.wos_thematic_areas|tojson }}.map(function(wta){ return wta[0]});
  prepare_checkboxes(wos_thematic_areas, filters_wta, 1, "#wos-thematic-area-checkboxes", "#af-wos-thematic-area-modal", 4, "wos_thematic_areas[]", "wta", LANG);
  
  //COLLECTIONS
  collections = filtersJson['collections']
  filters_c = {{ filters.collections|tojson }}.map(function(c){ return c[0]});
  prepare_checkboxes(collections, filters_c, 1, "#collection-checkboxes", "#af-collections-modal", 4, "collections[]", "coll", LANG);
  
  //JOURNALS
  journals = filtersJson['journals']
  prepare_checkboxes(journals, {{ filters.journals|tojson }}, 0, "#journal-checkboxes", "#af-journals-modal", 4, "journals[]", "jour", LANG);
  
  //
  $("#document-type-select").val('{{ filters.document_type }}')
  //DOCUMENT TYPE DETAILS
  document_types = filtersJson['documentTypes']
  filters_dtd = {{ filters.document_type_details|tojson }}.map(function(dtd){ return dtd[0]});
  prepare_checkboxes(document_types, filters_dtd, 1, "#document-type-detail-checkboxes", "#af-document-type-details-modal", 4, "document_type_details[]", "dtd", LANG);
  
  //LANGUAGES
  languages = filtersJson['languages']
  filters_l = {{ filters.languages|tojson }}.map(function(l){ return l[0]});
  prepare_checkboxes(languages, filters_l, 1, "#language-checkboxes", "#af-languages-modal", 4, "languages[]", "lang", LANG);
  
  //Mantener concordancia de los años
  $('#year-select-from').on('change', function(){
    $('#advanced-filters-form-submit-btn').prop('disabled', false).removeClass('disabled');
    if( $('#year-select-to').val() < $(this).val()){
      $('#year-select-to').val($(this).val());
      $('select').material_select();
    }
  });
  $('#year-select-to').on('change', function(){
    $('#advanced-filters-form-submit-btn').prop('disabled', false).removeClass('disabled');
    if( $('#year-select-from').val() > $(this).val()){
      $('#year-select-from').val($(this).val());
      $('select').material_select();
    }
  });


  //Cambio de visualización desde select
  var scope1_containers = {'map': '#world-map',
                           'treemap': '#treemap-by',
                           'linechart': '#linechart-by'}
  var scope2_containers = {'circular layout': '#circular-layout',
                           'map': '#world-map',
                           'force-direct layout': '#force-direct-layout'}
  function set_exporter_options(){
    var vistype = $("#scope-tabulation-visualizations-select").val();
    if(vistype == "table"){
      $('.print-export-option').hide();
      $('.image-export-option').hide();
      $('.pdf-export-option').hide();
    }else if("{{filters.scope}}" == "1"){
      var container = scope1_containers[vistype];
      $('.print-export-option').show();
      $('.image-export-option').show();
      $('.pdf-export-option').show();
    }else if("{{filters.scope}}" == "2"){
      var container = scope2_containers[vistype];
      $('.print-export-option').show();
      $('.image-export-option').show();
      $('.pdf-export-option').show();
    }
    $('#eo-png').attr('onclick', "svgcrowbar('"+ container +" svg', 'png');");
    $('#eo-jpeg').attr('onclick', "svgcrowbar('"+ container +" svg', 'jpeg');");
    $('#eo-svg').attr('onclick', "svgcrowbar('"+ container +" svg', 'svg');");
    $('#eo-ppt').attr('onclick', "svgcrowbar('"+ container +" svg', 'pptx');"); 
    $('#eo-pdf').attr('onclick', "svgcrowbar('"+ container +" svg', 'pdf');"); 
  }

  set_exporter_options();

  
  $("#scope-tabulation-visualizations-select").on('change', function(){
    $("#vis-label").removeClass("parpadea");
    if($('#scope-tabulations-select').val() == "{{ filters.tabulation }}"){
      $('#visualization-hidden').val($(this).val());
      $('#visualization-hidden-aux').val($(this).val());
      $("#vis-label").addClass("parpadea");
      
      $("#animate-btn").attr('onclick', "change_visualization('"+$(this).val()+"', true);");
      save_to_history('#advanced-filters-form-aux');
      
      change_visualization($(this).val(), false);
      
      //Cambiamos div de exportar visualizaciones
      set_exporter_options();
    }
  });

  
  function select_from_visualization(container_id, f,data_id, data_label){
    if($('#ms-'+data_id).length == 0){
      ul = $('<div>').attr('class', 'collection col s3 m3 l3')
                     .attr('id', 'ms-'+data_id)
                     .text(data_label)
                     .append($('<input />', {type: 'hidden',
                                            class: 'viz_select',
                                            value: data_label,
                                            })
                    )
      $("#clicked").append(ul);    
      $("#f-clicked-btn").attr('onclick', "check_"+f+"_from_visualization(false);");
      $("#sd-clicked-btn").attr('onclick', "check_"+f+"_from_visualization(true);");
      $(container_id).show();
    }else{
      $('#ms-'+data_id).remove();
      if($(".viz_select").length == 0){
        $(container_id).hide();
      }
    }
  }
  
  function check_collection_from_visualization(show_table){
    inputs = $('.viz_select');
    $('.coll-checkbox').prop('checked', false);
    $.each(inputs, function(i){
      country_label = $(inputs[i]).val();
      if(country_label != false){
        checkboxes = $("input[name='collections[]']");
        $.each(checkboxes, function(i){
          var checkbox = $('#'+checkboxes[i].id);       
          var label = $('label[for="'+ checkbox.attr('id') +'"]');
          if(label.text() == country_label){
            checkbox.click();
          }
        });
      }
    });
    if(show_table){
      $('#visualization-hidden').val('table');
    }
    $('#advanced-filters-form-submit-btn').click();
  }

  function check_thematic_area_from_visualization(show_table){
    inputs = $('.viz_select');
    $('.ta-checkbox').prop('checked', false);
    $.each(inputs, function(i){
      thematic_area = $(inputs[i]).val();
      if(thematic_area != false){
        checkboxes = $("input[name='thematic_areas[]']");
        $.each(checkboxes, function(i){
          var checkbox = $('#'+checkboxes[i].id);       
          var label = $('label[for="'+ checkbox.attr('id') +'"]');
          if(label.text() == thematic_area){
            checkbox.click();
          }
        });
      }
    });
    if(show_table){
      $('#visualization-hidden').val('table');
    }
    $('#advanced-filters-form-submit-btn').click();
  }

  function check_language_from_visualization(show_table){
    inputs = $('.viz_select');
    $('.lang-checkbox').prop('checked', false);
    $.each(inputs, function(i){
      language = $(inputs[i]).val();
      if(language != false){
        checkboxes = $("input[name='languages[]']");
        $.each(checkboxes, function(i){
          var checkbox = $('#'+checkboxes[i].id);       
          var label = $('label[for="'+ checkbox.attr('id') +'"]');
          if(label.text() == language){
            checkbox.click();
          }
        });
      }
    });
    if(show_table){
      $('#visualization-hidden').val('table');
    }
    $('#advanced-filters-form-submit-btn').click();
  }

  function check_journal_from_visualization(show_table){
    inputs = $('.viz_select');
    $('.jour-checkbox').prop('checked', false);
    $.each(inputs, function(i){
      journal = $(inputs[i]).val();
      if(journal != false){
        checkboxes = $("input[name='journals[]']");
        $.each(checkboxes, function(i){
          var checkbox = $('#'+checkboxes[i].id);       
          var label = $('label[for="'+ checkbox.attr('id') +'"]');
          if(label.text() == journal){
            checkbox.click();
          }
        });
      }
    });
    if(show_table){
      $('#visualization-hidden').val('table');
    }
    $('#advanced-filters-form-submit-btn').click();
  }


  function toggle_filter_container(label_class, container_id){
    $(label_class).toggle();
    $(container_id).slideToggle("slow");
  }

  function prepare_checkboxes(data, filters, type, container_id, modal_id, n_show, checkbox_name, txt_id, lang){
    var p_all = $('<p>');
    p_all.append($('<input />', { type: 'checkbox',
                                  id: txt_id,
                                  class: 'filled-in checkbox-deep-purple '+txt_id+'-checkbox',
                                  checked: false, 
                                  value: 'all',
                                  onchange: "check_all('."+txt_id+"-checkbox', $(this) )"}));
    p_all.append($('<label />', { 'for': txt_id, text: "{{ gettext('All') }}" }));
    $(container_id).append(p_all);

    for(i in data){
      if(type == 0){
        var text = data[i];
        var value = data[i];
      }else{
        var value = data[i][0];
        var text = data[i][1][lang];
      }
      
      (filters.indexOf(value) > -1 ) ? checked = true : checked = false;

      var tr = $('<tr>');
      var td = $('<td>');
      td.append($('<input />', { type: 'checkbox', id: txt_id+'-'+i, class: 'filled-in checkbox-deep-purple '+txt_id+'-checkbox', name: checkbox_name, value: value, checked: checked, onchange: "check('#"+txt_id+"-2-"+i+"');" }));
      td.append($('<label />', { 'for': txt_id+'-'+i, text: text }));
      tr.append(td);
      $(modal_id+'-checkboxes').append(tr);
        
      if(i == n_show){
        $(container_id).css({'height': $(container_id).height(),'overflow-y': 'scroll'});
        //$(container_id).append("<a href='"+modal_id+"' class='modal-trigger right'>Show all</a>");
      }
      var p2 = $('<p>');
      p2.append($('<input />', { type: 'checkbox', id: txt_id+'-2-'+i, class: 'filled-in checkbox-deep-purple '+txt_id+'-checkbox', checked: checked, onchange: "check('#"+txt_id+"-"+i+"');" }));
      p2.append($('<label />', { 'for': txt_id+'-2-'+i, text: text }));
      $(container_id).append(p2);
    }
  }

  function check(id){
    $(id).prop('checked', !$(id).prop('checked'));
    $('#advanced-filters-form-submit-btn').prop('disabled', false).removeClass('disabled');
  }
  function check_all(checkbox_class, checkbox_all){
    $(checkbox_class).prop('checked', $(checkbox_all).prop('checked'));
  }
  function current_filter_check(input_name, value){
    checkbox = $("#advanced-filters-form input[name='"+input_name+"'][value='"+value+"']");
    checkbox.prop("checked", false);
    $(checkbox.attr('onchange').replace("check('","").replace("');","")).prop("checked", false);
    $("#advanced-filters-form").submit();
  }
  function current_filter_years(){
    $('#year-select-from').val("1900");
    $('#year-select-to').val(parseInt(current_year));
    $("#advanced-filters-form").submit();
  }
  function current_filter_document_type(){
    $("#document-type-select").val('ALL').trigger('change');
    $("#advanced-filters-form").submit();
  }


  //SCROLL FIJAR BOTON
  $('#advanced-filters-form-submit-btn').css({ width: $("#advanced-filters-form").width() });
  var fp_top = $('#filter-panel').position().top;
  var fp_height = $('#filter-panel').height();
  var fp_bottom = fp_top + fp_height;
  var affsb_top = $('#advanced-filters-form-submit-btn').position().top
  if($(window).scrollTop() >= affsb_top - fp_bottom){
      $('#advanced-filters-form-submit-btn').css({ position: 'fixed', top: fp_bottom });
    }
  $(window).scroll(function(){
    $('#advanced-filters-form-submit-btn').css({ width: $("#advanced-filters-form").width() });
    if($(this).scrollTop() >= affsb_top - fp_bottom){
      $('#advanced-filters-form-submit-btn').css({ position: 'fixed', top: fp_bottom });
    }else{
      $('#advanced-filters-form-submit-btn').css({ position: 'relative', top: 'auto' });
    }
  });


  function set_lang(lang){
    $('#filters-form-lang').val(lang);
    $('#advanced-filters-form-lang').val(lang);
    $('#advanced-filters-form-lang-aux').val(lang);
    $('#advanced-filters-form-aux').submit();
  }

  
  //DataTable para checkbox
  $('.checkbox-datatable').DataTable({
    "paging": false,
    "info": false,
  });
  //Materialize
  $('select').material_select();
  //Modals
  $('.modal-trigger').leanModal();
</script>